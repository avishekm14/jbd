package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessWebLayoutModel;

import java.util.ArrayList;

/**
 * Created by administrator on 13/6/17.
 */

public class AddBusinessWebLayoutAdapter extends RecyclerView.Adapter<AddBusinessWebLayoutAdapter.Holder> {

    Context context;
    ArrayList<AddBusinessWebLayoutModel> addBusinessWebLayoutModels;

    LayoutInflater inflater;

    public AddBusinessWebLayoutAdapter(Context context, ArrayList<AddBusinessWebLayoutModel> addBusinessWebLayoutModels) {
        this.context = context;
        this.addBusinessWebLayoutModels = addBusinessWebLayoutModels;

        inflater = LayoutInflater.from(context);
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(inflater.inflate(R.layout.inflater_business_web, parent, false));
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int positionlayout) {

        final AddBusinessWebLayoutModel model = addBusinessWebLayoutModels.get(positionlayout);
        holder.holderModel = addBusinessWebLayoutModels.get(positionlayout);

        if (model.isAddOrNot()) {
            holder.add_business_remove_web_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_add_round));
        } else {
            holder.add_business_remove_web_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_remove_view));
        }

        holder.add_business_remove_web_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addBusinessWebLayoutModels.get(positionlayout).isAddOrNot()) {
                    if (allFieldsHasVals()) {              // // TODO: 13/6/17 need to set all other lists here in the model
                        AddBusinessWebLayoutModel model = new AddBusinessWebLayoutModel(false);

                        addBusinessWebLayoutModels.add(model);
                    }
                } else {
                    addBusinessWebLayoutModels.remove(positionlayout);
                }


                notifyDataSetChanged();

            }
        });

        holder.business_web_et.setText(model.getWebUrl());
        holder.business_web_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                holder.holderModel.setWebUrl(String.valueOf(s));
            }
        });

        holder.business_web_restriction_cb.setChecked(holder.holderModel.isDoNotDiaplay());
        holder.business_web_restriction_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                holder.holderModel.setDoNotDiaplay(isChecked);
            }
        });


    }

    private boolean allFieldsHasVals() {
        for(int i=0; i<addBusinessWebLayoutModels.size();i++){
            if(addBusinessWebLayoutModels.get(i).getWebUrl().equals(""))
                return false;
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return addBusinessWebLayoutModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView add_business_remove_web_iv;
        EditText business_web_et;
        CheckBox business_web_restriction_cb;
        AddBusinessWebLayoutModel holderModel;

        public Holder(View itemView) {
            super(itemView);

            add_business_remove_web_iv = (ImageView) itemView.findViewById(R.id.add_business_remove_web_iv);
            business_web_et = (EditText)itemView.findViewById(R.id.business_web_et);
            business_web_restriction_cb = (CheckBox) itemView.findViewById(R.id.business_web_restriction_cb);
        }
    }
}
