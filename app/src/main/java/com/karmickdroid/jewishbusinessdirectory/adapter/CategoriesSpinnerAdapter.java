package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.BusinessCategories;

import java.util.List;

/**
 * Created by avishek on 12/4/17.
 */

public class CategoriesSpinnerAdapter extends ArrayAdapter<String> {
    private TextView et_selected_place;
    private Context context;
    private List<BusinessCategories> location;
    private LayoutInflater inflter;
    private int dropDownView, header;

    public CategoriesSpinnerAdapter(Context applicationContext, List<BusinessCategories> mList, int header, int dropDownView) {
        super(applicationContext, R.layout.custom_spinner_items);
        this.context = applicationContext;
        this.location = mList;
        this.dropDownView = header;
        this.header = dropDownView;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return location.size();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflter.inflate(dropDownView,null);
        }
        et_selected_place = (TextView) convertView.findViewById(R.id.et_browse_categories);
        if(position==0){
            convertView.setVisibility(View.GONE);
            et_selected_place.setHeight(0);
        }else{
            convertView.setVisibility(View.VISIBLE);
            et_selected_place.setHeight(context.getResources().getDimensionPixelSize(R.dimen._32sdp));
            et_selected_place.setText(String.format("%s",location.get(position).getBusiness_category_name()));
        }
        return convertView;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflter.inflate(header, null);
        et_selected_place = (TextView) view.findViewById(R.id.et_browse_categories);
        et_selected_place.setFocusable(false);
        et_selected_place.setClickable(false);
        et_selected_place.setText(String.format("%s",location.get(position).getBusiness_category_name()));
        return view;
    }

    public void notifyDataChanged(List<BusinessCategories> mList){
        this.location = mList;
        notifyDataSetChanged();
    }
}
