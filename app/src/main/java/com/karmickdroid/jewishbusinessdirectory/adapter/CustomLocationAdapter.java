package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.LocationModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 27-06-2017.
 */

public class CustomLocationAdapter extends RecyclerView.Adapter<CustomLocationAdapter.EventPlacesViewHolder> implements View.OnClickListener {


    private Context context;
    private List<LocationModel> locationListModels;


    public CustomLocationAdapter(Context context, List<LocationModel> placesModelArrayList) {
        this.context = context;
        this.locationListModels = placesModelArrayList;

    }


    @Override
    public EventPlacesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventPlacesViewHolder(LayoutInflater.from(context).inflate(R.layout.row_event_place, parent, false));
    }

    @Override
    public void onBindViewHolder(EventPlacesViewHolder holder, int position) {
        LocationModel model = locationListModels.get(position);


        if (model.isSelected()) {
            holder.rowEventPlace_tv_name.setTextColor(context.getResources().getColor(R.color.colorAccent));
            holder.rowEventPlace_chkbx.setChecked(true);
        } else {
            holder.rowEventPlace_tv_name.setTextColor(context.getResources().getColor(R.color.textColorBlack));
            holder.rowEventPlace_chkbx.setChecked(false);
        }
        holder.rowEventPlace_tv_name.setText(model.getLocation_name());

        holder.rowEventPlace_ll.setTag(model);
        holder.rowEventPlace_ll.setOnClickListener(this);

    }

    @Override
    public int getItemCount() {
        return locationListModels.size();
    }

    public void notifyAdapter(List<LocationModel> placesModelArrayList) {
        this.locationListModels = placesModelArrayList;
        notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.rowEventPlace_ll:
                LocationModel model = (LocationModel) view.getTag();

                locationListModels.get(locationListModels.indexOf(model)).setSelected(!locationListModels.get(locationListModels.indexOf(model)).isSelected());

                notifyItemChanged(locationListModels.indexOf(model));
                ((OnLocationItemClicked)context).onLocationItemClicked();


                break;
        }
    }







    public class EventPlacesViewHolder extends RecyclerView.ViewHolder  {

        public TextView rowEventPlace_tv_name;
        public CheckBox rowEventPlace_chkbx;
        public LinearLayout rowEventPlace_ll;

        public EventPlacesViewHolder(View itemView) {
            super(itemView);
            rowEventPlace_tv_name = (TextView)itemView.findViewById(R.id.rowEventPlace_tv_name);
            rowEventPlace_chkbx = (CheckBox)itemView.findViewById(R.id.rowEventPlace_chkbx);
            rowEventPlace_ll = (LinearLayout) itemView.findViewById(R.id.rowEventPlace_ll);
        }
    }


    public interface OnLocationItemClicked{
        void onLocationItemClicked();
    }

}
