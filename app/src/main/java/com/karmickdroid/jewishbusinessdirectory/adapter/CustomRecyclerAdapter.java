package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.BusinessCategories;
import com.karmickdroid.jewishbusinessdirectory.model.LocationModel;
import com.viethoa.RecyclerViewFastScroller;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 5/12/17.
 */

public class CustomRecyclerAdapter extends RecyclerView.Adapter<CustomRecyclerAdapter.ViewHolder>  implements RecyclerViewFastScroller.BubbleTextGetter{
    private static final String TAG = "CustomAdapter";

    List<BusinessCategories> locationModel;
    private ArrayList<Integer> mSectionPositions;

    public CustomRecyclerAdapter(List<BusinessCategories> locationModel) {
        this.locationModel=locationModel;
    }


    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public String getTextToShowInBubble(int pos) {
        if (pos < 0 || pos >= locationModel.size())
            return null;

        String name = locationModel.get(pos).getBusiness_category_name();
        if (name == null || name.length() < 1)
            return null;

        return locationModel.get(pos).getBusiness_category_name().substring(0, 1);
    }

//    @Override
//    public int getSectionForPosition(int position) {
//        return 0;
//    }
//
//    @Override
//    public Object[] getSections() {
//        List<String> sections = new ArrayList<>(26);
//        mSectionPositions = new ArrayList<>(26);
//        for (int i = 0, size = locationModel.size(); i < size; i++) {
//            String section = String.valueOf(locationModel.get(i).getBusiness_category_name().charAt(0)).toUpperCase();
//            if (!sections.contains(section)) {
//                sections.add(section);
//                mSectionPositions.add(i);
//            }
//        }
//        return sections.toArray(new String[0]);
//    }
//
//    @Override
//    public int getPositionForSection(int sectionIndex) {
//        return mSectionPositions.get(sectionIndex);
//    }

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getAdapterPosition() + " clicked.");
                }
            });
            textView = (TextView) v.findViewById(R.id.et_browse_categories);
        }

        public TextView getTextView() {
            return textView;
        }
    }
    // END_INCLUDE(recyclerViewSampleViewHolder)


//    public CustomRecyclerAdapter(String[] dataSet) {
//        mDataSet = dataSet;
//    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.categories_dropdown_row, viewGroup, false);

        return new ViewHolder(v);
    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");

        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        viewHolder.getTextView().setText(locationModel.get(position).getBusiness_category_name());
    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return locationModel.size();
    }
}