package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.LocationModel;

import java.util.List;

/**
 * Created by avishek on 12/4/17.
 */

public class CustomSpinnerAdapter extends ArrayAdapter<String> {
    private TextView et_selected_place;
    private Context context;
    private List<LocationModel> locationModel;
    private LayoutInflater inflter;
    private int dropDownView, header;

    public CustomSpinnerAdapter(Context applicationContext, List<LocationModel> mList, int dropDownView, int header) {
        super(applicationContext, R.layout.custom_spinner_items);
        this.context = applicationContext;
        this.locationModel = mList;
        this.dropDownView = dropDownView;
        this.header = header;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return locationModel.size();
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            convertView = inflter.inflate(dropDownView,null);
        }
        et_selected_place = (TextView) convertView.findViewById(R.id.et_selected_place);
        if(position==0){
            convertView.setVisibility(View.GONE);
            et_selected_place.setHeight(0);
        }else{
            convertView.setVisibility(View.VISIBLE);
            et_selected_place.setHeight(context.getResources().getDimensionPixelSize(R.dimen._30sdp));
            et_selected_place.setText(String.format("%s", locationModel.get(position).getLocation_name()));
        }
        return convertView;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = inflter.inflate(header, null);
        et_selected_place = (TextView) view.findViewById(R.id.et_selected_place);
        et_selected_place.setHeight(context.getResources().getDimensionPixelSize(R.dimen._30sdp));
        et_selected_place.setText(String.format("%s", locationModel.get(position).getLocation_name()));
        return view;
    }

    public void notifyDataChanged(List<LocationModel> mList){
        this.locationModel = mList;
        notifyDataSetChanged();
    }
}
