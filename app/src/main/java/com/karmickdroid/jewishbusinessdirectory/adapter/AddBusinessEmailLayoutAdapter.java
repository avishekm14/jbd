package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessEmailLayoutModel;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessPhoneLayoutModel;

import java.util.ArrayList;

/**
 * Created by administrator on 13/6/17.
 */

public class AddBusinessEmailLayoutAdapter extends RecyclerView.Adapter<AddBusinessEmailLayoutAdapter.Holder> {

    Context context;
    ArrayList<AddBusinessEmailLayoutModel> addBusinessEmailLayoutModels;

    LayoutInflater inflater;

    public AddBusinessEmailLayoutAdapter(Context context, ArrayList<AddBusinessEmailLayoutModel> addBusinessEmailLayoutModels) {
        this.context = context;
        this.addBusinessEmailLayoutModels = addBusinessEmailLayoutModels;

        inflater = LayoutInflater.from(context);
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(inflater.inflate(R.layout.inflater_business_mail, parent, false));
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int positionlayout) {

        final AddBusinessEmailLayoutModel model = addBusinessEmailLayoutModels.get(positionlayout);
        holder.holderModel = addBusinessEmailLayoutModels.get(positionlayout);

        if (model.isAddOrNot()) {
            holder.add_business_remove_mail_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_add_round));
        } else {
            holder.add_business_remove_mail_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_remove_view));
        }

        holder.add_business_remove_mail_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addBusinessEmailLayoutModels.get(positionlayout).isAddOrNot()) {
                    if (allFieldsHasVals()) {              // // TODO: 13/6/17 need to set all other lists here in the model
                        AddBusinessEmailLayoutModel model = new AddBusinessEmailLayoutModel(false);

                        addBusinessEmailLayoutModels.add(model);
                    }
                } else {
                    addBusinessEmailLayoutModels.remove(positionlayout);
                }


                notifyDataSetChanged();

            }
        });

        holder.business_mail_et.setText(model.getMailId());
        holder.business_mail_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                holder.holderModel.setMailId(String.valueOf(s));
            }
        });

        holder.business_mail_restriction_cb.setChecked(holder.holderModel.isDoNotDiaplay());
        holder.business_mail_restriction_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                holder.holderModel.setDoNotDiaplay(isChecked);
            }
        });


    }

    private boolean allFieldsHasVals() {
        for(int i=0; i<addBusinessEmailLayoutModels.size();i++){
            if(addBusinessEmailLayoutModels.get(i).getMailId().equals(""))
                return false;
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return addBusinessEmailLayoutModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView add_business_remove_mail_iv;
        EditText business_mail_et;
        CheckBox business_mail_restriction_cb;
        AddBusinessEmailLayoutModel holderModel;

        public Holder(View itemView) {
            super(itemView);

            add_business_remove_mail_iv = (ImageView) itemView.findViewById(R.id.add_business_remove_mail_iv);
            business_mail_et = (EditText)itemView.findViewById(R.id.business_mail_et);
            business_mail_restriction_cb = (CheckBox) itemView.findViewById(R.id.business_mail_restriction_cb);
        }
    }
}
