package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessCategoryModel;
import com.karmickdroid.jewishbusinessdirectory.model.BusinessCategories;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 13/6/17.
 */

public class AddBusinessCategoryLayoutAdapter extends RecyclerView.Adapter<AddBusinessCategoryLayoutAdapter.Holder> {

    Context context;
    ArrayList<AddBusinessCategoryModel> addBusinessCategoryModels;
    //List<BusinessCategories> businessCategories;
    LayoutInflater inflater;

    public AddBusinessCategoryLayoutAdapter(Context context, ArrayList<AddBusinessCategoryModel> addBusinessCategoryModels) {
        this.context = context;
        this.addBusinessCategoryModels = addBusinessCategoryModels;
        //this.businessCategories = businessCategories;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(inflater.inflate(R.layout.inflater_business_category, parent, false));
    }

    @Override
    public void onBindViewHolder(Holder holder, final int position) {

        final AddBusinessCategoryModel model = addBusinessCategoryModels.get(position);
        final List<BusinessCategories> categoriesList = model.getCategoriesList();

        if (model.isAddOrNot()) {
            holder.add_business_remove_cat_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_add_round));
        } else {
            holder.add_business_remove_cat_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_remove_view));
        }


        AddBusinessCategorySpinnerAdapter adapter = new AddBusinessCategorySpinnerAdapter(context, android.R.layout.simple_spinner_item, addBusinessCategoryModels.get(position).getCategoriesList());
        holder.add_business_business_category_sp.setAdapter(adapter);
        holder.add_business_business_category_sp.setSelection(model.getSelectedPos());
        holder.add_business_business_category_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                model.setSelectedPos(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.add_business_remove_cat_iv.setTag(position);
        holder.add_business_remove_cat_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addBusinessCategoryModels.get(position).isAddOrNot()) {
                    if (allSpinnerHasVals()) {
                        AddBusinessCategoryModel model = new AddBusinessCategoryModel(false);
                        model.setCategoriesList(categoriesList);
                        addBusinessCategoryModels.add(model);
                    }
                } else {
                    addBusinessCategoryModels.remove(position);
                }
                notifyDataSetChanged();
            }
        });
    }

    private boolean allSpinnerHasVals() {
        for(int i=0; i<addBusinessCategoryModels.size();i++){
            if(addBusinessCategoryModels.get(i).getSelectedPos()==0)
                return false;
        }
        return true;
    }

    @Override
    public int getItemCount() {
        return addBusinessCategoryModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView add_business_remove_cat_iv;
        Spinner add_business_business_category_sp;

        public Holder(View itemView) {
            super(itemView);

            add_business_remove_cat_iv = (ImageView) itemView.findViewById(R.id.add_business_remove_cat_iv);
            add_business_business_category_sp = (Spinner) itemView.findViewById(R.id.add_business_business_category_sp);
        }
    }
}
