package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.Contacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by avishek on 10/4/17.
 */

public class ContactResultListAdapter extends RecyclerView.Adapter<ContactResultListAdapter.ContactViewHolder> {

    private List<Contacts> mContacts = new ArrayList<>();
    private Context mContext;
    private OnRowClickListener mListener;

    public ContactResultListAdapter(Context mContext, List<Contacts> mContacts) {
        this.mContext = mContext;
        this.mContacts = mContacts;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_row_address,
                parent, false);
        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, final int position) {
        if (mContacts.get(position).isNeedLoader()) {
            holder.progress.setVisibility(View.VISIBLE);
            holder.rl_contact_row.setVisibility(View.GONE);
        } else {
            holder.progress.setVisibility(View.GONE);
            holder.rl_contact_row.setVisibility(View.VISIBLE);
            if (mContacts.get(position).getAccount_type().equalsIgnoreCase("1")) {
                holder.contact_text.setBackground(mContext.getResources().getDrawable(R.drawable.circle_black));
                holder.contact_text.setText(mContext.getResources().getString(R.string.residential_type));
                holder.contact_type.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_residential_dark));

                holder.contact_name.setText(mContacts.get(position).getLast_name() + " " + mContacts.get(position).getFirst_name());
            } else if (mContacts.get(position).getAccount_type().equalsIgnoreCase("2")) {
                holder.contact_text.setBackground(mContext.getResources().getDrawable(R.drawable.circle_green));
                holder.contact_text.setText(mContext.getResources().getString(R.string.business_type));
                holder.contact_type.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_business_dark));

                holder.contact_name.setText(mContacts.get(position).getBusiness_name());
            } else if (mContacts.get(position).getAccount_type().equalsIgnoreCase("3")) {
                holder.contact_text.setBackground(mContext.getResources().getDrawable(R.drawable.circle_blue));
                holder.contact_text.setText(mContext.getResources().getString(R.string.home_business_type));
                holder.contact_type.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_community_dark));

                holder.contact_name.setText(mContacts.get(position).getLast_name() + " " + mContacts.get(position).getFirst_name()
                        + " - " + mContacts.get(position).getBusiness_name());
            }

            //holder.contact_address.setText(mContacts.get(position).getStreet_name());

            String streetOrHouseNo = "";

            /*
             * IF house no starts with letter then show house_no field value where showing street .
             */
//            if (mContacts.get(position).getHouse_no().length()>0 &&
//                    Character.isLetter(mContacts.get(position).getHouse_no().charAt(0))) {  // ie. house no starts with letter

            String street_name = mContacts.get(position).getStreet_name();
            if (street_name.trim().equals("")) {
                streetOrHouseNo = mContacts.get(position).getHouse_no() + " " + mContacts.get(position).getOther_street();
            } else {
                streetOrHouseNo = mContacts.get(position).getHouse_no() + " " + mContacts.get(position).getStreet_name();
            }

//            } else {
//                streetOrHouseNo = mContacts.get(position).getStreet_name();
//            }

            if (mContacts.get(position).getIs_publish().equals("1")) {
                holder.contact_address.setVisibility(View.VISIBLE);
                holder.contact_address.setText(" " + streetOrHouseNo.trim());
            } else {
                holder.contact_address.setVisibility(View.GONE);
            }

            holder.contact_call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mListener != null) {
                        mListener.onCall(position);
                    }
                }
            });

            holder.ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onNextToAccountDetails(mContacts.get(position).getAccount_id());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mContacts.size();
    }

    public void notifyDataChanged(List<Contacts> mContacts) {
        this.mContacts = mContacts;
        notifyDataSetChanged();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        LinearLayout ll;
        RelativeLayout rl_contact_row;
        ImageView contact_type, contact_call, iv_next;
        TextView contact_text, contact_name, contact_address;
        ProgressBar progress;

        public ContactViewHolder(View mView) {
            super(mView);
            ll = (LinearLayout) mView;
            rl_contact_row = (RelativeLayout) mView.findViewById(R.id.rl_contact_row);
            contact_call = (ImageView) mView.findViewById(R.id.contact_call);
            contact_type = (ImageView) mView.findViewById(R.id.contact_type);
            contact_text = (TextView) mView.findViewById(R.id.contact_text);
            contact_name = (TextView) mView.findViewById(R.id.contact_name);
            contact_address = (TextView) mView.findViewById(R.id.contact_address);
            iv_next = (ImageView) mView.findViewById(R.id.iv_next);

            progress = (ProgressBar) mView.findViewById(R.id.progress);
        }
    }

    public void setOnRowClickListener(OnRowClickListener mListener) {
        this.mListener = mListener;
    }

    public interface OnRowClickListener {
        void onCall(int position);

        void onNextToAccountDetails(String account_id);
    }
}
