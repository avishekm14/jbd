package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.ModelCityStateStreetTelZip;
import com.karmickdroid.jewishbusinessdirectory.model.ModelLocationDetail;

import java.util.List;

/**
 * Created by administrator on 13/6/17.
 */

public class AddBusinessAddressStreetSpinnerAdapter extends ArrayAdapter<ModelLocationDetail> {

    List<ModelCityStateStreetTelZip> list;
    Context context;
    LayoutInflater inflater;


    public AddBusinessAddressStreetSpinnerAdapter(@NonNull Context context, @LayoutRes int resource,
                                                  List<ModelCityStateStreetTelZip> list) {
        super(context, resource);
        this.context = context;
        this.list = list;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup viewGroup) {
        return getCustomView(position, convertView, viewGroup);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        return getCustomView(position, convertView, viewGroup);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    public View getCustomView(final int position, View convertView, ViewGroup viewGroup) {
        View row = inflater.inflate(android.R.layout.simple_spinner_item, viewGroup, false);
        TextView label = (TextView) row.findViewById(android.R.id.text1);
        label.setText(list.get(position).getName());

        return row;
    }

    public void notify(List<ModelCityStateStreetTelZip> list) {
        this.list = list;
        notifyDataSetChanged();
    }

}
