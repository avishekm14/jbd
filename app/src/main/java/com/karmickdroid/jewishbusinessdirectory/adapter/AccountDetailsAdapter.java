package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.AccountDetailsTelephoneAddressModel;
import com.karmickdroid.jewishbusinessdirectory.viewHolders.AccountDetailsHolder;

import java.util.ArrayList;

/**
 * Created by administrator on 9/6/17.
 */

public class AccountDetailsAdapter extends RecyclerView.Adapter<AccountDetailsHolder> {

    Context context;
    ArrayList<AccountDetailsTelephoneAddressModel> accountDetailsTelephoneAddressModels;
    public OnRowClicked onRowClickedCallBack = null;
    String address = "";

    public AccountDetailsAdapter(Context context, ArrayList<AccountDetailsTelephoneAddressModel> accountDetailsTelephoneAddressModels) {
        this.context = context;
        this.accountDetailsTelephoneAddressModels = accountDetailsTelephoneAddressModels;
    }


    @Override
    public AccountDetailsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AccountDetailsHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inflater_account_details_row,
                parent, false));
    }

    @Override
    public void onBindViewHolder(AccountDetailsHolder holder, final int position) {

        if (!accountDetailsTelephoneAddressModels.get(position).getTelephone_no().equals("")) {
            holder.accountDetailsRow_iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_call));


            if (accountDetailsTelephoneAddressModels.get(position).getLocation_telephone().equals("2")) {
                holder.accountDetailsRow_tv_upper.setText(context.getResources().getString(R.string.other));
            } else {
                holder.accountDetailsRow_tv_upper.setText(accountDetailsTelephoneAddressModels.get(position).getLocation_name());
            }

            holder.accountDetailsRow_tv_lower_ph.setText(accountDetailsTelephoneAddressModels.get(position).getArea_code() + "-"
                    + accountDetailsTelephoneAddressModels.get(position).getTelephone_no().substring(0, 3) + "-"
                    + accountDetailsTelephoneAddressModels.get(position).getTelephone_no().substring(3)
            );

            holder.accountDetailsRow_tv_lower_ph.setVisibility(View.VISIBLE);
            holder.accountDetailsRow_tv_lower_address.setVisibility(View.GONE);
            holder.accountDetailsRow_ll_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRowClickedCallBack.onClickingOnRow(position, "call");
                }
            });
        } else {
            holder.accountDetailsRow_iv_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_address_icon));

            String streetOrHouseNo = "";
            address = "";

            String houseNo = "", streetName = "", aptNo = "", city = "", state = "", zip = "";
            String[] add = new String[6];

            /*
             * IF location value is 2 then show location name 'OTHER' and take fields value 'other_street',
             * 'other_city', 'other_state', 'other_zip' insteed of 'street', 'city', 'state', 'zip'
             * */
            if (accountDetailsTelephoneAddressModels.get(position).getLocation_address().equals("2")) {
                holder.accountDetailsRow_tv_upper.setText(context.getResources().getString(R.string.other));

                houseNo = accountDetailsTelephoneAddressModels.get(position).getHouse_no();
                streetName = accountDetailsTelephoneAddressModels.get(position).getOther_street();
                aptNo = accountDetailsTelephoneAddressModels.get(position).getApt_no();
                city = accountDetailsTelephoneAddressModels.get(position).getOther_city();
                state = accountDetailsTelephoneAddressModels.get(position).getOther_state();
                zip = accountDetailsTelephoneAddressModels.get(position).getOther_zip();
            } else {
                holder.accountDetailsRow_tv_upper.setText(accountDetailsTelephoneAddressModels.get(position).getLocation_name());

                houseNo = accountDetailsTelephoneAddressModels.get(position).getHouse_no();
                streetName = accountDetailsTelephoneAddressModels.get(position).getStreet_name();
                aptNo = accountDetailsTelephoneAddressModels.get(position).getApt_no();
                city = accountDetailsTelephoneAddressModels.get(position).getCity_name();
                state = accountDetailsTelephoneAddressModels.get(position).getState_code();
                zip = accountDetailsTelephoneAddressModels.get(position).getZipcode();
            }


            add[0] = (houseNo.equals(null) || houseNo.equals("")) ? "" : houseNo + " ";
            add[1] = (streetName == null || streetName.equals("")) ? "" : streetName + ", ";
            add[2] = (aptNo.equals(null) || aptNo.equals("")) ? "" : aptNo + ", ";
            add[3] = (city == null || city.equals("")) ? "" : city + ", ";
            add[4] = (state == null || state.equals("")) ? "" : state + " ";
            add[5] = (zip == null || zip.equals("")) ? "" : zip;

//            for (int i = 0; i < add.length; i++) {
//                if (!add[i].equals("")) {
//                    if (address.equals("")) {
//                        address = add[i];
//                    } else {
//                        address = address + ", " + add[i];
//                    }
//                }
//            }

            for (int i = 0; i < add.length; i++) {
                if (!add[i].equals("")) {
                    if (address.equals("")) {
                        address = add[i];
                    } else {
                        address = address + add[i];
                    }
                }
            }


            /*
             * IF house no starts with letter then show house_no field value where showing street .
             */
            /*if (accountDetailsTelephoneAddressModels.get(position).getHouse_no().length() > 0 &&
                    Character.isLetter(accountDetailsTelephoneAddressModels.get(position).getHouse_no().charAt(0))) {  // ie. house no starts with letter
                streetOrHouseNo = accountDetailsTelephoneAddressModels.get(position).getHouse_no();
            } else {
                streetOrHouseNo = accountDetailsTelephoneAddressModels.get(position).getStreet_name();
            }*/


            holder.accountDetailsRow_tv_lower_address.setText(address);
            holder.accountDetailsRow_tv_lower_address.setVisibility(View.VISIBLE);
            holder.accountDetailsRow_tv_lower_ph.setVisibility(View.GONE);

            holder.accountDetailsRow_ll_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRowClickedCallBack.onClickingOnRow(position, address.toString());
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return accountDetailsTelephoneAddressModels.size();
    }


    public void notifyAdapter(ArrayList<AccountDetailsTelephoneAddressModel> accountDetailsTelephoneAddressModels) {
        this.accountDetailsTelephoneAddressModels = accountDetailsTelephoneAddressModels;

    }


    public interface OnRowClicked {
        void onClickingOnRow(int position, String type);
    }


}
