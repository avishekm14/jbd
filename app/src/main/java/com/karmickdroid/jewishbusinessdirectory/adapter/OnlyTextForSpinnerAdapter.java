package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.ModelCityStateStreetTelZip;

import java.util.List;

/**
 * Created by administrator on 13/4/17.
 */

public class OnlyTextForSpinnerAdapter extends ArrayAdapter<ModelCityStateStreetTelZip> {
    List<ModelCityStateStreetTelZip> list;
    Context context;
    LayoutInflater inflater;
    String firstItemName;

    public OnlyTextForSpinnerAdapter(Context context, int resource,
                                     List<ModelCityStateStreetTelZip> list, String firstItemName) {
        super(context, resource, list);
        this.context = context;
        this.list = list;
        this.firstItemName = firstItemName;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup viewGroup) {
        return getCustomView(position, convertView, viewGroup);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        return getCustomView(position, convertView, viewGroup);
    }

    @Override
    public int getCount() {
        return list.size() + 1;
    }

    public View getCustomView(final int position, View convertView, ViewGroup viewGroup) {
        View row = inflater.inflate(android.R.layout.simple_spinner_item, viewGroup, false);
        TextView label = (TextView) row.findViewById(android.R.id.text1);
        if (position == 0) {
            label.setText(firstItemName);
        } else {
            label.setText(list.get(position - 1).getName());
        }
        return row;
    }

    public void notifyAdapter(List<ModelCityStateStreetTelZip> list) {
        this.list = list;
        notifyDataSetChanged();
    }
}
