package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessAddressLayoutModel;
import com.karmickdroid.jewishbusinessdirectory.model.ModelCityStateStreetTelZip;
import com.karmickdroid.jewishbusinessdirectory.model.ModelLocationDetail;

import java.util.ArrayList;

/**
 * Created by administrator on 13/6/17.
 */

public class AddBusinessAddressLayoutAdapter extends RecyclerView.Adapter<AddBusinessAddressLayoutAdapter.Holder> {

    Context context;
    ArrayList<AddBusinessAddressLayoutModel> addBusinessAddressLayoutModels;
    LayoutInflater inflater;

    public OnLocationSelection onLocationSelection;

    public AddBusinessAddressLayoutAdapter(Context context, ArrayList<AddBusinessAddressLayoutModel> addBusinessCategoryModels) {
        this.context = context;
        this.addBusinessAddressLayoutModels = addBusinessCategoryModels;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(inflater.inflate(R.layout.inflater_business_address, parent, false));
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int positionlayout) {
        final AddBusinessAddressLayoutModel model = addBusinessAddressLayoutModels.get(positionlayout);
        final ArrayList<ModelLocationDetail> finalModelLocationDetails = model.getModelLocationDetails();
        holder.holderModel = model;

        if (addBusinessAddressLayoutModels.get(positionlayout).isAddOrNot()) {
            holder.add_business_remove_address_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_add_round));
        } else {
            holder.add_business_remove_address_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_remove_view));
        }
        holder.add_business_remove_address_iv.setTag(positionlayout);

        holder.add_business_remove_address_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addBusinessAddressLayoutModels.get(positionlayout).isAddOrNot()) {
                    if (allFieldsHasVals()) {              // // TODO: 13/6/17 need to set all other lists here in the model
                        AddBusinessAddressLayoutModel model = new AddBusinessAddressLayoutModel(false);
                        model.setModelLocationDetails(finalModelLocationDetails);
                        addBusinessAddressLayoutModels.add(model);

                        /*finalModelStreet.clear();
                        ModelCityStateStreetTelZip streetModel = new ModelCityStateStreetTelZip(context.getString(R.string.select_street));
                        finalModelStreet.add(0, streetModel);*/
                    }
                } else {
                    addBusinessAddressLayoutModels.remove(positionlayout);
                }

                makeAllModelsNotifyFlagTrue();
                notifyDataSetChanged();

            }
        });

        holder.business_address_house_et.setText(model.getHouseNo());
        holder.business_address_apt_et.setText(model.getAptNo());

        // location spinner section
        AddBusinessAddressLocationSpinnerAdapter adapter = new AddBusinessAddressLocationSpinnerAdapter(context, android.R.layout.simple_spinner_item, finalModelLocationDetails);
        holder.business_address_location_sp.setAdapter(adapter);
        holder.business_address_location_sp.setSelection(model.getSelectedLocationPos());

        // street spinner section
        final AddBusinessAddressStreetSpinnerAdapter streetAdapter = new AddBusinessAddressStreetSpinnerAdapter(context, android.R.layout.simple_spinner_item,
                model.getModelLocationDetails().get(model.getSelectedLocationPos()).getStreet());
        holder.business_address_street_sp.setAdapter(streetAdapter);

        // city spinner section
        final AddBusinessAddressCitySpinnerAdapter cityAdapter = new AddBusinessAddressCitySpinnerAdapter(context, android.R.layout.simple_spinner_item,
                model.getModelLocationDetails().get(model.getSelectedLocationPos()).getCity());
        holder.business_address_city_sp.setAdapter(cityAdapter);

        // state spinner section
        final AddBusinessAddressStateSpinnerAdapter stateAdapter = new AddBusinessAddressStateSpinnerAdapter(context, android.R.layout.simple_spinner_item,
                model.getModelLocationDetails().get(model.getSelectedLocationPos()).getState());
        holder.business_address_state_sp.setAdapter(stateAdapter);

        // zip spinner section
        final AddBusinessAddressZipSpinnerAdapter zipAdapter = new AddBusinessAddressZipSpinnerAdapter(context, android.R.layout.simple_spinner_item,
                model.getModelLocationDetails().get(model.getSelectedLocationPos()).getZip());
        holder.business_address_zip_sp.setAdapter(zipAdapter);


        holder.business_address_location_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int positionLoc, long id) {

                model.setSelectedLocationPos(positionLoc);
                model.setSelectedPrevAddModel(model.getModelLocationDetails().get(positionLoc));
                onLocationSelection.onLocationSelected(model.getModelLocationDetails().get(positionLoc));


                // street section
                if (positionLoc > 0) {
                    if (model.getModelLocationDetails().get(positionLoc).getStreet().size() == 0 || model.getModelLocationDetails().get(positionLoc).getStreet().get(0).getId() != 0)
                        model.getModelLocationDetails().get(positionLoc).getStreet().add(0, new ModelCityStateStreetTelZip(context.getString(R.string.select_street)));

                    if (model.getModelLocationDetails().get(positionLoc).getCity().size() == 0 || model.getModelLocationDetails().get(positionLoc).getCity().get(0).getId() != 0)
                        model.getModelLocationDetails().get(positionLoc).getCity().add(0, new ModelCityStateStreetTelZip(context.getString(R.string.select_city)));

                    if (model.getModelLocationDetails().get(positionLoc).getState().size() == 0 || model.getModelLocationDetails().get(positionLoc).getState().get(0).getId() != 0)
                        model.getModelLocationDetails().get(positionLoc).getState().add(0, new ModelCityStateStreetTelZip(context.getString(R.string.select_state)));

                    if (model.getModelLocationDetails().get(positionLoc).getZip().size() == 0 || model.getModelLocationDetails().get(positionLoc).getZip().get(0).getId() != 0)
                        model.getModelLocationDetails().get(positionLoc).getZip().add(0, new ModelCityStateStreetTelZip(context.getString(R.string.select_zip)));

                    if (model.isIfNotifiedByAddRemoveBtn()) {
                        holder.business_address_street_sp.setSelection(model.getSelectedStreetPos());
                        holder.business_address_city_sp.setSelection(model.getSelectedCityPos());
                        holder.business_address_state_sp.setSelection(model.getSelectedStatePos());
                        holder.business_address_zip_sp.setSelection(model.getSelectedZipPos());
                        model.setIfNotifiedByAddRemoveBtn(false);
                    } else {
                        holder.business_address_street_sp.setSelection(0);
                        holder.business_address_city_sp.setSelection(0);
                        holder.business_address_state_sp.setSelection(0);
                        holder.business_address_zip_sp.setSelection(0);
                    }
                }

                // notifyAdapter other related spinners
                streetAdapter.notify(model.getModelLocationDetails().get(positionLoc).getStreet());
                cityAdapter.notify(model.getModelLocationDetails().get(positionLoc).getCity());
                stateAdapter.notify(model.getModelLocationDetails().get(positionLoc).getState());
                zipAdapter.notify(model.getModelLocationDetails().get(positionLoc).getZip());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // \location spinner section

        // street spinner section
        holder.business_address_street_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int positionStreet, long id) {
                model.setSelectedStreetPos(positionStreet);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // \street spinner section

        // city spinner section
        holder.business_address_city_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int positionCity, long id) {
                model.setSelectedCityPos(positionCity);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // \city spinner section

        // state spinner section
        holder.business_address_state_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int positionstate, long id) {
                model.setSelectedStatePos(positionstate);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // \state spinner section

        // zip spinner section
        holder.business_address_zip_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int positionZip, long id) {
                model.setSelectedZipPos(positionZip);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        // \zip spinner section


        // checkbox
        holder.business_address_restriction_cb.setChecked(model.isDoNotDisplayChecked());
        holder.business_address_restriction_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                model.setDoNotDisplayChecked(isChecked);
            }
        });
        // \checkbox

        holder.business_address_house_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                holder.holderModel.setHouseNo(String.valueOf(s));
            }
        });

        holder.business_address_apt_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                holder.holderModel.setAptNo(String.valueOf(s));
            }
        });


    }

    private void makeAllModelsNotifyFlagTrue() {
        for (int i = 0; i < addBusinessAddressLayoutModels.size(); i++) {
            addBusinessAddressLayoutModels.get(i).setIfNotifiedByAddRemoveBtn(true);
        }
    }

    private boolean allFieldsHasVals() { // // TODO: 13/6/17 check all other fields is filled or not
        for (int i = 0; i < addBusinessAddressLayoutModels.size(); i++) {
            if (addBusinessAddressLayoutModels.get(i).getSelectedLocationPos() == 0 ||
                    (addBusinessAddressLayoutModels.get(i).getHouseNo().length()>0 && !Character.isLetter(addBusinessAddressLayoutModels.get(i).getHouseNo().charAt(0)) && addBusinessAddressLayoutModels.get(i).getSelectedStreetPos() == 0) ||
                    addBusinessAddressLayoutModels.get(i).getSelectedCityPos() == 0 ||
                    addBusinessAddressLayoutModels.get(i).getSelectedStatePos() == 0
                    ) {
                return false;
            } else if (addBusinessAddressLayoutModels.get(i).getHouseNo().equals("") //|| addBusinessAddressLayoutModels.get(i).getAptNo().equals("")
                    ) {
                return false;
            }


        }
        return true;
    }

    @Override
    public int getItemCount() {
        return addBusinessAddressLayoutModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView add_business_remove_address_iv;
        Spinner business_address_location_sp, business_address_street_sp, business_address_city_sp, business_address_state_sp, business_address_zip_sp;
        CheckBox business_address_restriction_cb;
        EditText business_address_house_et, business_address_apt_et;
        AddBusinessAddressLayoutModel holderModel;

        public Holder(View itemView) {
            super(itemView);

            add_business_remove_address_iv = (ImageView) itemView.findViewById(R.id.add_business_remove_address_iv);
            business_address_restriction_cb = (CheckBox) itemView.findViewById(R.id.business_address_restriction_cb);
            business_address_location_sp = (Spinner) itemView.findViewById(R.id.business_address_location_sp);
            business_address_street_sp = (Spinner) itemView.findViewById(R.id.business_address_street_sp);
            business_address_city_sp = (Spinner) itemView.findViewById(R.id.business_address_city_sp);
            business_address_state_sp = (Spinner) itemView.findViewById(R.id.business_address_state_sp);
            business_address_zip_sp = (Spinner) itemView.findViewById(R.id.business_address_zip_sp);
            business_address_house_et = (EditText) itemView.findViewById(R.id.business_address_house_et);
            business_address_apt_et = (EditText) itemView.findViewById(R.id.business_address_apt_et);
        }
    }


    public interface OnLocationSelection{
        void onLocationSelected(ModelLocationDetail modelLocationDetail);
    }
}
