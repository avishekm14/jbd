package com.karmickdroid.jewishbusinessdirectory.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessAddressLayoutModel;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessPhoneLayoutModel;
import com.karmickdroid.jewishbusinessdirectory.model.ModelCityStateStreetTelZip;
import com.karmickdroid.jewishbusinessdirectory.model.ModelLocationDetail;

import java.util.ArrayList;

/**
 * Created by administrator on 13/6/17.
 */

public class AddBusinessPhoneLayoutAdapter extends RecyclerView.Adapter<AddBusinessPhoneLayoutAdapter.Holder> {

    Context context;
    ArrayList<AddBusinessPhoneLayoutModel> addBusinessPhoneLayoutModels;
    LayoutInflater inflater;

    public AddBusinessPhoneLayoutAdapter(Context context, ArrayList<AddBusinessPhoneLayoutModel> addBusinessPhoneLayoutModels) {
        this.context = context;
        this.addBusinessPhoneLayoutModels = addBusinessPhoneLayoutModels;
        inflater = LayoutInflater.from(context);
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(inflater.inflate(R.layout.inflater_business_address_phone, parent, false));
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int positionlayout) {
        final AddBusinessPhoneLayoutModel model = addBusinessPhoneLayoutModels.get(positionlayout);
        final ArrayList<ModelLocationDetail> finalModelLocationDetails = model.getModelLocationDetails();
        final ArrayList<ModelCityStateStreetTelZip> finalModelPhoneTypes = model.getPhoneTypeModels();
        holder.holderModel = model;


        if (addBusinessPhoneLayoutModels.get(positionlayout).isAddOrNot()) {
            holder.add_business_remove_phone_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_add_round));
        } else {
            holder.add_business_remove_phone_iv.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_remove_view));
        }

        holder.add_business_remove_phone_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (addBusinessPhoneLayoutModels.get(positionlayout).isAddOrNot()) {
                    if (allFieldsHasVals()) {              // // TODO: 13/6/17 need to set all other lists here in the model
                        AddBusinessPhoneLayoutModel model = new AddBusinessPhoneLayoutModel(false);
                        model.setModelLocationDetails(finalModelLocationDetails);
                        model.setPhoneTypeModels(finalModelPhoneTypes);
                        addBusinessPhoneLayoutModels.add(model);
                    }
                } else {
                    addBusinessPhoneLayoutModels.remove(positionlayout);
                }

                makeAllModelsNotifyFlagTrue();
                notifyDataSetChanged();

            }
        });


        // location spinner section
        AddBusinessAddressLocationSpinnerAdapter adapter = new AddBusinessAddressLocationSpinnerAdapter(context, android.R.layout.simple_spinner_item, finalModelLocationDetails);
        holder.business_phone_location_sp.setAdapter(adapter);
        holder.business_phone_location_sp.setSelection(model.getSelectedLocationPos());

        // phone spinner section
        AddBusinessPhoneTypeSpinnerAdapter phAdapter = new AddBusinessPhoneTypeSpinnerAdapter(context, android.R.layout.simple_spinner_item,
                model.getPhoneTypeModels());
        holder.address_phone_type_sp.setAdapter(phAdapter);
        holder.address_phone_type_sp.setSelection(model.getSelectedPhoneTypePos());


        holder.business_phone_location_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                model.setSelectedLocationPos(position);
                if (model.getModelLocationDetails().get(model.getSelectedLocationPos()).getAreacode().size() > 0) {
                    holder.address_phone_area_et.setText(model.getModelLocationDetails().get(model.getSelectedLocationPos()).getAreacode().get(0).getAreacode() + "");
                    model.setAreaCode(model.getModelLocationDetails().get(model.getSelectedLocationPos()).getAreacode().get(0).getAreacode() + "");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        holder.address_phone_type_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                model.setSelectedPhoneTypePos(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        holder.business_phone_restriction_cb.setChecked(model.isDoNotDisplayPhoneChecked());
        holder.business_phone_restriction_cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                model.setDoNotDisplayPhoneChecked(isChecked);
            }
        });


        holder.address_phone_area_et.setText(model.getAreaCode());
        holder.address_phone_area_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                holder.holderModel.setAreaCode(String.valueOf(s));
            }
        });

        holder.address_phone_et.setText(model.getPhoneNo());
        holder.address_phone_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                holder.holderModel.setPhoneNo(String.valueOf(s));
            }
        });


    }

    private void makeAllModelsNotifyFlagTrue() {
        for (int i = 0; i < addBusinessPhoneLayoutModels.size(); i++) {
            addBusinessPhoneLayoutModels.get(i).setIfNotifiedByAddRemoveBtn(true);
        }
    }

    private boolean allFieldsHasVals() { // // TODO: 13/6/17 check all other fields is filled or not
        for (int i = 0; i < addBusinessPhoneLayoutModels.size(); i++) {
            if (addBusinessPhoneLayoutModels.get(i).getSelectedLocationPos() == 0 ||
                    addBusinessPhoneLayoutModels.get(i).getSelectedPhoneTypePos() == 0
                    ) {
                return false;
            } else if (addBusinessPhoneLayoutModels.get(i).getAreaCode().equals("") ||
                    addBusinessPhoneLayoutModels.get(i).getPhoneNo().equals("")
                    ) {
                return false;
            }
        }
        return true;
    }


    public void notifyAdapter(ArrayList<ModelLocationDetail> modelLocationDetailsForTelephone){
        for (int i=0;i<addBusinessPhoneLayoutModels.size();i++) {
            this.addBusinessPhoneLayoutModels.get(i).setModelLocationDetails(modelLocationDetailsForTelephone);
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return addBusinessPhoneLayoutModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        ImageView add_business_remove_phone_iv;
        Spinner business_phone_location_sp, address_phone_type_sp;
        EditText address_phone_area_et, address_phone_et;
        CheckBox business_phone_restriction_cb;
        AddBusinessPhoneLayoutModel holderModel;

        public Holder(View itemView) {
            super(itemView);

            add_business_remove_phone_iv = (ImageView) itemView.findViewById(R.id.add_business_remove_phone_iv);
            business_phone_restriction_cb = (CheckBox) itemView.findViewById(R.id.business_phone_restriction_cb);
            business_phone_location_sp = (Spinner) itemView.findViewById(R.id.business_phone_location_sp);
            address_phone_type_sp = (Spinner) itemView.findViewById(R.id.address_phone_type_sp);
            address_phone_area_et = (EditText) itemView.findViewById(R.id.address_phone_area_et);
            address_phone_et = (EditText) itemView.findViewById(R.id.address_phone_et);
        }
    }
}
