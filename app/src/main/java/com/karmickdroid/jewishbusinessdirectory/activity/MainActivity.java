package com.karmickdroid.jewishbusinessdirectory.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.adapter.CategoriesSpinnerAdapter;
import com.karmickdroid.jewishbusinessdirectory.adapter.CustomLocationAdapter;
import com.karmickdroid.jewishbusinessdirectory.adapter.CustomRecyclerAdapter;
import com.karmickdroid.jewishbusinessdirectory.debug.DBG;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;
import com.karmickdroid.jewishbusinessdirectory.helper.CONST;
import com.karmickdroid.jewishbusinessdirectory.helper.MarginDecorator;
import com.karmickdroid.jewishbusinessdirectory.icenet.Body;
import com.karmickdroid.jewishbusinessdirectory.icenet.Header;
import com.karmickdroid.jewishbusinessdirectory.icenet.IceNet;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestCallback;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestError;
import com.karmickdroid.jewishbusinessdirectory.intrface.RecyclerItemClickListener;
import com.karmickdroid.jewishbusinessdirectory.model.BusinessCategories;
import com.karmickdroid.jewishbusinessdirectory.model.LocationModel;
import com.viethoa.RecyclerViewFastScroller;
import com.viethoa.models.AlphabetItem;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;

public class MainActivity extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, CustomLocationAdapter.OnLocationItemClicked {

    private DrawerLayout drawer;
    private NavigationView navigationView;
    private ImageView drawer_toggle, iv_search, iv_submit;
    private EditText et_search;
    //private Spinner sp_selected_place;
    private TextView sp_browse_categories;
    private RelativeLayout rl_home_tab, rl_menu_tab, rl_add_tab, rl_browse_categories;
    private List<LocationModel> locationModel = new ArrayList<>();
    private List<BusinessCategories> businessCategories = new ArrayList<>();
    //private CustomSpinnerAdapter customAdapter;
    private CategoriesSpinnerAdapter categoryAdapter;
    private String location_id = "", categories_id = "";
    DisplayMetrics displayMetrics = new DisplayMetrics();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //addBlankModelToLocationAndCategoryLists();

        setContentView(R.layout.main_container_layout_new);
        init();
    }


    private void init() {
        /*drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        drawer_toggle = (ImageView) findViewById(R.id.drawer_toggle);
        setUpNavigationDrawer();*/
        iv_search = (ImageView) findViewById(R.id.iv_search);
        iv_submit = (ImageView) findViewById(R.id.iv_submit);
        et_search = (EditText) findViewById(R.id.et_search);
        sp_browse_categories = (TextView) findViewById(R.id.sp_browse_categories);
        rl_browse_categories = (RelativeLayout) findViewById(R.id.rl_browse_categories);
        /*sp_selected_place = (Spinner) findViewById(R.id.sp_selected_place);

        customAdapter = new CustomSpinnerAdapter(getApplicationContext(), locationModel, R.layout.custom_spinner_header,
                R.layout.custom_spinner_items);
        sp_selected_place.setAdapter(customAdapter);*/
        tv_selected_place = (TextView) findViewById(R.id.tv_selected_place);
        tv_selected_place.setOnClickListener(this);
        initiatePopupWindiw();

        categoryAdapter = new CategoriesSpinnerAdapter(getApplicationContext(), businessCategories, R.layout.categories_dropdown_row,
                R.layout.categorie_header_row);
//        sp_browse_categories.setAdapter(categoryAdapter);

        rl_home_tab = (RelativeLayout) findViewById(R.id.rl_home_tab);
        rl_menu_tab = (RelativeLayout) findViewById(R.id.rl_menu_tab);
        rl_add_tab = (RelativeLayout) findViewById(R.id.rl_add_tab);

        rl_home_tab.setOnClickListener(this);
        rl_menu_tab.setOnClickListener(this);
        rl_add_tab.setOnClickListener(this);
        iv_search.setOnClickListener(this);
        iv_submit.setOnClickListener(this);

//        sp_browse_categories.setCustomAdapter(categoryAdapter);
//        sp_browse_categories.setOnItemChosenListener(this);

//        sp_browse_categories.setOnItemSelectedListener(this);
//        sp_selected_place.setOnItemSelectedListener(this);

        rl_browse_categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog();
            }
        });
        getBusinessCategoryData();
    }

    private void getDialog() {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this, R.style.AppTheme);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_recyclerview, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog alertDialog = dialogBuilder.create();

        RecyclerViewFastScroller fast_scroller = (RecyclerViewFastScroller) dialogView.findViewById(R.id.fast_scroller);
        RecyclerView recyclerView = (RecyclerView) dialogView.findViewById(R.id.recyclervw);
        ImageView iv_close = (ImageView) dialogView.findViewById(R.id.iv_close);
        CustomRecyclerAdapter customRecyclerAdapter = new CustomRecyclerAdapter(businessCategories);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        fast_scroller.setRecyclerView(recyclerView);
        recyclerView.setAdapter(customRecyclerAdapter);

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.cancel();
            }
        });

        generateAlphabetItem(businessCategories, fast_scroller);
//        recyclerView.setIndexTextSize(12);
//        recyclerView.setIndexBarColor("#33334c");
//        recyclerView.setIndexBarCornerRadius(0);
//        recyclerView.setIndexBarTransparentValue((float) 0.4);
//        recyclerView.setIndexbarMargin(0);
//        recyclerView.setIndexbarWidth(40);
//        recyclerView.setPreviewPadding(0);
//        recyclerView.setIndexBarTextColor("#FFFFFF");
//
//        recyclerView.setIndexBarVisibility(true);
//        recyclerView.setIndexbarHighLateTextColor("#33334c");
//        recyclerView.setIndexBarHighLateTextVisibility(true);

//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(MainActivity.this);
//        recyclerView.setLayoutManager(mLayoutManager);


//        FastScroller fastScroller = (FastScroller) dialogView.findViewById(R.id.fastscroll);


        //has to be called AFTER RecyclerView.setAdapter()
//        fastScroller.setRecyclerView(recyclerView);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(MainActivity.this, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever
//                        if (!businessCategories.get(position).getBusiness_category_id().equalsIgnoreCase("0")) {
                        categories_id = businessCategories.get(position).getBusiness_category_id();
                        sp_browse_categories.setText(businessCategories.get(position).getBusiness_category_name());
                        alertDialog.cancel();
//                        }
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        Window window = alertDialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.CENTER | Gravity.CENTER_HORIZONTAL;
        Log.v("sp_browse_categories", "" + sp_browse_categories.getY());


        ;
        // dialog.show();

        // window.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL); // set alert dialog in Bottom

        // Cancel Button
//        Button cancel_btn = (Button) dialogView.findViewById(R.id.buttoncancellist);
//        cancel_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.hide();
//            }
//        });

        alertDialog.show();
    }

    private void generateAlphabetItem(List<BusinessCategories> businessCategories, RecyclerViewFastScroller fast_scroller) {
        ArrayList<AlphabetItem> mAlphabetItems = new ArrayList<>();
        List<String> strAlphabets = new ArrayList<>();
        for (int i = 0; i < businessCategories.size(); i++) {
            String name = businessCategories.get(i).getBusiness_category_name();
            if (name == null || name.trim().isEmpty())
                continue;

            String word = name.substring(0, 1);
            if (!strAlphabets.contains(word)) {
                strAlphabets.add(word);
                mAlphabetItems.add(new AlphabetItem(i, word, false));
            }
        }
        fast_scroller.setUpAlphabet(mAlphabetItems);
    }

    private void setUpNavigationDrawer() {
        drawer_toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (drawer.isDrawerOpen(Gravity.END)) {
                    drawer.closeDrawer(Gravity.END);
                } else {
                    drawer.openDrawer(Gravity.END);
                }
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onPostCreate(savedInstanceState);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_home_tab:
                //Tab 1 click function goes from here
                refreshFields();
                break;
            case R.id.rl_menu_tab:
                //Tab 2 click function goes from here
                searchContact(categories_id, getTrim(R.id.et_search));
                break;
            case R.id.rl_add_tab:
                //Tab 3 click function goes from here
                startActivity(new Intent(MainActivity.this, AddContactActivity.class));
                break;
            case R.id.iv_search:
                searchContact("", getTrim(R.id.et_search));
                break;
            case R.id.iv_submit:
                searchContact(categories_id, "");
                break;

            case R.id.tv_selected_place:
                if (!backupIds.equals("")) {
                    //selectAllOtherPlaces();
                    filteredPlacesList(backupIds);
                    //backupIds = "";

                    customPopupLocationList_btnAll.setTag(false);
                    customPopupLocationList_btnAll.setText(getResources().getString(R.string.deselect_all));
                } else {
                    //filteredPlacesList(ids);
                }


                //open the popup window here
                popUpWindow.showAtLocation(tv_selected_place, Gravity.BOTTOM | Gravity.END, 0,
                        tv_selected_place.getTop() - 60);

                break;

            case R.id.customPopupLocationList_btnDone:
                //ids = "";

                ids = getSelectedLocationIds();

                if (!ids.equals("")) {
                    backupIds = ids;
                    //sortEventListBasedOnDate(backupIds);
                    popUpWindow.dismiss();
                } else {
                    /*final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this, R.style.alertDialog)
                            .setTitle(getResources().getString(R.string.alert))
                            .setMessage(getResources().getString(R.string.deselect_all_empty_msg))
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filteredPlacesList(backupIds);
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();*/

                    Toast.makeText(MainActivity.this, getResources().getString(R.string.deselect_all_empty_msg), Toast.LENGTH_LONG).show();
                }
                location_id = ids;
                Paper.book().write(CONST.NAME_VALUE_PAIR.LOCATIONS, locationModel);     // write again the location list to paper db
                //deselectAllOtherPlaces();
                break;

            case R.id.customPopupLocationList_btnAll:

                if ((boolean) customPopupLocationList_btnAll.getTag()) {
                    selectAllOtherPlaces();
                    placesAdapter.notifyAdapter(locationModel);
                    customPopupLocationList_btnAll.setTag(false);
                    customPopupLocationList_btnAll.setText(getResources().getString(R.string.deselect_all));
                } else {
                    deselectAllOtherPlaces();
                    placesAdapter.notifyAdapter(locationModel);
                    customPopupLocationList_btnAll.setTag(true);
                    customPopupLocationList_btnAll.setText(getResources().getString(R.string.select_all));

                }
                break;
        }
    }


    private void searchContact(String categories_id, String search_text) {
        AppUtilities.hideSoftInputMode(MainActivity.this, et_search);

        Intent i = new Intent(MainActivity.this, SearchResultActivityNew.class);
        i.putExtra(CONST.NAME_VALUE_PAIR.LOCATION_ID, getSelectedLocationIds());
        i.putExtra(CONST.NAME_VALUE_PAIR.CATEGORIES_ID, categories_id);
        i.putExtra(CONST.NAME_VALUE_PAIR.SEARCH_TEXT, search_text);
        startActivity(i);
    }

    private void getLocationData() {
        locationModel.clear();
        //locationModel.add(new LocationModel("0", getResources().getString(R.string.select_a_location), false));

        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);
        DBG.d("AAAAAA", "" + bodyRequest.getBody().toString());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .get(header)
                .pathUrl(CONST.REST_API.GET_LOCATION)
                .fromString(false)      // to stop showing loader
                .execute(MainActivity.this, "getLocationData", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("AAAAAA", "getLocationData - " + (String) o);
                        try {
                            JSONObject result = new JSONObject((String) o);

                            if (result.getBoolean("success")) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<LocationModel>>() {
                                }.getType();

                                String data = result.getJSONArray("details").toString();
                                List<LocationModel> mLocationModels = gson.fromJson(data, listType);
                                locationModel.addAll(mLocationModels);
                                placesAdapter.notifyAdapter(locationModel);
//                                customAdapter.notifyDataChanged(locationModel);
                                Paper.book().write(CONST.NAME_VALUE_PAIR.LOCATIONS, locationModel);
                            } else {
                                Toast.makeText(MainActivity.this, "" + result.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void getBusinessCategoryData() {
        businessCategories.clear();
//        businessCategories.add(new BusinessCategories("0", getResources().getString(R.string.browse_for_categories)));

        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);
        DBG.d("AAAAAA", "" + bodyRequest.getBody().toString());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .get(header)
                .pathUrl(CONST.REST_API.GET_BUSINESS_CATEGORIES)
                .fromString()
                .execute(MainActivity.this, "getLocationData", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("AAAAAA", "GET_BUSINESS_CATEGORIES - " + (String) o);
                        try {
                            JSONObject result = new JSONObject((String) o);

                            if (result.getBoolean("success")) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<BusinessCategories>>() {
                                }.getType();

                                String data = result.getJSONArray("details").toString();
                                List<BusinessCategories> mBusinessCategories = gson.fromJson(data, listType);
                                businessCategories.addAll(mBusinessCategories);
                                categoryAdapter.notifyDataChanged(businessCategories);
                                Paper.book().write(CONST.NAME_VALUE_PAIR.CATEGORY, businessCategories);
                            } else {
                                Toast.makeText(MainActivity.this, "" + result.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                            getLocationData();
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.sp_browse_categories:
                if (!businessCategories.get(i).getBusiness_category_id().equalsIgnoreCase("0")) {
                    categories_id = businessCategories.get(i).getBusiness_category_id();
                }
                break;
            /*case R.id.sp_selected_place:
                if (!locationModel.get(i).getLocation_id().equalsIgnoreCase("0")) {
                    location_id = locationModel.get(i).getLocation_id();
                }
                break;*/
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void addBlankModelToLocationAndCategoryLists() {
        //locationModel.add(new LocationModel("0", getResources().getString(R.string.select_a_location), false));
        businessCategories.add(new BusinessCategories("0", getResources().getString(R.string.browse_for_categories)));
    }

    private void refreshFields() {
        et_search.setText("");
        //change
//        sp_browse_categories.setSelection(0);
//        sp_selected_place.setSelection(0);
        location_id = "";
        categories_id = "";
        deselectAllOtherPlaces();
        customPopupLocationList_btnAll.setTag(true);
        customPopupLocationList_btnAll.setText(getResources().getString(R.string.select_all));
        //addBlankModelToLocationAndCategoryLists();
    }


    // location popup section
    private PopupWindow popUpWindow;

    View customPopupView;
    RecyclerView customPopupLocationList_rvLocation;
    Button customPopupLocationList_btnDone;
    Button customPopupLocationList_btnAll;
    CustomLocationAdapter placesAdapter;
    TextView tv_selected_place;

    String ids = "";
    String backupIds = "";

    private void initiatePopupWindiw() {
        // popup section
        //popupMenu = new PopupMenu(getActivity(), header_tv_location);
        customPopupView = LayoutInflater.from(this).inflate(R.layout.cutom_popup_location_list, null);
        customPopupLocationList_btnAll = (Button) customPopupView.findViewById(R.id.customPopupLocationList_btnAll);
        customPopupLocationList_btnAll.setOnClickListener(this);
        customPopupLocationList_btnAll.setTag(true);        // true means select, false means deselect

        customPopupLocationList_rvLocation = (RecyclerView) customPopupView.findViewById(R.id.customPopupLocationList_rvLocation);
        placesAdapter = new CustomLocationAdapter(this, locationModel);
        customPopupLocationList_rvLocation.setLayoutManager(new LinearLayoutManager(this));
        customPopupLocationList_rvLocation.addItemDecoration(new MarginDecorator(this));
        customPopupLocationList_rvLocation.setAdapter(placesAdapter);
        customPopupLocationList_btnDone = (Button) customPopupView.findViewById(R.id.customPopupLocationList_btnDone);
        customPopupLocationList_btnDone.setOnClickListener(this);

        popUpWindow = new PopupWindow(customPopupView,
                Math.round(getResources().getDimension(R.dimen._150sdp)),
                LinearLayoutCompat.LayoutParams.WRAP_CONTENT);


        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            popUpWindow.setElevation(10.0f);
        }
        popUpWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popUpWindow.setFocusable(true);
        popUpWindow.setOutsideTouchable(true);
        popUpWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popUpWindow.dismiss();
                    return true;
                }
                return false;
            }
        });
        // end popup window
    }


    private void filteredPlacesList(String ids) {
        String[] locationIds = ids.split("~");

        //placesModelArrayList.clear();
        for (int i = 0; i < locationModel.size(); i++) {
            LocationModel model = locationModel.get(i);
            for (int l = 0; l < locationIds.length; l++) {
                if (model.getLocation_id().equals(locationIds[l])) {
                    locationModel.get(i).setSelected(true);
                }
            }
        }
        placesAdapter.notifyAdapter(locationModel);
    }

    private String getSelectedLocationIds() {
        String id = "";
        for (int i = 0; i < locationModel.size(); i++) {
            if (locationModel.get(i).isSelected()) {
                if (id.equals("")) {
                    id = locationModel.get(i).getLocation_id();
                } else {
                    id = id + "~" + locationModel.get(i).getLocation_id();
                }
            }
        }
        return id;
    }

    private void deselectAllOtherPlaces() {
        for (int i = 0; i < locationModel.size(); i++) {
            locationModel.get(i).setSelected(false);
        }
        placesAdapter.notifyAdapter(locationModel);
    }

    private void selectAllOtherPlaces() {
        for (int i = 0; i < locationModel.size(); i++) {
            locationModel.get(i).setSelected(true);
        }
        placesAdapter.notifyAdapter(locationModel);
    }

    @Override
    public void onLocationItemClicked() {
        if (ifAnyLocationIsDeSelected()) {
            customPopupLocationList_btnAll.setTag(true);
            customPopupLocationList_btnAll.setText(getResources().getString(R.string.select_all));
        } else {
            customPopupLocationList_btnAll.setTag(false);
            customPopupLocationList_btnAll.setText(getResources().getString(R.string.deselect_all));
        }
    }

    private boolean ifAnyLocationIsDeSelected() {
        for (int i = 0; i < locationModel.size(); i++) {
            if (!locationModel.get(i).isSelected()) {
                return true;
            }
        }
        return false;
    }
    //change

//    @Override
//    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
//        switch (adapterView.getId()) {
//            case R.id.sp_browse_categories:
//                if (!businessCategories.get(position).getBusiness_category_id().equalsIgnoreCase("0")) {
//                    categories_id = businessCategories.get(position).getBusiness_category_id();
//                }
//                break;
//            /*case R.id.sp_selected_place:
//                if (!locationModel.get(i).getLocation_id().equalsIgnoreCase("0")) {
//                    location_id = locationModel.get(i).getLocation_id();
//                }
//                break;*/
//        }
//    }
//
//    @Override
//    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {
//
//    }

    // location popup section


}
