package com.karmickdroid.jewishbusinessdirectory.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.adapter.ViewPagerAdapter;
import com.karmickdroid.jewishbusinessdirectory.fragment.AddBusinessFragment;
import com.karmickdroid.jewishbusinessdirectory.fragment.AddBusinessFragmentNew;
import com.karmickdroid.jewishbusinessdirectory.fragment.AddResidentialFragment;
import com.karmickdroid.jewishbusinessdirectory.fragment.DemoFragment;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;
import com.karmickdroid.jewishbusinessdirectory.helper.CONST;

public class AddContactActivity extends AppCompatActivity implements View.OnClickListener {
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private RelativeLayout rl_home_tab, rl_menu_tab, rl_add_tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        initView();
    }

    /**
     * Initializing view elements
     */
    private void initView() {
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);
        setupViewPager(viewPager);
        setupTabIcons();

        rl_home_tab = (RelativeLayout) findViewById(R.id.rl_home_tab);
        rl_menu_tab = (RelativeLayout) findViewById(R.id.rl_menu_tab);
        rl_add_tab = (RelativeLayout) findViewById(R.id.rl_add_tab);

        rl_home_tab.setOnClickListener(this);
        rl_menu_tab.setOnClickListener(this);
        rl_add_tab.setOnClickListener(this);
    }

    private void setupTabIcons() {

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(android.R.layout.test_list_item, null);
        tabOne.setText(AppUtilities.getStringFromResource(AddContactActivity.this, R.string.business));
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_business_toolbar, 0, 0);
        tabOne.setTextColor(getResources().getColor(R.color.toolbarTextColor));
        tabLayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(android.R.layout.test_list_item, null);
        tabTwo.setText(AppUtilities.getStringFromResource(AddContactActivity.this, R.string.residential));
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_residential_toolbar, 0, 0);
        tabTwo.setTextColor(getResources().getColor(R.color.toolbarTextColor));
        tabLayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(android.R.layout.test_list_item, null);
        tabThree.setText(AppUtilities.getStringFromResource(AddContactActivity.this, R.string.community));
        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_community_toolbar, 0, 0);
        tabThree.setTextColor(getResources().getColor(R.color.toolbarTextColor));
        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    /**
     * Set up view pager
     *
     * @param viewPager
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new AddBusinessFragmentNew(), AppUtilities.getStringFromResource(AddContactActivity.this, R.string.business));
        adapter.addFragment(new AddResidentialFragment(), AppUtilities.getStringFromResource(AddContactActivity.this, R.string.residential));
        adapter.addFragment(new DemoFragment(), AppUtilities.getStringFromResource(AddContactActivity.this, R.string.community));
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_home_tab:
                Intent i = new Intent(AddContactActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                break;
            case R.id.rl_menu_tab:
                //Tab 2 click function goes from here
                Intent intent = new Intent(AddContactActivity.this, SearchResultActivityNew.class);
                intent.putExtra(CONST.NAME_VALUE_PAIR.LOCATION_ID, "");
                intent.putExtra(CONST.NAME_VALUE_PAIR.CATEGORIES_ID, "");
                intent.putExtra(CONST.NAME_VALUE_PAIR.SEARCH_TEXT, "");
                startActivity(intent);
                break;
            case R.id.rl_add_tab:
                //Tab 3 click function goes from here
                break;
        }
    }
}
