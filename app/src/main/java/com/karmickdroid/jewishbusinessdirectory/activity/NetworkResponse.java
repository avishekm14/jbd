package com.karmickdroid.jewishbusinessdirectory.activity;

import com.android.volley.VolleyError;

/**
 * Copyright (C) Karmick Solution Pvt. Ltd. on 30/3/16.
 */
public interface NetworkResponse {
    void onSuccessResponse(String response);
    void onErrorResponse(VolleyError volleyError);
}
