package com.karmickdroid.jewishbusinessdirectory.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentProviderOperation;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.adapter.AccountDetailsAdapter;
import com.karmickdroid.jewishbusinessdirectory.debug.DBG;
import com.karmickdroid.jewishbusinessdirectory.dialogs.Dialogs;
import com.karmickdroid.jewishbusinessdirectory.gateWay.NetworkUtil;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;
import com.karmickdroid.jewishbusinessdirectory.helper.CONST;
import com.karmickdroid.jewishbusinessdirectory.helper.MarginDecorator;
import com.karmickdroid.jewishbusinessdirectory.icenet.Body;
import com.karmickdroid.jewishbusinessdirectory.icenet.Header;
import com.karmickdroid.jewishbusinessdirectory.icenet.IceNet;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestCallback;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestError;
import com.karmickdroid.jewishbusinessdirectory.model.AccountDetailsTelephoneAddressModel;
import com.karmickdroid.jewishbusinessdirectory.model.CustomImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class AccountDetailsActivity extends BaseActivity implements View.OnClickListener, AccountDetailsAdapter.OnRowClicked {

    private RelativeLayout rl_container;
    private String account_id;
    private ArrayList<String> images_list = new ArrayList<>();

    private RecyclerView business_detail_rv;
    private TextView business_detail_tv_businessName, business_detail_tv_accountTypeName,
            business_detail_tv_category, business_detail_tv_businessFatherNameTitle,
            business_detail_tv_businessFatherName, business_detail_tv_businessMaidenNameTitle,
            business_detail_tv_businessMaidenName;

    private String account_type_name = "", account_type = "", business_name = "", account_id_response = "",
            business_category_name = "", father_name = "",
            maidan_name = "";

    private AccountDetailsAdapter adapter;
    private ArrayList<AccountDetailsTelephoneAddressModel> models = new ArrayList<>();

    private RelativeLayout rl_back;
    private RelativeLayout rl_home_tab, rl_menu_tab, rl_add_tab;

    private TextView business_detail_add_business_tv, business_detail_reportit_tv;
    private TextView business_detail_directory_ads_tv, business_detail_add_to_contact_tv, business_detail_share_tv;

    String selectedPhoneNumber = "";
    LinearLayout imageLayout;

    HorizontalScrollView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_details);

        if (getIntent().getExtras() != null) {
            account_id = getIntent().getExtras().getString("account_id");
        }

        initViews();
        getAccountDetails();
    }

    private void initViews() {
        rl_container = (RelativeLayout) findViewById(R.id.rl_container);
        imageLayout = (LinearLayout) findViewById(R.id.imageLayout);
        sv = (HorizontalScrollView) findViewById(R.id.sv);
        business_detail_tv_businessName = (TextView) findViewById(R.id.business_detail_tv_businessName);
        business_detail_tv_accountTypeName = (TextView) findViewById(R.id.business_detail_tv_accountTypeName);
        business_detail_tv_category = (TextView) findViewById(R.id.business_detail_tv_category);
        business_detail_tv_businessFatherNameTitle = (TextView) findViewById(R.id.business_detail_tv_businessFatherNameTitle);
        business_detail_tv_businessFatherName = (TextView) findViewById(R.id.business_detail_tv_businessFatherName);
        business_detail_tv_businessMaidenNameTitle = (TextView) findViewById(R.id.business_detail_tv_businessMaidenNameTitle);
        business_detail_tv_businessMaidenName = (TextView) findViewById(R.id.business_detail_tv_businessMaidenName);

        business_detail_rv = (RecyclerView) findViewById(R.id.business_detail_rv);
        business_detail_rv.setLayoutManager(new LinearLayoutManager(this));
        //to remove the line in between
        business_detail_rv.addItemDecoration(new MarginDecorator(this));
        adapter = new AccountDetailsAdapter(this, models);
        adapter.onRowClickedCallBack = this;
        business_detail_rv.setAdapter(adapter);

        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);
        rl_home_tab = (RelativeLayout) findViewById(R.id.rl_home_tab);
        rl_menu_tab = (RelativeLayout) findViewById(R.id.rl_menu_tab);
        rl_add_tab = (RelativeLayout) findViewById(R.id.rl_add_tab);

        rl_home_tab.setOnClickListener(this);
        rl_menu_tab.setOnClickListener(this);
        rl_add_tab.setOnClickListener(this);


        business_detail_directory_ads_tv = (TextView) findViewById(R.id.business_detail_directory_ads_tv);
        business_detail_directory_ads_tv.setOnClickListener(this);
        business_detail_add_to_contact_tv = (TextView) findViewById(R.id.business_detail_add_to_contact_tv);
        business_detail_add_to_contact_tv.setOnClickListener(this);
        business_detail_share_tv = (TextView) findViewById(R.id.business_detail_share_tv);
        business_detail_share_tv.setOnClickListener(this);

        business_detail_add_business_tv = (TextView) findViewById(R.id.business_detail_add_business_tv);
        business_detail_add_business_tv.setOnClickListener(this);
        business_detail_reportit_tv = (TextView) findViewById(R.id.business_detail_reportit_tv);
        business_detail_reportit_tv.setOnClickListener(this);

        rl_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageLayout.setVisibility(View.GONE);
                sv.setVisibility(View.GONE);
            }
        });
    }

    private void getAccountDetails() {
        if (!NetworkUtil.isConnected(AccountDetailsActivity.this)) {
            Snackbar.make(rl_container,
                    AppUtilities.getStringFromResource(AccountDetailsActivity.this
                            , R.string.net_check_error), Snackbar.LENGTH_LONG).show();
            return;
        }


        Body.Builder builder = new Body.Builder()
                .add(CONST.NAME_VALUE_PAIR.account_id, account_id);
        Body bodyRequest = new Body(builder);
        DBG.d("details params", "" + bodyRequest.getBody().toString());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.GET_ACCOUNT_DETAILS_API)
                .fromString()
                .execute(AccountDetailsActivity.this, "getaAccountDetailsResult", new RequestCallback() {
                    @Override

                    public void onRequestSuccess(Object o) {

                        try {
                            JSONObject result = new JSONObject((String) o);
                            DBG.d("details", "Get account detail - " + o.toString());

                            if (result.getBoolean("success")) {

                                JSONArray detailsArr = result.getJSONArray("details");
                                if (detailsArr.length() > 0) {
                                    for (int i = 0; i < detailsArr.length(); i++) {
                                        JSONObject obj = detailsArr.getJSONObject(i);

                                        if (obj.has("account_type_name")) {
                                            if (account_type_name.equals(""))
                                                account_type_name = obj.optString("account_type_name");
                                            else
                                                account_type_name = account_type_name + obj.optString("account_type_name");

                                        }
                                        if (obj.has("account_type")) {
                                            account_type = obj.optString("account_type");
                                        }


                                        if (obj.has("business_category_name")) {
                                            if (business_category_name.equals(""))
                                                business_category_name = obj.optString("business_category_name");
                                            else
                                                business_category_name = business_category_name + ", " + obj.optString("business_category_name");
                                        }

                                    }

                                    if (account_type.equals("1")) {     // residential
                                        business_name = detailsArr.getJSONObject(0).optString("title_name") + " " + detailsArr.getJSONObject(0).optString("first_name") + " " + detailsArr.getJSONObject(0).optString("last_name");
                                        father_name = detailsArr.getJSONObject(0).optString("father_name");
                                        maidan_name = detailsArr.getJSONObject(0).optString("maidan_name");
                                    } else if (account_type.equals("2")) {     // business
                                        business_name = detailsArr.getJSONObject(0).optString("business_name");
                                        father_name = "";
                                        maidan_name = "";
                                    } else {
                                        business_name = detailsArr.getJSONObject(0).optString("business_name");
                                        father_name = "";
                                        maidan_name = "";
                                    }


                                }

                                //if type resedential hide business_detail_directory_ads_tv
                                //account_id==1 residential
                                if (account_type.equals("1")) {
                                    business_detail_directory_ads_tv.setVisibility(View.GONE);
                                } else {
                                    business_detail_directory_ads_tv.setVisibility(View.VISIBLE);
                                }

                                business_detail_directory_ads_tv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (images_list.size() == 0)
                                            directoryAdsImageAPI(account_id);
                                        else
                                            generateImageSlider();

                                    }
                                });

                                // telephone and address section
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<AccountDetailsTelephoneAddressModel>>() {
                                }.getType();

                                String data = result.getJSONArray("details_telephone_address").toString();
                                models = gson.fromJson(data, listType);


                                setTheValues();

                            } else {
                                Snackbar.make(rl_container, result.getString("message"), Snackbar.LENGTH_LONG).show();
                            }

                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private List<CustomImage> getImages() {
        List<CustomImage> list = new ArrayList<>();
        list.add(new CustomImage(getResources().getDrawable(R.drawable.img_1)));
        list.add(new CustomImage(getResources().getDrawable(R.drawable.img_2)));
        list.add(new CustomImage(getResources().getDrawable(R.drawable.img_3)));
        list.add(new CustomImage(getResources().getDrawable(R.drawable.img_4)));
        list.add(new CustomImage(getResources().getDrawable(R.drawable.img_5)));
        list.add(new CustomImage(getResources().getDrawable(R.drawable.img_6)));
        return list;
    }

    private void setTheValues() {
        business_detail_tv_businessFatherNameTitle.setText(getResources().getString(R.string.ben_reb));
        business_detail_tv_businessFatherName.setText("" + father_name);
        business_detail_tv_businessMaidenNameTitle.setText(getResources().getString(R.string.maiden));
        business_detail_tv_businessMaidenName.setText("" + maidan_name);


        if (father_name.trim().equals("")) {
            business_detail_tv_businessFatherNameTitle.setVisibility(View.GONE);
            business_detail_tv_businessFatherName.setVisibility(View.GONE);
        } else {
            business_detail_tv_businessFatherNameTitle.setVisibility(View.VISIBLE);
            business_detail_tv_businessFatherName.setVisibility(View.VISIBLE);
        }
        if (maidan_name.trim().equals("")) {
            business_detail_tv_businessMaidenNameTitle.setVisibility(View.GONE);
            business_detail_tv_businessMaidenName.setVisibility(View.GONE);
        } else {
            business_detail_tv_businessMaidenNameTitle.setVisibility(View.VISIBLE);
            business_detail_tv_businessMaidenName.setVisibility(View.VISIBLE);
        }

        business_detail_tv_businessName.setText(business_name.trim());
        if (account_type_name.equals("Residential")) {
            business_detail_tv_accountTypeName.setText(account_type_name);
            business_detail_tv_accountTypeName.setVisibility(View.GONE);
        } else {
            business_detail_tv_accountTypeName.setText(account_type_name + ": ");
            business_detail_tv_category.setText(business_category_name);
            business_detail_tv_accountTypeName.setVisibility(View.GONE);
        }

//        jvhjkhkljolj

        adapter.notifyAdapter(models);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_back:
                finish();
                break;
            case R.id.rl_home_tab:
                Intent i = new Intent(AccountDetailsActivity.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                break;
            case R.id.rl_menu_tab:
                //Tab 2 click function goes from here
                finish();
                break;
            case R.id.rl_add_tab:
                //Tab 3 click function goes from here
                startActivity(new Intent(AccountDetailsActivity.this, AddContactActivity.class));
                break;

            case R.id.business_detail_add_business_tv:
                startActivity(new Intent(AccountDetailsActivity.this, AddContactActivity.class));
                break;

            case R.id.business_detail_reportit_tv:
                Dialogs.ReportErrorWithCloseButton(AccountDetailsActivity.this, rl_container,
                        new Dialogs.OnErrorReportMsgBtnClickListener() {
                            @Override
                            public void onErrorReportMsgBtnClickListener(String reportMsg) {
                                // api call for report error

                                reportAPI(reportMsg + "\n\nConcerned Post:\n" + business_name.trim() + "\n" + getShareData(models) + "\n Account Id: " + account_id);
                            }


                        });
                break;
            case R.id.business_detail_directory_ads_tv:

                break;
            case R.id.business_detail_add_to_contact_tv:
                getPermissionToAddContact();
//                addToContact();
                break;
            case R.id.business_detail_share_tv:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, business_name.trim() + "\n" + getShareData(models));
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
        }
    }


    ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();

    private void addToContact() {
        ops.clear();
        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        if (business_name != null) {
            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(
                            ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                            business_name).build());
        }

        //------------------------------------------------------ Mobile Number
        if (models.size() > 1) {
            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, models.get(1).getArea_code() + "" + models.get(1).getTelephone_no())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }
        /*//------------------------------------------------------ Home Numbers
        if (models.size() > 2) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, models.get(2).getArea_code() + "" + models.get(2).getTelephone_no())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_HOME)
                    .build());
        }

        //------------------------------------------------------ Work Numbers
        if (models.size() > 3) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, models.get(3).getArea_code() + "" + models.get(3).getTelephone_no())
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_WORK)
                    .build());
        }*/

        // Asking the Contact provider to create a new contact
        try {
            getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
            Snackbar.make(rl_container,
                    AppUtilities.getStringFromResource(AccountDetailsActivity.this
                            , R.string.contact_successfully_added), Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Identifier for the permission request
    private static final int ADD_CONTACT_PERMISSIONS_REQUEST = 3;

    public void getPermissionToAddContact() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS)) {

                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_CONTACTS},
                        ADD_CONTACT_PERMISSIONS_REQUEST);
            }
        } else {
            addToContact();
        }

    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == ADD_CONTACT_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                addToContact();
            } else {

            }
        } else if (requestCode == CALL_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    private void directoryAdsImageAPI(String account_id) {
        if (!NetworkUtil.isConnected(AccountDetailsActivity.this)) {
            Snackbar.make(rl_container,
                    AppUtilities.getStringFromResource(AccountDetailsActivity.this
                            , R.string.net_check_error), Snackbar.LENGTH_LONG).show();
            return;
        }
//        images_list.clear();

        Body.Builder builder = new Body.Builder()
                .add(CONST.NAME_VALUE_PAIR.account_id, account_id);
//                .add(CONST.NAME_VALUE_PAIR.account_id, "305517");
        Body bodyRequest = new Body(builder);
        DBG.d("image params", "" + bodyRequest.getBody().toString());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.GET_DIRECTORY_ADS_IMAGE_API)
                .fromString()
                .execute(AccountDetailsActivity.this, "getaAccountDetailsResult", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        try {
                            DBG.d("image result ", "" + o.toString());

                            JSONObject result = new JSONObject((String) o);
                            if (result.has("success")) {
                                if (result.getBoolean("success")) {
                                    JSONArray details = result.getJSONArray("details");
                                    for (int i = 0; i < details.length(); i++) {
                                        JSONObject jsonObject = details.getJSONObject(i);
                                        String image = jsonObject.getString("image");
                                        images_list.add(image);
                                    }
                                    if (images_list.size() > 0)
                                        generateImageSlider();
                                    else
                                        Toast.makeText(AccountDetailsActivity.this, "No Images!!!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(AccountDetailsActivity.this, "No Images!!!", Toast.LENGTH_SHORT).show();
                                }
                            }

                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {
                        DBG.d("error code", "" + error.getErrorCode());
                    }
                });
    }

    private void generateImageSlider() {

        Intent intent = new Intent(AccountDetailsActivity.this, FullScreenViewActivity.class);
        intent.putStringArrayListExtra("images_list", images_list);
        startActivity(intent);
    }

    private void reportAPI(String reportMsg) {
        if (!NetworkUtil.isConnected(AccountDetailsActivity.this)) {
            Snackbar.make(rl_container,
                    AppUtilities.getStringFromResource(AccountDetailsActivity.this
                            , R.string.net_check_error), Snackbar.LENGTH_LONG).show();
            return;
        }


        Body.Builder builder = new Body.Builder()
                .add(CONST.NAME_VALUE_PAIR.comment_report, reportMsg);
        Body bodyRequest = new Body(builder);
        DBG.d("report params", "" + bodyRequest.getBody().toString());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.MAIL_REPORT_MSG_API)
                .fromString()
                .execute(AccountDetailsActivity.this, "getaAccountDetailsResult", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("Report", "report response - " + (String) o);
                        try {
                            JSONObject result = new JSONObject((String) o);

                            Snackbar.make(rl_container, result.getString("message"), Snackbar.LENGTH_LONG).show();

                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {
                        Snackbar.make(rl_container, "Report error!!", Snackbar.LENGTH_LONG).show();
                    }
                });
    }

//    @Override
//    public void onClickingOnRow(int position) {
//
//    }


    private void checkCall() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToCall();
        } else {
            makeCall();
        }
    }

    // Identifier for the permission request
    private static final int CALL_PERMISSIONS_REQUEST = 2;

    @SuppressLint("NewApi")
    public void getPermissionToCall() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {

            }

            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                    CALL_PERMISSIONS_REQUEST);
        } else {
            makeCall();
        }

    }

    private void makeCall() {
        String phones = String.format("%s", selectedPhoneNumber);
        Intent intent;
        intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phones));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    public String getShareData(ArrayList<AccountDetailsTelephoneAddressModel> models) {

        String shareData = "";

        for (int position = 0; position < models.size(); position++) {
            if (!models.get(position).getTelephone_no().equals("") && models.get(position).getTelephone_no() != null) {
                String loc_temp = models.get(position).getLocation_name();
                String loc = (loc_temp == null || loc_temp.equals("")) ? "" : loc_temp;
                String area_code_temp = models.get(position).getArea_code();
                String area_code = (area_code_temp == null || area_code_temp.equals("")) ? "" : area_code_temp;
                shareData += loc + "\n" +
                        area_code + "-"
                        + models.get(position).getTelephone_no().substring(0, 3) + "-"
                        + models.get(position).getTelephone_no().substring(3);
            } else {
                String houseNo = "", streetName = "", aptNo = "", city = "", state = "", zip = "";
                String[] add = new String[6];

//                if (models.get(position).getLocation_address().equals("2")) {
//                    shareData += this.getResources().getString(R.string.other) + "\n";
//                } else {
                String loc = models.get(position).getLocation_name();
                shareData += (loc == null || loc.equals("")) ? "" : loc + "\n";

//                }
                houseNo = models.get(position).getHouse_no();
                streetName = models.get(position).getStreet_name();
                aptNo = models.get(position).getApt_no();
                city = models.get(position).getCity_name();
                state = models.get(position).getState_code();
                zip = models.get(position).getZipcode();

                add[0] = (houseNo == null || houseNo.equals("")) ? "" : houseNo + " ";
                add[1] = (streetName == null || streetName.equals("")) ? "" : streetName + ", ";
                add[2] = (aptNo == null || aptNo.equals("")) ? "" : aptNo + ", ";
                add[3] = (city == null || city.equals("")) ? "" : city + ", ";
                add[4] = (state == null || state.equals("")) ? "" : state + " ";
                add[5] = (zip == null || zip.equals("")) ? "" : zip;

                String address = "";
                for (int j = 0; j < add.length; j++) {
                    if (!add[j].equals("")) {
                        if (address.equals("")) {
                            address = add[j];
                        } else {
                            address = address + add[j];
                        }
                    }
                }

                shareData += address + "\n";

            }
        }

        return shareData;

    }

    @Override
    public void onClickingOnRow(int position, String type) {

        if (type.equals("call")) {
            if (!models.get(position).getTelephone_no().equals("")) {
                selectedPhoneNumber = models.get(position).getArea_code() + "" + models.get(position).getTelephone_no();
                Log.v("selectedPhoneNumber", selectedPhoneNumber);
                checkCall();
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("http://maps.google.co.in/maps?q=" + type));
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }
//
//    //To hide keyboard pass parent view and activity
//    public static void hideCheck(ViewGroup viewGroup, Activity activity) {
//
//        if (viewGroup instanceof ViewGroup) {
//
//            for (int i = 0; i < ((ViewGroup) viewGroup).getChildCount(); i++) {
//                View innerView = ((ViewGroup) viewGroup).getChildAt(i);
//                setupUI(innerView, activity);
//            }
//            setupUI(viewGroup, activity);
//        }
//    }
//
//    public static void setupUI(View view, final Activity activity) {
//
//
//        //Set up touch listener for non-text box views to hide keyboard.
//        if (!(view instanceof EditText)) {
//
//            view.setOnTouchListener(new View.OnTouchListener() {
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    // TODO Auto-generated method stub
//                    hideSoftKeyboard(activity);
//                    return false;
//                }
//            });
//        }
//
//        //If a layout container, iterate over children and seed recursion.
//
//    }
//
//    public static void hideSoftKeyboard(Context context) {
//        InputMethodManager inputMethodManager =
//                (InputMethodManager) context.getSystemService(
//                        Activity.INPUT_METHOD_SERVICE);
//        inputMethodManager.hideSoftInputFromWindow(
//                ((Activity) context).getWindow().getDecorView().getRootView().getWindowToken(), 0);
//    }


}
