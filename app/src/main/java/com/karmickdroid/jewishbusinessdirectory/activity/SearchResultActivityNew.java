package com.karmickdroid.jewishbusinessdirectory.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.adapter.ContactResultListAdapter;
import com.karmickdroid.jewishbusinessdirectory.adapter.CustomLocationAdapter;
import com.karmickdroid.jewishbusinessdirectory.debug.DBG;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;
import com.karmickdroid.jewishbusinessdirectory.helper.CONST;
import com.karmickdroid.jewishbusinessdirectory.helper.MarginDecorator;
import com.karmickdroid.jewishbusinessdirectory.helper.MyDialog;
import com.karmickdroid.jewishbusinessdirectory.helper.NetworkServiceGenerator;
import com.karmickdroid.jewishbusinessdirectory.helper.RestApi;
import com.karmickdroid.jewishbusinessdirectory.icenet.Body;
import com.karmickdroid.jewishbusinessdirectory.icenet.Header;
import com.karmickdroid.jewishbusinessdirectory.icenet.IceNet;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestCallback;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestError;
import com.karmickdroid.jewishbusinessdirectory.model.Contacts;
import com.karmickdroid.jewishbusinessdirectory.model.LocationModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchResultActivityNew extends BaseActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, ContactResultListAdapter.OnRowClickListener,
        CustomLocationAdapter.OnLocationItemClicked {

    private static final String TAG = "CHECK";
    private RelativeLayout rl_all_tab, rl_business_tab, rl_residential_tab, rl_community_tab, rl_back;
    private LinearLayout parent;
    private ImageView tab_img1, tab_img2, tab_img3, tab_img4, iv_search;
    private TextView tab_text1, tab_text2, tab_text3, tab_text4;
    private EditText et_search;
    private RecyclerView rv_data_list;
    private ContactResultListAdapter mAdapter;
    //private Spinner sp_selected_place;
    //private CustomSpinnerAdapter customAdapter;
    private List<LocationModel> locationModel = new ArrayList<>();
    private List<Contacts> mContacts = new ArrayList<>();
    private String location_id = "", categories_id = "", search_text = "";
    private boolean searchForFirstTime = true;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager layoutManager;
    private int length = 40, offset = 0, totalData = 0;
    private String account_type = "";
    private String selectedPhoneNumber = "";

    private TextView tv_no_result_found;
    private ProgressBar progress;
    MyDialog mDialog = new MyDialog();

    RestApi restApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().getExtras() != null) {
            location_id = getIntent().getExtras().getString(CONST.NAME_VALUE_PAIR.LOCATION_ID);
            categories_id = getIntent().getExtras().getString(CONST.NAME_VALUE_PAIR.CATEGORIES_ID);
            search_text = getIntent().getExtras().getString(CONST.NAME_VALUE_PAIR.SEARCH_TEXT);
        }
        //locationModel.add(new LocationModel("0", getResources().getString(R.string.select_a_location),false));

        setContentView(R.layout.activity_search_result);

        restApi = NetworkServiceGenerator.createService(RestApi.class);

        init();
    }

    private void init() {
        parent = (LinearLayout) findViewById(R.id.ll_parent);
        tv_no_result_found = (TextView) findViewById(R.id.tv_no_result_found);
        //progress = (ProgressBar) findViewById(R.id.progress);

        rl_back = (RelativeLayout) findViewById(R.id.rl_back);
        rl_all_tab = (RelativeLayout) findViewById(R.id.rl_all_tab);
        rl_business_tab = (RelativeLayout) findViewById(R.id.rl_business_tab);
        rl_residential_tab = (RelativeLayout) findViewById(R.id.rl_residential_tab);
        rl_community_tab = (RelativeLayout) findViewById(R.id.rl_community_tab);

        tab_img1 = (ImageView) findViewById(R.id.tab_img1);
        tab_img2 = (ImageView) findViewById(R.id.tab_img2);
        tab_img3 = (ImageView) findViewById(R.id.tab_img3);
        tab_img4 = (ImageView) findViewById(R.id.tab_img4);
        iv_search = (ImageView) findViewById(R.id.iv_search);


        tab_text1 = (TextView) findViewById(R.id.tab_text1);
        tab_text2 = (TextView) findViewById(R.id.tab_text2);
        tab_text3 = (TextView) findViewById(R.id.tab_text3);
        tab_text4 = (TextView) findViewById(R.id.tab_text4);
        et_search = (EditText) findViewById(R.id.et_search);

        et_search.setText(search_text);     // setting the search text value from previous page
        et_search.post(new Runnable() {
            @Override
            public void run() {
                et_search.setSelection(et_search.getText().length());
            }
        });


        /*sp_selected_place = (Spinner) findViewById(R.id.sp_selected_place);
        sp_selected_place.setOnItemSelectedListener(this);

        customAdapter = new CustomSpinnerAdapter(getApplicationContext(), locationModel, R.layout.custom_spinner_header,
                R.layout.custom_spinner_header_white);
        sp_selected_place.setAdapter(customAdapter);*/

        tv_selected_place = (TextView) findViewById(R.id.tv_selected_place);
        tv_selected_place.setOnClickListener(this);


        rv_data_list = (RecyclerView) findViewById(R.id.rv_data_list);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        rv_data_list.setLayoutManager(layoutManager);
        DividerItemDecoration mDividerItemDecoration = new DividerItemDecoration(rv_data_list.getContext(), LinearLayout.VERTICAL);
        rv_data_list.addItemDecoration(mDividerItemDecoration);
        mAdapter = new ContactResultListAdapter(this, mContacts);
        mAdapter.setOnRowClickListener(this);
        rv_data_list.setAdapter(mAdapter);

        rl_back.setOnClickListener(this);
        rl_all_tab.setOnClickListener(this);
        rl_business_tab.setOnClickListener(this);
        rl_residential_tab.setOnClickListener(this);
        rl_community_tab.setOnClickListener(this);
        iv_search.setOnClickListener(this);
        selectTab(0);

        initiatePopupWindiw();



        /*for(int t =0; t<locationModel.size(); t++){
            Log.e("\ntest", String.valueOf(locationModel.get(t).isSelected()));
        }*/
        rv_data_list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = layoutManager.getChildCount();
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= (totalItemCount - (length / 2))) {
                            loading = false;
                            //Do pagination.. i.e. fetch new data
                            if (offset < totalData) {
                                DBG.d("AAAAAA", "Lazy loading call");

                                // for showing loader add a blank model in arraylist
                                Contacts model = new Contacts(true);
                                mContacts.add(model);
                                mAdapter.notifyDataChanged(mContacts);
//                                if (offset > 0)     // means not first time
                                //progress.setVisibility(View.VISIBLE);

                                getSearchResult(false, categories_id);
                            }
                        }
                    }
                }
            }
        });

        //showSnackBar();
    }

    private void showSnackBar() {
        Snackbar snackbar = Snackbar
                .make(parent, getResources().getString(R.string.load_more_data), Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_back:
                finish();
                break;
            case R.id.iv_search:
                if (getTrim(R.id.et_search).isEmpty()) {
                    Toast.makeText(this, getResources().getString(R.string.search_text_empthy), Toast.LENGTH_SHORT).show();
                } else {
                    search_text = getTrim(R.id.et_search);
                    resetList();
                    getSearchResult(true, categories_id);
                    AppUtilities.hideSoftInputMode(SearchResultActivityNew.this, et_search);
                }
                break;
            case R.id.rl_all_tab:
                selectTab(0);
                resetList();
                account_type = "";
//                et_search.setText("");
                getSearchResult(true, categories_id);
                break;
            case R.id.rl_residential_tab:
                selectTab(2);
                resetList();
                account_type = "1";
                getSearchResult(true, "");
                break;
            case R.id.rl_business_tab:
                selectTab(1);
                resetList();
                account_type = "2";
                getSearchResult(true, categories_id);
                break;
            case R.id.rl_community_tab:
                selectTab(3);
                resetList();
                account_type = "3";
                getSearchResult(true, categories_id);
                break;

            case R.id.tv_selected_place:
                if (!backupIds.equals("")) {
                    //selectAllOtherPlaces();
                    filteredPlacesList(backupIds);
                    //backupIds = "";

                    customPopupLocationList_btnAll.setTag(false);
                    customPopupLocationList_btnAll.setText(getResources().getString(R.string.deselect_all));
                } else {
                    //filteredPlacesList(ids);
                }


                //open the popup window here
                popUpWindow.showAsDropDown(tv_selected_place);

                break;

            case R.id.customPopupLocationList_btnDone:
                //ids = "";

                ids = getSelectedLocationIds();

                if (!ids.equals("")) {
                    backupIds = ids;
                    location_id = ids;
                    //sortEventListBasedOnDate(backupIds);
                    popUpWindow.dismiss();
                    resetList();
                    getSearchResult(true, categories_id);
                } else {
                    /*final AlertDialog alertDialog = new AlertDialog.Builder(SearchResultActivity.this, R.style.alertDialog)
                            .setTitle(getResources().getString(R.string.alert))
                            .setMessage(getResources().getString(R.string.deselect_all_empty_msg))
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    filteredPlacesList(backupIds);
                                    dialogInterface.dismiss();
                                }
                            })
                            .show();*/
                    Toast.makeText(SearchResultActivityNew.this, getResources().getString(R.string.deselect_all_empty_msg), Toast.LENGTH_LONG).show();
                }

                //deselectAllOtherPlaces();
                break;

            case R.id.customPopupLocationList_btnAll:

                if ((boolean) customPopupLocationList_btnAll.getTag()) {
                    selectAllOtherPlaces();
                    placesAdapter.notifyAdapter(locationModel);
                    customPopupLocationList_btnAll.setTag(false);
                    customPopupLocationList_btnAll.setText(getResources().getString(R.string.deselect_all));
                } else {
                    deselectAllOtherPlaces();
                    placesAdapter.notifyAdapter(locationModel);
                    customPopupLocationList_btnAll.setTag(true);
                    customPopupLocationList_btnAll.setText(getResources().getString(R.string.select_all));

                }
                break;
        }
    }

    private void selectTab(int position) {
        tab_img1.setImageDrawable(getResources().getDrawable(R.drawable.ic_all));
        tab_img2.setImageDrawable(getResources().getDrawable(R.drawable.ic_business));
        tab_img3.setImageDrawable(getResources().getDrawable(R.drawable.ic_residential));
        tab_img4.setImageDrawable(getResources().getDrawable(R.drawable.ic_community));

        tab_text1.setTextColor(ContextCompat.getColor(this, R.color.textColorGray));
        tab_text2.setTextColor(ContextCompat.getColor(this, R.color.textColorGray));
        tab_text3.setTextColor(ContextCompat.getColor(this, R.color.textColorGray));
        tab_text4.setTextColor(ContextCompat.getColor(this, R.color.textColorGray));
        switch (position) {
            case 0:
                tab_img1.setImageDrawable(getResources().getDrawable(R.drawable.ic_all_selected));
                tab_text1.setTextColor(ContextCompat.getColor(this, R.color.textColorYellow));
                break;
            case 1:
                tab_img2.setImageDrawable(getResources().getDrawable(R.drawable.ic_business_selected));
                tab_text2.setTextColor(ContextCompat.getColor(this, R.color.textColorYellow));
                break;
            case 2:
                tab_img3.setImageDrawable(getResources().getDrawable(R.drawable.ic_residential_selected));
                tab_text3.setTextColor(ContextCompat.getColor(this, R.color.textColorYellow));
                break;
            case 3:
                tab_img4.setImageDrawable(getResources().getDrawable(R.drawable.ic_community_selected));
                tab_text4.setTextColor(ContextCompat.getColor(this, R.color.textColorYellow));
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            /*case R.id.sp_selected_place:
                if (!locationModel.get(i).getLocation_id().equalsIgnoreCase("0") ||
                        !locationModel.get(i).getLocation_id().equalsIgnoreCase("" + location_id)) {
                    if (!searchForFirstTime) {
                        location_id = locationModel.get(i).getLocation_id();
                        DBG.d("AAAAAA", "Called");
                        resetList();
                        getSearchResult(true, categories_id);
                    }
                }
                break;*/
        }
    }

    private void resetList() {
        mContacts.clear();
        mAdapter.notifyDataChanged(mContacts);
        offset = 0;
        length = 40;
        totalData = 0;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void getLocationData() {
        locationModel.clear();
        locationModel.add(new LocationModel("0", getResources().getString(R.string.select_a_location), false));

        Body.Builder builder = new Body.Builder();
        Body bodyRequest = new Body(builder);
        DBG.d("AAAAAA", "" + bodyRequest.getBody().toString());

        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .get(header)
                .pathUrl(CONST.REST_API.GET_LOCATION)
                .fromString()
                .execute(SearchResultActivityNew.this, "getSponsorData", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("AAAAAA", "Get location - " + (String) o);
                        try {
                            JSONObject result = new JSONObject((String) o);

                            if (result.getBoolean("success")) {
                                Gson gson = new Gson();
                                Type listType = new TypeToken<List<LocationModel>>() {
                                }.getType();

                                String data = result.getJSONArray("details").toString();
                                List<LocationModel> mLocationModel = gson.fromJson(data, listType);
                                locationModel.addAll(mLocationModel);
                                placesAdapter.notifyAdapter(locationModel);
//                                customAdapter.notifyDataChanged(locationModel);
//                                sp_selected_place.setSelection(getLocationIndex());
                            } else {
                                Toast.makeText(SearchResultActivityNew.this, "" + result.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                            getSearchResult(true, categories_id);
                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private int getLocationIndex() {
        for (int i = 0; i < locationModel.size(); i++) {
            if (locationModel.get(i).getLocation_id().equalsIgnoreCase(location_id)) {
                return i;
            }
        }
        return 0;
    }

    private List<Contacts> getContactFilters(String id) {
        List<Contacts> mList = new ArrayList<>();
        for (Contacts model :
                mContacts) {
            if (model.getAccount_type().equalsIgnoreCase(id)) {
                mList.add(model);
            }
        }
        return mList;
    }

    private void getSearchResult(boolean needLoader, String categoryId) {


        if (loading)                // if called for new search criteria, clear the previous result
            mContacts.clear();

        if (needLoader) {

            mDialog.showProgressLoader(this, "Load Data..");
        }
        Log.e(TAG, "getSearchResult: params" + location_id+"\n"+account_type);

        // try with retrofit
        restApi.getSearchResult(
                categoryId,
                "" + location_id,
                "" + getTrim(R.id.et_search),
                account_type,
                String.valueOf(offset),
                String.valueOf(length)

        ).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    mDialog.dismissProgressLoader();
                    ResponseBody responseBody = response.body();
                    ResponseBody errorBody = response.errorBody();
                    if (responseBody != null) {
                        String responseString = responseBody.string();

                        JSONObject result = new JSONObject(responseString);
                        totalData = result.getInt("total_record");
                        if (result.getBoolean("success")) {
                            Gson gson = new Gson();
                            Type listType = new TypeToken<List<Contacts>>() {
                            }.getType();

                            tv_no_result_found.setVisibility(View.GONE);

                            String data = result.getJSONArray("details").toString();
                            List<Contacts> mList = gson.fromJson(data, listType);

                            if (mContacts.size() > 0)      // remove the last model as it is added due to showing loader
                                mContacts.remove(mContacts.size() - 1);

                            mContacts.addAll(mList);
                            mAdapter.notifyDataChanged(mContacts);
                            if (mContacts.size() == 0) {
                                tv_no_result_found.setText(getResources().getString(R.string.no_data_available));
                                tv_no_result_found.setVisibility(View.VISIBLE);
//                                    Toast.makeText(SearchResultActivity.this,
//                                            "" + getResources().getString(R.string.no_data_available), Toast.LENGTH_SHORT).show();
                            }
                            loading = true;
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(parent, result.getString("message"), Snackbar.LENGTH_LONG);
                            snackbar.show();
                            //Toast.makeText(SearchResultActivity.this, "" + result.getString("message"), Toast.LENGTH_SHORT).show();
                        }
                        offset = length + offset;
                        searchForFirstTime = false;
                    }
                } catch (JSONException exception) {
                    mDialog.dismissProgressLoader();
                    exception.printStackTrace();
                } catch (IOException e) {
                    mDialog.dismissProgressLoader();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });


//        Body.Builder builder = new Body.Builder()
//                .add("business_category", "" + categoryId)
//                .add("location", "" + location_id)
//                    .add("name", "" + getTrim(R.id.et_search))
//                .add("account_type", account_type)
//                .add("offset", String.format("%s", offset))
//                .add("length", String.format("%s", length));
//        Body bodyRequest = new Body(builder);
//
//        Log.e(TAG, "getSearchResult: request" + bodyRequest.getBody().toString());
//
//        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
//        IceNet.connect()
//                .createRequest()
//                .post(header, bodyRequest)
//                .pathUrl(CONST.REST_API.GET_BUSINESS_CONTACTS)
//                .fromString(needLoader)
//                .execute(SearchResultActivityNew.this, "getSearchResult", new RequestCallback() {
//                    @Override
//                    public void onRequestSuccess(Object o) {
//                        //progress.setVisibility(View.GONE);
//
//                    }
//
//                    @Override
//                    public void onRequestError(RequestError error) {
//
//                    }
//                });
    }

    private void checkCall() {
        if (Build.VERSION.SDK_INT > 22) {
            getPermissionToCall();
        } else {
            makeCall();
        }
    }

    // Identifier for the permission request
    private static final int CALL_PERMISSIONS_REQUEST = 2;

    public void getPermissionToCall() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {

                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                        CALL_PERMISSIONS_REQUEST);
            }
        } else {
            makeCall();
        }

    }

    private void makeCall() {
        String phones = String.format("%s", selectedPhoneNumber);
        Intent intent;
        intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:+" + phones));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    // Callback with the request from calling requestPermissions(...)
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == CALL_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makeCall();
            } else {

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onCall(int position) {
        selectedPhoneNumber = mContacts.get(position).getArea_code() + "" + mContacts.get(position).getTelephone_no();
        checkCall();
    }

    @Override
    public void onNextToAccountDetails(String account_id) {
        Intent i = new Intent(SearchResultActivityNew.this, AccountDetailsActivity.class);
        i.putExtra("account_id", account_id);
        startActivity(i);
    }


    // location popup section
    private PopupWindow popUpWindow;

    View customPopupView;
    RecyclerView customPopupLocationList_rvLocation;
    Button customPopupLocationList_btnDone;
    Button customPopupLocationList_btnAll;
    CustomLocationAdapter placesAdapter;
    TextView tv_selected_place;

    String ids = "";
    String backupIds = "";

    private void initiatePopupWindiw() {

        if (Paper.book().read(CONST.NAME_VALUE_PAIR.LOCATIONS) == null) {
            getLocationData();
        } else {
            locationModel = Paper.book().read(CONST.NAME_VALUE_PAIR.LOCATIONS);

            // api call for the first time when page is loaded
            //rl_all_tab.performClick();
            account_type = "";
            et_search.setText(search_text);
            getSearchResult(true, categories_id);
//            tab_img1.setImageDrawable(getResources().getDrawable(R.drawable.ic_all));
//            tab_text1.setTextColor(ContextCompat.getColor(this, R.color.textColorGray));
            selectTab(0);
        }
        // popup section
        //popupMenu = new PopupMenu(getActivity(), header_tv_location);
        customPopupView = LayoutInflater.from(this).inflate(R.layout.cutom_popup_location_list, null);
        customPopupLocationList_btnAll = (Button) customPopupView.findViewById(R.id.customPopupLocationList_btnAll);
        customPopupLocationList_btnAll.setOnClickListener(this);


        if (ifAnyLocationIsDeSelected()) {         // true means select, false means deselect
            customPopupLocationList_btnAll.setTag(true);
            customPopupLocationList_btnAll.setText(getResources().getString(R.string.select_all));
        } else {
            customPopupLocationList_btnAll.setTag(false);
            customPopupLocationList_btnAll.setText(getResources().getString(R.string.deselect_all));
        }

        customPopupLocationList_rvLocation = (RecyclerView) customPopupView.findViewById(R.id.customPopupLocationList_rvLocation);
        placesAdapter = new CustomLocationAdapter(this, locationModel);
        customPopupLocationList_rvLocation.setLayoutManager(new LinearLayoutManager(this));
        customPopupLocationList_rvLocation.addItemDecoration(new MarginDecorator(this));
        customPopupLocationList_rvLocation.setAdapter(placesAdapter);
        customPopupLocationList_btnDone = (Button) customPopupView.findViewById(R.id.customPopupLocationList_btnDone);
        customPopupLocationList_btnDone.setOnClickListener(this);

        popUpWindow = new PopupWindow(customPopupView,
                Math.round(getResources().getDimension(R.dimen._150sdp)),
                LinearLayoutCompat.LayoutParams.WRAP_CONTENT);


        // Set an elevation value for popup window
        // Call requires API level 21
        if (Build.VERSION.SDK_INT >= 21) {
            popUpWindow.setElevation(10.0f);
        }
        popUpWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popUpWindow.setFocusable(true);
        popUpWindow.setOutsideTouchable(true);
        popUpWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    popUpWindow.dismiss();
                    return true;
                }
                return false;
            }
        });
        // end popup window
    }


    private void filteredPlacesList(String ids) {
        String[] locationIds = ids.split("~");

        //placesModelArrayList.clear();
        for (int i = 0; i < locationModel.size(); i++) {
            LocationModel model = locationModel.get(i);
            for (int l = 0; l < locationIds.length; l++) {
                if (model.getLocation_id().equals(locationIds[l])) {
                    locationModel.get(i).setSelected(true);
                }
            }
        }
        placesAdapter.notifyAdapter(locationModel);
    }

    private String getSelectedLocationIds() {
        String id = "";
        for (int i = 0; i < locationModel.size(); i++) {
            if (locationModel.get(i).isSelected()) {
                if (id.equals("")) {
                    id = locationModel.get(i).getLocation_id();
                } else {
                    id = id + "~" + locationModel.get(i).getLocation_id();
                }
            }
        }
        return id;
    }

    private void deselectAllOtherPlaces() {
        for (int i = 0; i < locationModel.size(); i++) {
            locationModel.get(i).setSelected(false);
        }
        placesAdapter.notifyAdapter(locationModel);
    }

    private void selectAllOtherPlaces() {
        for (int i = 0; i < locationModel.size(); i++) {
            locationModel.get(i).setSelected(true);
        }
        placesAdapter.notifyAdapter(locationModel);
    }


    @Override
    public void onLocationItemClicked() {
        if (ifAnyLocationIsDeSelected()) {
            customPopupLocationList_btnAll.setTag(true);
            customPopupLocationList_btnAll.setText(getResources().getString(R.string.select_all));
        } else {
            customPopupLocationList_btnAll.setTag(false);
            customPopupLocationList_btnAll.setText(getResources().getString(R.string.deselect_all));
        }
    }

    private boolean ifAnyLocationIsDeSelected() {
        for (int i = 0; i < locationModel.size(); i++) {
            if (!locationModel.get(i).isSelected()) {
                return true;
            }
        }
        return false;
    }

    // location popup section

}