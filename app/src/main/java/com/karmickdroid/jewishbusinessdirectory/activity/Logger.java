package com.karmickdroid.jewishbusinessdirectory.activity;

import android.util.Log;

import com.karmickdroid.jewishbusinessdirectory.BuildConfig;


/**
 * Created on 27 jan,2016.
 */
public class Logger {

    private static String logTag = "LOG";
    private static String extraCheractor = "";

    public static final void setLogTag(String tag) {
        if (BuildConfig.DEBUG) Logger.logTag = tag;
    }

    public static final void setExtraChar(String spcl) {
        if (BuildConfig.DEBUG) Logger.extraCheractor = spcl;
    }

    public static final void showErrorLog(String msg) {
        if (BuildConfig.DEBUG) Log.e(logTag, extraCheractor + msg);
    }

    public static final void showVerboseLog(String msg) {
        if (BuildConfig.DEBUG) Log.v(logTag, extraCheractor + msg);
    }

    public static final void showDebugLog(String msg) {
        if (BuildConfig.DEBUG) Log.d(logTag, extraCheractor + msg);
    }

    public static final void showWarningLog(String msg) {
        if (BuildConfig.DEBUG) Log.w(logTag, extraCheractor + msg);
    }

    public static final void showInfoLog(String msg) {
        if (BuildConfig.DEBUG) Log.i(logTag, extraCheractor + msg);
    }

    public static final void showWTFLog(String msg) {
        if (BuildConfig.DEBUG) Log.wtf(logTag, extraCheractor + msg);
    }

    public static final void print(String msg) {
        if (BuildConfig.DEBUG) System.out.println(extraCheractor + msg);
    }

    public static final void printStackTrace(Exception e) {
        if (BuildConfig.DEBUG) e.printStackTrace();
    }

    public static final void printStackTrace(Error e) {
        if (BuildConfig.DEBUG) e.printStackTrace();
    }
}
