package com.karmickdroid.jewishbusinessdirectory.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.karmickdroid.jewishbusinessdirectory.R;

public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public ProgressDialog PD(Context mContext, boolean showLoadingText, String loaderText) {
        ProgressDialog PD = new ProgressDialog(mContext);
        if(showLoadingText)
            PD.setMessage("" + loaderText);
        PD.setCancelable(false);
        PD.show();
        // dialog.setMessage(Message);
        return PD;
    }

    public boolean isDataAvailable() {
        ConnectivityManager conxMgr = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);

        NetworkInfo[] info = conxMgr.getAllNetworkInfo();
        boolean flag= false;

        for (int i = 0; i<info.length; i++){
            if (info[i].getState() == NetworkInfo.State.CONNECTED){
                flag = true;
            }
        }
        return flag;
    }

    public void setHideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            View view = getCurrentFocus();
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = new View(this);
            }
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public boolean validate(String thisname) {
        String regexStrforEmail = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if(!thisname.matches(regexStrforEmail))
            return false;

        return true;
    }

    public boolean validate1(String thisname) {
        String regexStrforPhn = "^[0-9]*$";

        if(thisname.length()>15)
            return false;
        if(thisname.length()<6)
            return false;
        if(!thisname.matches(regexStrforPhn))
            return false;

        return true;
    }

    public boolean checkLocationService() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled && !network_enabled) {
            return false;
        }

        return true;
    }

    public void showCustomDialog(String title, String message, String buttonOk, String buttonCancel,
                                  boolean showCancelButton, boolean setCancelable, final OnOkCancelListner onOkCancelListner) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Netural "Cancel" Button
        alertDialog.setPositiveButton(buttonOk, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // User pressed Cancel button. Write Logic Here
                onOkCancelListner.onOk();
            }
        });

        if (setCancelable) {
            alertDialog.setCancelable(false);
        }

        if (showCancelButton) {
            alertDialog.setNegativeButton(buttonCancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    // User pressed Cancel button. Write Logic Here
                    onOkCancelListner.onCancel();
                }
            });
        }
        // Showing Alert Message
        alertDialog.show();
    }

    public interface OnOkCancelListner {
        void onOk();
        void onCancel();
    }

    @NonNull
    public String getTrim(int editTextId) {
        EditText v = (EditText) findViewById(editTextId);
        return v.getText().toString().trim();
    }

    public String string(int stringId){
        try {
            return getResources().getString(stringId);
        }catch (Exception e){

        }
        return "";
    }

}
