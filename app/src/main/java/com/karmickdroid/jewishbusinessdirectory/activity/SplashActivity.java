package com.karmickdroid.jewishbusinessdirectory.activity;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.gateWay.NetworkUtil;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;

public class SplashActivity extends BaseActivity {

    private ImageView iv_logo;
    private Animation shake;
    private RelativeLayout rl_splashContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        init();
    }

    private void init() {
        rl_splashContainer = (RelativeLayout)findViewById(R.id.rl_splashContainer);
        iv_logo = (ImageView) findViewById(R.id.iv_logo);

        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }


            @Override
            public void onAnimationEnd(Animation animation) {

                if (!NetworkUtil.isConnected(SplashActivity.this)) {
                    Snackbar.make(rl_splashContainer,
                            AppUtilities.getStringFromResource(SplashActivity.this
                                    , R.string.net_check_error), Snackbar.LENGTH_LONG).show();
                    return;
                }


                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(i);
                overridePendingTransition(R.anim.slideinfromright, R.anim.slideouttoleft);
                supportFinishAfterTransition();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        iv_logo.setAnimation(shake);
    }
}
