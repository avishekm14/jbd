package com.karmickdroid.jewishbusinessdirectory.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.karmickdroid.jewishbusinessdirectory.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment {

    private View view;
    private EditText et_search, et_browse_categories, et_selected_place;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        et_search = (EditText) view.findViewById(R.id.et_search);
        et_browse_categories = (EditText) view.findViewById(R.id.et_browse_categories);
        et_selected_place = (EditText) view.findViewById(R.id.et_selected_place);
    }

}
