package com.karmickdroid.jewishbusinessdirectory.fragment;


import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.activity.BaseActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TextView textView = new TextView(getActivity());
        textView.setText(R.string.hello_blank_fragment);
        return textView;
    }

    public boolean isDataAvailable() {
        return ((BaseActivity)getActivity()).isDataAvailable();
    }

    public void setHideSoftKeyboard() {
        ((BaseActivity)getActivity()).setHideSoftKeyboard();
    }

    public boolean validate(String thisname) {
        return ((BaseActivity)getActivity()).validate(thisname);
    }

    public boolean validate1(String thisname) {
        return ((BaseActivity)getActivity()).validate1(thisname);
    }

    public boolean checkLocationService() {
        return ((BaseActivity)getActivity()).checkLocationService();
    }

    @NonNull
    public String getTrim(int editTextId) {
        return getTrim(editTextId);
    }

    public String string(int stringId){
        return ((BaseActivity)getActivity()).string(stringId);
    }

    public void showCustomDialog(String title, String message, String buttonOk, String buttonCancel,
                                 boolean showCancelButton, boolean setCancelable, BaseActivity.OnOkCancelListner onOkCancelListner) {
        ((BaseActivity)getActivity()).showCustomDialog(title, message, buttonOk, buttonCancel,
                                              showCancelButton, setCancelable, onOkCancelListner);
    }
}
