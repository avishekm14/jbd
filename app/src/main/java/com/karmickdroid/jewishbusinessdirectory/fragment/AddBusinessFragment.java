package com.karmickdroid.jewishbusinessdirectory.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.debug.DBG;
import com.karmickdroid.jewishbusinessdirectory.gateWay.NetworkUtil;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;
import com.karmickdroid.jewishbusinessdirectory.helper.CONST;
import com.karmickdroid.jewishbusinessdirectory.icenet.IceNet;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestCallback;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestError;
import com.karmickdroid.jewishbusinessdirectory.model.BusinessCategories;
import com.karmickdroid.jewishbusinessdirectory.model.LocationModel;
import com.karmickdroid.jewishbusinessdirectory.model.ModelMasterLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.paperdb.Paper;


public class AddBusinessFragment extends Fragment implements View.OnClickListener {
    private EditText business_address_comment_et, add_business_business_name_et, add_business_contact_name_et;
    private Button business_address_save_btn;
    private CheckBox business_contact_us_cb;
    private ImageView add_business_cat_iv, add_businesss_address_iv, add_business_phone_iv, add_business_mail_iv, add_business_web_iv;
    private LinearLayout add_business_parent_cat_ll, business_adress_parent_ll, business_adress_phone_ll,
            business_adress_mail_ll, business_adress_web_ll;
    private int countCat = 0, countAddress = 0, countPhone = 0, countMail = 0, countWeb = 0;
    private View mExclusiveEmptyView;
    private ArrayList<Spinner> businessCatSpArray = new ArrayList<>(), businessLocationSpArray = new ArrayList<>(),
            addressStreetSpArray = new ArrayList<>(), addressCitySpArray = new ArrayList<>(),
            addressStateSpArray = new ArrayList<>(), addressZipSpArray = new ArrayList<>(),
            phoneLocationSpArray = new ArrayList<>(), phoneArearSpArray = new ArrayList<>(), phoneTypeSpArray = new ArrayList<>();

    private ArrayList<String> businessCats = new ArrayList<>(), businessLocs = new ArrayList<>(), businessCatIds = new ArrayList<>(),
            emailList = new ArrayList<>(), webList = new ArrayList<>(), phoneLocationList = new ArrayList<>(),
            phoneAreaList = new ArrayList<>(), phonenumberList = new ArrayList<>(), phoneTypeList = new ArrayList<>(),
            addressLocationList = new ArrayList<>(), addressHouseList = new ArrayList<>(), addressStreetList = new ArrayList<>(), addressAptList = new ArrayList<>(),
            addressCityList = new ArrayList<>(), addressStateList = new ArrayList<>(), addressZipList = new ArrayList<>();

    private ArrayList<Boolean> dndMailList = new ArrayList<>(), dndWebList = new ArrayList<>(),
            dndPhoneList = new ArrayList<>(), dndAddressList = new ArrayList<>();
    private List<BusinessCategories> businessCategories;
    private ArrayList<LocationModel> businessLocationModels = new ArrayList<>();
    private ArrayList<EditText> businessListMailEt = new ArrayList<>(), businessListWebEt = new ArrayList<>(),
            areaListEt = new ArrayList<>(), phoneListEt = new ArrayList<>(), houseListEt = new ArrayList<>(), aptListEt = new ArrayList<>();
    private ArrayList<CheckBox> dndAddressListCb = new ArrayList<>(), dndPhoneListCb = new ArrayList<>(), dndMailListCb = new ArrayList<>(), dndWebListCb = new ArrayList<>();
    private LinearLayout business_parent_ll;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareCategoryLocationData();

    }

    private void prepareCategoryLocationData() {
        businessCategories = Paper.book().read(CONST.NAME_VALUE_PAIR.CATEGORY);
        businessLocationModels = Paper.book().read(CONST.NAME_VALUE_PAIR.LOCATIONS);
        for (int i = 0; i < businessCategories.size(); i++) {
            businessCats.add(businessCategories.get(i).getBusiness_category_name());
        }
        for (int i = 0; i < businessLocationModels.size(); i++) {
            businessLocs.add(businessLocationModels.get(i).getLocation_name());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_add_business, container, false);
        initView(rootView);
        GetMasterLocation();
        return rootView;
    }

    /**
     * Api call for Getting master location
     */
    private void GetMasterLocation() {
        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.net_check_error), Snackbar.LENGTH_LONG).show();
            return;
        }
        IceNet.connect()
                .createRequest()
                .get()
                .pathUrl(CONST.REST_API.MASTER_LOCATION)
                .fromString()
                .execute(getActivity(), "getMasterLocation", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("AAAAAA", "Get detail - " + (String) o);
                        try {
                            JSONObject result = new JSONObject((String) o);
                            Gson gson = new Gson();
//                            Type listType = new TypeToken<ModelMasterLocation>() { }.getType();
                            ModelMasterLocation modelMasterLocation = gson.fromJson(result.toString(), ModelMasterLocation.class);
                            modelMasterLocation.getDetails();
                            modelMasterLocation.getTelephone_type();
/*                               JSONObject result = new JSONObject((String) o);

                         if (result.getBoolean("success")) {
                                Gson gson = new Gson();
                                Type telType = new TypeToken<List<ModelCityStateStreetTelZip>>() {
                                }.getType();
                             Type detailType = new TypeToken<List<ModelLocationDetail>>() {
                             }.getType();
                                String telData = result.getJSONArray("telephone_type").toString();
                                List<ModelCityStateStreetTelZip> telephoneList = gson.fromJson(telData, telType);
                             String detailData = result.getJSONArray("details").toString();

                            } else {
                                Snackbar.make(business_parent_ll, result.getString("message"), Snackbar.LENGTH_LONG).show();
                            }*/

                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

/*    @Override
    public void onResume() {
        super.onResume();
        countCat = 0;
        countAddress = 0;
    }*/

    private void initView(View rootView) {
        business_address_comment_et = (EditText) rootView.findViewById(R.id.business_address_comment_et);
        add_business_business_name_et = (EditText) rootView.findViewById(R.id.add_business_business_name_et);
        add_business_contact_name_et = (EditText) rootView.findViewById(R.id.add_business_contact_name_et);
        add_business_cat_iv = (ImageView) rootView.findViewById(R.id.add_business_cat_iv);
        add_businesss_address_iv = (ImageView) rootView.findViewById(R.id.add_businesss_address_iv);
        add_business_phone_iv = (ImageView) rootView.findViewById(R.id.add_business_phone_iv);
        add_business_mail_iv = (ImageView) rootView.findViewById(R.id.add_business_mail_iv);
        add_business_web_iv = (ImageView) rootView.findViewById(R.id.add_business_web_iv);
        business_address_save_btn = (Button) rootView.findViewById(R.id.business_address_save_btn);
        business_contact_us_cb = (CheckBox) rootView.findViewById(R.id.business_contact_us_cb);
        add_business_parent_cat_ll = (LinearLayout) rootView.findViewById(R.id.add_business_parent_cat_ll);
        business_adress_parent_ll = (LinearLayout) rootView.findViewById(R.id.business_adress_parent_ll);
        business_adress_phone_ll = (LinearLayout) rootView.findViewById(R.id.business_adress_phone_ll);
        business_adress_mail_ll = (LinearLayout) rootView.findViewById(R.id.business_adress_mail_ll);
        business_adress_web_ll = (LinearLayout) rootView.findViewById(R.id.business_adress_web_ll);
        business_parent_ll = (LinearLayout) rootView.findViewById(R.id.business_parent_ll);
        add_business_cat_iv.setOnClickListener(this);
        add_businesss_address_iv.setOnClickListener(this);
        add_business_phone_iv.setOnClickListener(this);
        add_business_mail_iv.setOnClickListener(this);
        add_business_web_iv.setOnClickListener(this);
        business_address_save_btn.setOnClickListener(this);
        addDynamicLayout();

    }

    /**
     * Adding Dynamic Layout
     */
    private void addDynamicLayout() {
        // Add Business Category
        inflateBusinessCat();

        // Add Business Address
        inflateBusinessAddress();

        // Add Business Phone
        inflateBusinessPhone();

        // Add Business Mail
        inflateBusinessMail();

        // Add Business Wb
        inflateBusinessWeb();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_business_cat_iv:
                inflateBusinessCat();
                break;
            case R.id.add_businesss_address_iv:
                inflateBusinessAddress();
                break;
            case R.id.add_business_phone_iv:
                inflateBusinessPhone();
                break;
            case R.id.add_business_mail_iv:
                inflateBusinessMail();
                break;
            case R.id.add_business_web_iv:
                inflateBusinessWeb();
                break;
            case R.id.business_address_save_btn:
                saveBusinessForm();
                break;
        }
    }

    /**
     * Add Business Web
     */
    private void inflateBusinessWeb() {
        countWeb++;
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.inflater_business_web, null);

        final ImageView add_business_remove_web_iv = (ImageView) rowView.findViewById(R.id.add_business_remove_web_iv);
        final EditText business_web_et = (EditText) rowView.findViewById(R.id.business_web_et);
        final CheckBox business_web_restriction_cb = (CheckBox) rowView.findViewById(R.id.business_web_restriction_cb);

        if (countWeb == 1)
            add_business_remove_web_iv.setVisibility(View.GONE);
        mExclusiveEmptyView = rowView;
        business_adress_web_ll.addView(rowView, business_adress_web_ll.getChildCount() - 1);
        businessListWebEt.add(business_web_et);
        dndWebListCb.add(business_web_restriction_cb);

        add_business_remove_web_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remove the row by calling the getParent on button
                if (countWeb != 1) {
                    business_adress_web_ll.removeView((View) v.getParent());
                    businessListWebEt.remove(business_web_et);
                    dndWebListCb.remove(business_web_restriction_cb);
                    countWeb--;
                }
            }
        });
    }

    /**
     * Add Business Mail
     */
    private void inflateBusinessMail() {
        countMail++;
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.inflater_business_mail, null);

        final ImageView add_business_remove_mail_iv = (ImageView) rowView.findViewById(R.id.add_business_remove_mail_iv);
        final EditText business_mail_et = (EditText) rowView.findViewById(R.id.business_mail_et);
        final CheckBox business_mail_restriction_cb = (CheckBox) rowView.findViewById(R.id.business_mail_restriction_cb);

        if (countMail == 1)
            add_business_remove_mail_iv.setVisibility(View.GONE);
        mExclusiveEmptyView = rowView;
        business_adress_mail_ll.addView(rowView, business_adress_mail_ll.getChildCount() - 1);
        businessListMailEt.add(business_mail_et);
        dndMailListCb.add(business_mail_restriction_cb);
        //// TODO: 07-06-2017 Add text Change Listener to EditText for Allowing Add layout if proper email address is entered
        add_business_remove_mail_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remove the row by calling the getParent on button
                if (countMail != 1) {
                    business_adress_mail_ll.removeView((View) v.getParent());
                    businessListMailEt.remove(business_mail_et);
                    dndMailListCb.remove(business_mail_restriction_cb);
                    countMail--;
                }
            }
        });
    }

    /**
     * Add Business Phone
     */
    private void inflateBusinessPhone() {
        countPhone++;
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.inflater_business_address_phone, null);

        final ImageView add_business_remove_phone_iv = (ImageView) rowView.findViewById(R.id.add_business_remove_phone_iv);
        final Spinner business_phone_location_sp = (Spinner) rowView.findViewById(R.id.business_phone_location_sp);
        final CheckBox business_phone_restriction_cb = (CheckBox) rowView.findViewById(R.id.business_phone_restriction_cb);
        final EditText address_phone_area_sp = (EditText) rowView.findViewById(R.id.address_phone_area_et);
        final EditText address_phone_et = (EditText) rowView.findViewById(R.id.address_phone_et);
        final Spinner address_phone_type_sp = (Spinner) rowView.findViewById(R.id.address_phone_type_sp);
        ArrayAdapter locAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, businessLocs);
        business_phone_location_sp.setAdapter(locAdapter);
        business_phone_location_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String LocationId = businessLocationModels.get(position).getLocation_id();
                //// TODO: 07-06-2017 Fire LocationModel api or traverse list with the help of this Id
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if (countPhone == 1)
            add_business_remove_phone_iv.setVisibility(View.GONE);
        mExclusiveEmptyView = rowView;
        business_adress_phone_ll.addView(rowView, business_adress_phone_ll.getChildCount() - 1);
        phoneLocationSpArray.add(business_phone_location_sp);
        //phoneArearSpArray.add(address_phone_area_sp);
        dndPhoneListCb.add(business_phone_restriction_cb);
        phoneListEt.add(address_phone_et);
        phoneTypeSpArray.add(address_phone_type_sp);

        add_business_remove_phone_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remove the row by calling the getParent on button
                if (countPhone != 1) {
                    business_adress_phone_ll.removeView((View) v.getParent());
//                    businessCatSpArray.remove(business_address_location_sp);
                    countPhone--;
                    phoneLocationSpArray.remove(business_phone_location_sp);
                    phoneArearSpArray.remove(address_phone_area_sp);
                    dndPhoneListCb.remove(business_phone_restriction_cb);
                    phoneListEt.remove(address_phone_et);
                    phoneTypeSpArray.remove(address_phone_type_sp);
                }
            }
        });
    }

    /**
     * Add Business Address
     */
    private void inflateBusinessAddress() {
        countAddress++;
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.inflater_business_address, null);

        final ImageView add_business_remove_address_iv = (ImageView) rowView.findViewById(R.id.add_business_remove_address_iv);
        final Spinner business_address_location_sp = (Spinner) rowView.findViewById(R.id.business_address_location_sp);
        ArrayAdapter locAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, businessLocs);
        business_address_location_sp.setAdapter(locAdapter);
        business_address_location_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String LocationId = businessLocationModels.get(position).getLocation_id();
                //// TODO: 07-06-2017 Fire LocationModel api or traverse list with the help of this Id
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final CheckBox business_address_restriction_cb = (CheckBox) rowView.findViewById(R.id.business_address_restriction_cb);
        final EditText business_address_house_et = (EditText) rowView.findViewById(R.id.business_address_house_et);
        final Spinner business_address_street_sp = (Spinner) rowView.findViewById(R.id.business_address_street_sp);
        final EditText business_address_apt_et = (EditText) rowView.findViewById(R.id.business_address_apt_et);
        final Spinner business_address_city_sp = (Spinner) rowView.findViewById(R.id.business_address_city_sp);
        final Spinner business_address_state_sp = (Spinner) rowView.findViewById(R.id.business_address_state_sp);
        final Spinner business_address_zip_sp = (Spinner) rowView.findViewById(R.id.business_address_zip_sp);

        businessLocationSpArray.add(business_address_location_sp);
        dndAddressListCb.add(business_address_restriction_cb);
        houseListEt.add(business_address_house_et);
        addressStreetSpArray.add(business_address_street_sp);
        aptListEt.add(business_address_apt_et);
        addressCitySpArray.add(business_address_city_sp);
        addressStateSpArray.add(business_address_state_sp);
        addressZipSpArray.add(business_address_zip_sp);
        if (countAddress == 1)
            add_business_remove_address_iv.setVisibility(View.GONE);
        mExclusiveEmptyView = rowView;
        business_adress_parent_ll.addView(rowView, business_adress_parent_ll.getChildCount() - 1);


        add_business_remove_address_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countAddress != 1) {
                    business_adress_parent_ll.removeView(rowView);
                    businessLocationSpArray.remove(business_address_location_sp);
                    dndAddressListCb.remove(business_address_restriction_cb);
                    houseListEt.remove(business_address_house_et);
                    addressStreetSpArray.remove(business_address_street_sp);
                    aptListEt.remove(business_address_apt_et);
                    addressCitySpArray.remove(business_address_city_sp);
                    addressStateSpArray.remove(business_address_state_sp);
                    addressZipSpArray.remove(business_address_zip_sp);
                    countAddress--;
                }
            }
        });
    }

    /**
     * Add Business Category Layout
     */
    private void inflateBusinessCat() {
        countCat++;
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.inflater_business_category, null);
        final ImageView add_business_remove_cat_iv = (ImageView) rowView
                .findViewById(R.id.add_business_remove_cat_iv);
        final Spinner add_business_business_category_sp = (Spinner) rowView.findViewById(R.id.add_business_business_category_sp);
        ArrayAdapter catAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, businessCats);
        add_business_business_category_sp.setAdapter(catAdapter);
        if (countCat == 1) {
            add_business_remove_cat_iv.setTag(true);                // true for '+'
            add_business_remove_cat_iv.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_add_round));
//            add_business_remove_cat_iv.setVisibility(View.GONE);
        }else{
            add_business_remove_cat_iv.setTag(false);               // false for '-'
        }
        mExclusiveEmptyView = rowView;
        add_business_parent_cat_ll.addView(rowView, add_business_parent_cat_ll.getChildCount() );
        businessCatSpArray.add(add_business_business_category_sp);// Adding to ArrayList

        add_business_remove_cat_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // remove the row by calling the getParent on button
                if ((boolean)v.getTag()) {
                    inflateBusinessCat();
                }else{
                    countCat--;
                    add_business_parent_cat_ll.removeView((View) v.getParent());
                    businessCatSpArray.remove(add_business_business_category_sp);
                }
            }
        });
    }

    /**
     * Submit Data
     */
    //// TODO: 12-06-2017 Placer the proper key
    private void saveBusinessForm() {
        JSONObject businessFormObj = new JSONObject();
        try {
            businessFormObj.accumulate("BUSINESS_NAME", add_business_business_name_et.getText().toString().trim());
            businessFormObj.accumulate("CONTACT_NAME", add_business_contact_name_et.getText().toString().trim());
            businessFormObj.accumulate("BUSINESS_CATEGORY", getCategoryIds());
            businessFormObj.accumulate("BUSINESS_ADDRESS", getBusinessAddress());
            businessFormObj.accumulate("BUSINESS_PHONE", getPhoneNumbers());
            businessFormObj.accumulate("EMAIL", getMails());
            businessFormObj.accumulate("WEBSITE", getWebs());
            businessFormObj.accumulate("COMMENT", business_address_comment_et.getText().toString().trim());
            businessFormObj.accumulate("CONTACT_US", business_contact_us_cb.isChecked());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Prepare Business Address
     */
    private JSONArray getBusinessAddress() {
        JSONArray addressArray = new JSONArray();
        for (int i = 0; i < businessLocationSpArray.size(); i++) {
            JSONObject addressObj = new JSONObject();
            try {
                addressObj.accumulate("ADDRESS_LOCATION", businessLocationModels.get(businessLocationSpArray.get(i).getSelectedItemPosition()).getLocation_id());
                addressObj.accumulate("ADDRESS_HOUSE", houseListEt.get(i).getText().toString().trim());
                addressObj.accumulate("ADDRESS_APT", aptListEt.get(i).getText().toString().trim());
                addressObj.accumulate("ADDRESS_STREET", addressStreetSpArray.get(i).getSelectedItem().toString());//// TODO: 08-06-2017 get the street id
                addressObj.accumulate("ADDRESS_CITY", addressCitySpArray.get(i).getSelectedItem().toString());//// TODO: 08-06-2017 get the street id
                addressObj.accumulate("ADDRESS_STATE", addressStateSpArray.get(i).getSelectedItem().toString());//// TODO: 08-06-2017 get the street id
                addressObj.accumulate("ADDRESS_ZIP", addressZipSpArray.get(i).getSelectedItem().toString());//// TODO: 08-06-2017 get the street id
            } catch (JSONException e) {
                e.printStackTrace();
            }
            addressArray.put(addressObj);
        }

        return addressArray;
    }

    /**
     * Prepare phone
     */
    //// TODO: 12-06-2017 Change the key as far api
    private JSONArray getPhoneNumbers() {
        JSONArray phoneArray = new JSONArray();
        for (int i = 0; i < phoneLocationSpArray.size(); i++) {
            JSONObject phoneObj = new JSONObject();
            try {
                phoneObj.accumulate("PHONE_LOCATION", businessLocationModels.get(phoneLocationSpArray.get(i).getSelectedItemPosition()).getLocation_id());
                phoneObj.accumulate("DND_PHONE", dndWebListCb.get(i).isChecked());
                phoneObj.accumulate("PHONE_AREA", phoneArearSpArray.get(i).getSelectedItem().toString());//// TODO: 08-06-2017 get data from api and populate
                phoneObj.accumulate("PHONE_NUMBER", phoneListEt.get(i).getText().toString().trim());
                phoneObj.accumulate("PHONE_TYPE", phoneTypeSpArray.get(i).getSelectedItem().toString());//// TODO: 08-06-2017 get data from api and populate
            } catch (JSONException e) {
                e.printStackTrace();
            }
            phoneArray.put(phoneObj);
        }
        return phoneArray;
    }

    /**
     * prepare Web
     *
     * @return
     */
    //// TODO: 12-06-2017 Change the key as far api
    private JSONArray getWebs() {
        JSONArray webJArray = new JSONArray();

        for (int i = 0; i < businessListWebEt.size(); i++) {
            JSONObject webJObj = new JSONObject();
            try {
                webJObj.accumulate("WEBSITE", businessListWebEt.get(i).getText().toString());//// TODO: 12-06-2017 Change the key as far api
                webJObj.accumulate("DND_WEB", dndWebListCb.get(i).isChecked());//// TODO: 12-06-2017 Change the key as far api
            } catch (JSONException e) {
                e.printStackTrace();
            }
            webJArray.put(webJObj);
        }
        return webJArray;
    }

    /**
     * Prepare Mail
     *
     * @return
     */
    //// TODO: 12-06-2017 Change the key as far api
    private JSONArray getMails() {
        HashMap mailMap = new HashMap();
        JSONArray mailArray = new JSONArray();
        for (int i = 0; i < businessListMailEt.size(); i++) {
            JSONObject mailObj = new JSONObject();
            try {
                mailObj.accumulate("EMAIL", businessListMailEt.get(i).getText().toString());
                mailObj.accumulate("EMAIL_DND", dndMailListCb.get(i).isChecked());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return mailArray;
    }

    /**
     * Prepare CategoryId
     *
     * @return
     */
    //// TODO: 12-06-2017 Change the key as far api
    private JSONArray getCategoryIds() {
        JSONArray catArray = new JSONArray();
        for (int i = 0; i < businessCatSpArray.size(); i++) {
            JSONObject catObj = new JSONObject();
            businessCatIds.add(businessCategories.get(businessCatSpArray.get(i).getSelectedItemPosition()).getBusiness_category_id());
            try {
                catObj.accumulate("CAT_ID", businessCategories.get(businessCatSpArray.get(i).getSelectedItemPosition()).getBusiness_category_id());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            catArray.put(catObj);
        }
        return catArray;
    }
}
