package com.karmickdroid.jewishbusinessdirectory.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.adapter.AddResidenceLocationsAdapter;
import com.karmickdroid.jewishbusinessdirectory.adapter.OnlyTextForSpinnerAdapter;
import com.karmickdroid.jewishbusinessdirectory.debug.DBG;
import com.karmickdroid.jewishbusinessdirectory.gateWay.NetworkUtil;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;
import com.karmickdroid.jewishbusinessdirectory.helper.CONST;
import com.karmickdroid.jewishbusinessdirectory.icenet.Body;
import com.karmickdroid.jewishbusinessdirectory.icenet.Header;
import com.karmickdroid.jewishbusinessdirectory.icenet.IceNet;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestCallback;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestError;
import com.karmickdroid.jewishbusinessdirectory.model.ModelCityStateStreetTelZip;
import com.karmickdroid.jewishbusinessdirectory.model.ModelLocationDetail;
import com.karmickdroid.jewishbusinessdirectory.model.ModelMasterLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class AddResidentialFragment extends Fragment implements AdapterView.OnItemSelectedListener, View.OnClickListener {


    private ScrollView addResidential_sv_container;
    private Spinner residential_add_location_sp;
    private EditText residential_last_name_et, residential_first_name_et, residential_ben_reb_name_et, residential_maiden_name_et;
    private EditText address_house_et, address_apt_et;
    private Spinner address_street_sp, address_city_sp, address_state_sp, address_zip_sp;
    private EditText residential_phone_area_code_et, residential_phone_et, residential_comment_et;
    private Button residential_save_btn;


    ModelMasterLocation modelMasterLocation;

    private AddResidenceLocationsAdapter locationsAdapter;
    private OnlyTextForSpinnerAdapter streetAdapter;
    private OnlyTextForSpinnerAdapter cityAdapter;
    private OnlyTextForSpinnerAdapter stateAdapter;
    private OnlyTextForSpinnerAdapter zipAdapter;

    private List<ModelLocationDetail> listLocations = new ArrayList<>();
    private List<ModelCityStateStreetTelZip> listStreet = new ArrayList<>();
    private List<ModelCityStateStreetTelZip> listCity = new ArrayList<>();
    private List<ModelCityStateStreetTelZip> listState = new ArrayList<>();
    private List<ModelCityStateStreetTelZip> listZip = new ArrayList<>();

    String locationName = "", streetName = "", cityName = "", stateName = "", zip = "";
    String lastName = "", firstName = "", benRebFirstName = "", maidenName = "", houseNo = "", aptNo = "", areaCode = "", phoneNo = "";
    String commentStr = "";
    int selectedLocationPos = 0;


    String location_id = "", city_id = "", state_id = "", zip_id = "", street_id = "";

    JSONArray addArr;
    JSONObject addObj = new JSONObject();
    JSONArray phArr;
    JSONObject phObj = new JSONObject();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_add_residential, container, false);

        initViews(rootView);

        GetMasterLocation();
        return rootView;
    }

    private void initViews(View rootView) {
        addResidential_sv_container = (ScrollView) rootView.findViewById(R.id.addResidential_sv_container);
        residential_add_location_sp = (Spinner) rootView.findViewById(R.id.residential_add_location_sp);
        residential_last_name_et = (EditText) rootView.findViewById(R.id.residential_last_name_et);
        residential_first_name_et = (EditText) rootView.findViewById(R.id.residential_first_name_et);
        residential_ben_reb_name_et = (EditText) rootView.findViewById(R.id.residential_ben_reb_name_et);
        residential_maiden_name_et = (EditText) rootView.findViewById(R.id.residential_maiden_name_et);
        address_house_et = (EditText) rootView.findViewById(R.id.address_house_et);
        address_apt_et = (EditText) rootView.findViewById(R.id.address_apt_et);
        address_street_sp = (Spinner) rootView.findViewById(R.id.address_street_sp);
        address_city_sp = (Spinner) rootView.findViewById(R.id.address_city_sp);
        address_state_sp = (Spinner) rootView.findViewById(R.id.address_state_sp);
        address_zip_sp = (Spinner) rootView.findViewById(R.id.address_zip_sp);

        residential_phone_area_code_et = (EditText) rootView.findViewById(R.id.residential_phone_area_code_et);
        residential_phone_et = (EditText) rootView.findViewById(R.id.residential_phone_et);
        residential_comment_et = (EditText) rootView.findViewById(R.id.residential_comment_et);
        residential_save_btn = (Button) rootView.findViewById(R.id.residential_save_btn);

        locationsAdapter = new AddResidenceLocationsAdapter(getActivity(), android.R.id.text1, listLocations, getActivity().getString(R.string.selectLocation));
        streetAdapter = new OnlyTextForSpinnerAdapter(getActivity(), android.R.id.text1, listStreet, getActivity().getString(R.string.selectStreet));
        cityAdapter = new OnlyTextForSpinnerAdapter(getActivity(), android.R.id.text1, listCity, getActivity().getString(R.string.selectCity));
        stateAdapter = new OnlyTextForSpinnerAdapter(getActivity(), android.R.id.text1, listState, getActivity().getString(R.string.selectState));
        zipAdapter = new OnlyTextForSpinnerAdapter(getActivity(), android.R.id.text1, listZip, getActivity().getString(R.string.selectZip));

        residential_add_location_sp.setAdapter(locationsAdapter);
        address_street_sp.setAdapter(streetAdapter);
        address_city_sp.setAdapter(cityAdapter);
        address_state_sp.setAdapter(stateAdapter);
        address_zip_sp.setAdapter(zipAdapter);

        residential_add_location_sp.setOnItemSelectedListener(this);
        address_street_sp.setOnItemSelectedListener(this);
        address_city_sp.setOnItemSelectedListener(this);
        address_state_sp.setOnItemSelectedListener(this);
        address_zip_sp.setOnItemSelectedListener(this);
        residential_save_btn.setOnClickListener(this);


    }

    /**
     * Api call for Getting master location
     */
    private void GetMasterLocation() {
        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(addResidential_sv_container, AppUtilities.getStringFromResource(getActivity(), R.string.net_check_error), Snackbar.LENGTH_LONG).show();
            return;
        }
        IceNet.connect()
                .createRequest()
                .get()
                .pathUrl(CONST.REST_API.MASTER_LOCATION)
                .fromString()
                .execute(getActivity(), "getMasterLocation", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("AAAAAA", "Get LocationModel details - " + (String) o);
                        try {

                            JSONObject result = new JSONObject((String) o);

                            if (result.getBoolean("success")) {
                                modelMasterLocation = new ModelMasterLocation();
                                Gson gson = new Gson();

                                Type typeMaster = new TypeToken<ModelMasterLocation>() {
                                }.getType();
                                modelMasterLocation = gson.fromJson(result.toString(), typeMaster);

                                setVals(modelMasterLocation);

                            } else {
                                Snackbar.make(addResidential_sv_container, result.getString("message"), Snackbar.LENGTH_LONG).show();
                            }

                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }

    private void setVals(ModelMasterLocation modelMasterLocation) {
        listLocations = modelMasterLocation.getDetails();
        locationsAdapter.notifyAdapter(listLocations);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.residential_add_location_sp:
                selectedLocationPos = position - 1;


                listStreet.clear();
                listCity.clear();
                listState.clear();
                listZip.clear();
                if (position > 0) {
                    location_id = String.valueOf(modelMasterLocation.getDetails().get(selectedLocationPos).getLocation_id());
                    locationName = modelMasterLocation.getDetails().get(position - 1).getLocation_name();

                    streetAdapter.notifyAdapter(modelMasterLocation.getDetails().get(position - 1).getStreet());
                    cityAdapter.notifyAdapter(modelMasterLocation.getDetails().get(position - 1).getCity());
                    stateAdapter.notifyAdapter(modelMasterLocation.getDetails().get(position - 1).getState());
                    zipAdapter.notifyAdapter(modelMasterLocation.getDetails().get(position - 1).getZip());

                    areaCode = String.valueOf(modelMasterLocation.getDetails().get(position - 1).getAreacode().get(0).getAreacode());
                    residential_phone_area_code_et.setText(areaCode);
                } else {
                    location_id = "";
                    locationName = "";
                    streetAdapter.notifyAdapter(listStreet);
                    cityAdapter.notifyAdapter(listCity);
                    stateAdapter.notifyAdapter(listState);
                    zipAdapter.notifyAdapter(listZip);
                }

                address_street_sp.setSelection(0);
                address_city_sp.setSelection(0);
                address_state_sp.setSelection(0);
                address_zip_sp.setSelection(0);
                break;
            case R.id.address_street_sp:
                if (position > 0) {
                    street_id = String.valueOf(modelMasterLocation.getDetails().get(selectedLocationPos).getStreet().get(position - 1).getId());
                    streetName = modelMasterLocation.getDetails().get(selectedLocationPos).getStreet().get(position - 1).getName();
                } else {
                    street_id = "";
                    streetName = "";
                }

                break;
            case R.id.address_city_sp:
                if (position > 0) {
                    city_id = String.valueOf(modelMasterLocation.getDetails().get(selectedLocationPos).getCity().get(position - 1).getId());
                    cityName = modelMasterLocation.getDetails().get(selectedLocationPos).getCity().get(position - 1).getName();
                } else {
                    city_id = "";
                    cityName = "";
                }
                break;
            case R.id.address_state_sp:
                if (position > 0) {
                    state_id = String.valueOf(modelMasterLocation.getDetails().get(selectedLocationPos).getState().get(position - 1).getId());
                    stateName = modelMasterLocation.getDetails().get(selectedLocationPos).getState().get(position - 1).getName();
                } else {
                    state_id = "";
                    stateName = "";
                }
                break;
            case R.id.address_zip_sp:
                if (position > 0) {
                    zip_id = String.valueOf(modelMasterLocation.getDetails().get(selectedLocationPos).getZip().get(position - 1).getId());
                    zip = modelMasterLocation.getDetails().get(selectedLocationPos).getZip().get(position - 1).getName();
                } else {
                    zip_id = "";
                    zip = "";
                }
                break;

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.residential_save_btn:
                if (validate())
                    submitForm();
                break;
        }
    }


    private boolean validate() {
        lastName = residential_last_name_et.getText().toString().trim();
        firstName = residential_first_name_et.getText().toString().trim();
        benRebFirstName = residential_ben_reb_name_et.getText().toString().trim();
        maidenName = residential_maiden_name_et.getText().toString().trim();
        houseNo = address_house_et.getText().toString().trim();
        aptNo = address_apt_et.getText().toString().trim();
        areaCode = residential_phone_area_code_et.getText().toString().trim();
        phoneNo = residential_phone_et.getText().toString().trim();
        commentStr = residential_comment_et.getText().toString().trim();

        if (locationName.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.location_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (lastName.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.last_name_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (firstName.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.first_name_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (benRebFirstName.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.ben_reb_name_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (maidenName.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.maiden_name_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (houseNo.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.house_no_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (streetName.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.street_name_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (aptNo.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.apt_no_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (cityName.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.city_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (stateName.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.selectState), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (zip.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.zip_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (areaCode.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.area_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (phoneNo.equals("")) {
            Snackbar.make(addResidential_sv_container, getActivity().getResources().getString(R.string.ph_no_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private void submitForm() {

        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(addResidential_sv_container, AppUtilities.getStringFromResource(getActivity(), R.string.net_check_error), Snackbar.LENGTH_LONG).show();
            return;
        }

        generateParams();


        Body.Builder builder = new Body.Builder()
                .add(CONST.NAME_VALUE_PAIR.account_type, "1")
                .add(CONST.NAME_VALUE_PAIR.first_name, firstName)
                .add(CONST.NAME_VALUE_PAIR.last_name, lastName)
                .add(CONST.NAME_VALUE_PAIR.father_name, benRebFirstName)
                .add(CONST.NAME_VALUE_PAIR.maidan_name, maidenName)
                .add(CONST.NAME_VALUE_PAIR.comment, commentStr)
                .add(CONST.NAME_VALUE_PAIR.address, addArr.toString())
                .add(CONST.NAME_VALUE_PAIR.phone, phArr.toString());

        Body bodyRequest = new Body(builder);
        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));
        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.SAVE_LEAD_ACCOUNT_API)
                .fromString()
                .execute(getActivity(), "save residential form", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("AAAAAA", "save residential response - " + (String) o);
                        try {

                            JSONObject result = new JSONObject((String) o);

                            if (result.getBoolean("success")) {
                                Snackbar.make(addResidential_sv_container, result.getString("message"), Snackbar.LENGTH_LONG)
                                        .addCallback(new Snackbar.Callback() {
                                            @Override
                                            public void onDismissed(Snackbar transientBottomBar, int event) {
                                                super.onDismissed(transientBottomBar, event);
                                                getActivity().finish();
                                            }
                                        }).show();

                            } else {
                                Snackbar.make(addResidential_sv_container, result.getString("message"), Snackbar.LENGTH_LONG).show();
                            }

                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }


    private void generateParams() {
        addArr = new JSONArray();
        try {
            addObj.put(CONST.NAME_VALUE_PAIR.location_address, location_id);
            addObj.put(CONST.NAME_VALUE_PAIR.house_no, houseNo);
            addObj.put(CONST.NAME_VALUE_PAIR.street, street_id);
            addObj.put(CONST.NAME_VALUE_PAIR.apt_no, aptNo);
            addObj.put(CONST.NAME_VALUE_PAIR.city_id, city_id);
            addObj.put(CONST.NAME_VALUE_PAIR.state, state_id);
            addObj.put(CONST.NAME_VALUE_PAIR.zip_id, zip_id);
            addObj.put(CONST.NAME_VALUE_PAIR.is_publish_address, "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        addArr.put(addObj);

        phArr = new JSONArray();
        try {
            phObj.put(CONST.NAME_VALUE_PAIR.area_code, areaCode);
            phObj.put(CONST.NAME_VALUE_PAIR.telephone_no, phoneNo);
            phObj.put(CONST.NAME_VALUE_PAIR.is_publish_telephone, "1");
            phObj.put(CONST.NAME_VALUE_PAIR.location_telephone, location_id);
            phObj.put(CONST.NAME_VALUE_PAIR.telephone_type, "0");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        phArr.put(phObj);
    }


}