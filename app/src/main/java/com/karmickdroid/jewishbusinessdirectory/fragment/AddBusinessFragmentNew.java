package com.karmickdroid.jewishbusinessdirectory.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.adapter.AddBusinessAddressLayoutAdapter;
import com.karmickdroid.jewishbusinessdirectory.adapter.AddBusinessCategoryLayoutAdapter;
import com.karmickdroid.jewishbusinessdirectory.adapter.AddBusinessEmailLayoutAdapter;
import com.karmickdroid.jewishbusinessdirectory.adapter.AddBusinessPhoneLayoutAdapter;
import com.karmickdroid.jewishbusinessdirectory.adapter.AddBusinessWebLayoutAdapter;
import com.karmickdroid.jewishbusinessdirectory.debug.DBG;
import com.karmickdroid.jewishbusinessdirectory.gateWay.NetworkUtil;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;
import com.karmickdroid.jewishbusinessdirectory.helper.CONST;
import com.karmickdroid.jewishbusinessdirectory.icenet.Body;
import com.karmickdroid.jewishbusinessdirectory.icenet.Header;
import com.karmickdroid.jewishbusinessdirectory.icenet.IceNet;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestCallback;
import com.karmickdroid.jewishbusinessdirectory.icenet.RequestError;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessAddressLayoutModel;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessCategoryModel;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessEmailLayoutModel;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessPhoneLayoutModel;
import com.karmickdroid.jewishbusinessdirectory.model.AddBusinessWebLayoutModel;
import com.karmickdroid.jewishbusinessdirectory.model.BusinessCategories;
import com.karmickdroid.jewishbusinessdirectory.model.LocationModel;
import com.karmickdroid.jewishbusinessdirectory.model.ModelCityStateStreetTelZip;
import com.karmickdroid.jewishbusinessdirectory.model.ModelLocationDetail;
import com.karmickdroid.jewishbusinessdirectory.model.ModelMasterLocation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.paperdb.Paper;


public class AddBusinessFragmentNew extends Fragment implements View.OnClickListener, AddBusinessAddressLayoutAdapter.OnLocationSelection {
    private EditText business_address_comment_et, add_business_business_name_et, add_business_contact_name_et;
    private Button business_address_save_btn;
    private CheckBox business_contact_us_cb;
    //private ImageView add_business_cat_iv, add_businesss_address_iv, add_business_phone_iv, add_business_mail_iv, add_business_web_iv;
    /*private LinearLayout add_business_parent_cat_ll, business_adress_parent_ll, business_adress_phone_ll,
            business_adress_mail_ll, business_adress_web_ll;*/
    private int countCat = 0, countAddress = 0, countPhone = 0, countMail = 0, countWeb = 0;
    private View mExclusiveEmptyView;
    private ArrayList<Spinner> businessCatSpArray = new ArrayList<>(), businessLocationSpArray = new ArrayList<>(),
            addressStreetSpArray = new ArrayList<>(), addressCitySpArray = new ArrayList<>(),
            addressStateSpArray = new ArrayList<>(), addressZipSpArray = new ArrayList<>(),
            phoneLocationSpArray = new ArrayList<>(), phoneArearSpArray = new ArrayList<>(), phoneTypeSpArray = new ArrayList<>();

    private ArrayList<String> businessCats = new ArrayList<>(), businessLocs = new ArrayList<>(), businessCatIds = new ArrayList<>(),
            emailList = new ArrayList<>(), webList = new ArrayList<>(), phoneLocationList = new ArrayList<>(),
            phoneAreaList = new ArrayList<>(), phonenumberList = new ArrayList<>(), phoneTypeList = new ArrayList<>(),
            addressLocationList = new ArrayList<>(), addressHouseList = new ArrayList<>(), addressStreetList = new ArrayList<>(), addressAptList = new ArrayList<>(),
            addressCityList = new ArrayList<>(), addressStateList = new ArrayList<>(), addressZipList = new ArrayList<>();

    private ArrayList<Boolean> dndMailList = new ArrayList<>(), dndWebList = new ArrayList<>(),
            dndPhoneList = new ArrayList<>(), dndAddressList = new ArrayList<>();
    private List<BusinessCategories> businessCategories;
    private ArrayList<LocationModel> businessLocationModels = new ArrayList<>();
    private ArrayList<EditText> businessListMailEt = new ArrayList<>(), businessListWebEt = new ArrayList<>(),
            areaListEt = new ArrayList<>(), phoneListEt = new ArrayList<>(), houseListEt = new ArrayList<>(), aptListEt = new ArrayList<>();
    private ArrayList<CheckBox> dndAddressListCb = new ArrayList<>(), dndPhoneListCb = new ArrayList<>(), dndMailListCb = new ArrayList<>(), dndWebListCb = new ArrayList<>();
    private LinearLayout business_parent_ll;


    //////////////////////////////////// added pra
    ModelMasterLocation modelMasterLocation = new ModelMasterLocation();

    private RecyclerView add_business_parent_cat_rv;
    private ArrayList<AddBusinessCategoryModel> addBusinessCategoryLayoutModels = new ArrayList<AddBusinessCategoryModel>();

    private RecyclerView add_business_parent_add_rv;
    private ArrayList<AddBusinessAddressLayoutModel> addBusinessAddressLayoutModels = new ArrayList<AddBusinessAddressLayoutModel>();

    private RecyclerView add_business_parent_ph_rv;
    private ArrayList<AddBusinessPhoneLayoutModel> addBusinessPhoneLayoutModels = new ArrayList<AddBusinessPhoneLayoutModel>();

    private RecyclerView add_business_parent_mail_rv;
    private ArrayList<AddBusinessEmailLayoutModel> addBusinessEmailLayoutModels = new ArrayList<AddBusinessEmailLayoutModel>();

    private RecyclerView add_business_parent_web_rv;
    private ArrayList<AddBusinessWebLayoutModel> addBusinessWebLayoutModels = new ArrayList<AddBusinessWebLayoutModel>();


    JSONArray nameArr;
    JSONObject nameObj;
    JSONArray catArr;
    JSONObject catObj;
    JSONArray addArr;
    JSONObject addObj;
    JSONArray phArr;
    JSONObject phObj;
    JSONArray emailArr;
    JSONObject emailObj;
    JSONArray webArr;
    JSONObject webObj;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prepareCategoryLocationData();

    }

    private void prepareCategoryLocationData() {
        businessCategories = Paper.book().read(CONST.NAME_VALUE_PAIR.CATEGORY);
        businessLocationModels = Paper.book().read(CONST.NAME_VALUE_PAIR.LOCATIONS);
        for (int i = 0; i < businessCategories.size(); i++) {
            businessCats.add(businessCategories.get(i).getBusiness_category_name());
        }
        for (int i = 0; i < businessLocationModels.size(); i++) {
            businessLocs.add(businessLocationModels.get(i).getLocation_name());
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_add_business_new, container, false);
        initView(rootView);
        GetMasterLocation();
        return rootView;
    }

    /**
     * Api call for Getting master location
     */
    private void GetMasterLocation() {
        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.net_check_error), Snackbar.LENGTH_LONG).show();
            return;
        }
        IceNet.connect()
                .createRequest()
                .get()
                .pathUrl(CONST.REST_API.MASTER_LOCATION)
                .fromString()
                .execute(getActivity(), "getMasterLocation", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("AAAAAA", "Get detail - " + (String) o);
                        try {
                            JSONObject result = new JSONObject((String) o);
                            Gson gson = new Gson();
//                            Type listType = new TypeToken<ModelMasterLocation>() { }.getType();
                            modelMasterLocation = gson.fromJson(result.toString(), ModelMasterLocation.class);
                            modelMasterLocation.getDetails();
                            modelMasterLocation.getTelephone_type();

                            addDynamicLayout();


                        } catch (JSONException exception) {
                            exception.printStackTrace();
                        }
                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });
    }


    private void initView(View rootView) {
        business_address_comment_et = (EditText) rootView.findViewById(R.id.business_address_comment_et);
        add_business_business_name_et = (EditText) rootView.findViewById(R.id.add_business_business_name_et);
        add_business_contact_name_et = (EditText) rootView.findViewById(R.id.add_business_contact_name_et);
        /*add_business_cat_iv = (ImageView) rootView.findViewById(R.id.add_business_cat_iv);
        add_businesss_address_iv = (ImageView) rootView.findViewById(R.id.add_businesss_address_iv);
        add_business_phone_iv = (ImageView) rootView.findViewById(R.id.add_business_phone_iv);
        add_business_mail_iv = (ImageView) rootView.findViewById(R.id.add_business_mail_iv);
        add_business_web_iv = (ImageView) rootView.findViewById(R.id.add_business_web_iv);*/
        business_address_save_btn = (Button) rootView.findViewById(R.id.business_address_save_btn);
        business_contact_us_cb = (CheckBox) rootView.findViewById(R.id.business_contact_us_cb);
        /*add_business_parent_cat_ll = (LinearLayout) rootView.findViewById(R.id.add_business_parent_cat_ll);
        business_adress_parent_ll = (LinearLayout) rootView.findViewById(R.id.business_adress_parent_ll);
        business_adress_phone_ll = (LinearLayout) rootView.findViewById(R.id.business_adress_phone_ll);
        business_adress_mail_ll = (LinearLayout) rootView.findViewById(R.id.business_adress_mail_ll);
        business_adress_web_ll = (LinearLayout) rootView.findViewById(R.id.business_adress_web_ll);*/
        business_parent_ll = (LinearLayout) rootView.findViewById(R.id.business_parent_ll);

        add_business_parent_cat_rv = (RecyclerView) rootView.findViewById(R.id.add_business_parent_cat_rv);
        add_business_parent_add_rv = (RecyclerView) rootView.findViewById(R.id.add_business_parent_add_rv);
        add_business_parent_ph_rv = (RecyclerView) rootView.findViewById(R.id.add_business_parent_ph_rv);
        add_business_parent_mail_rv = (RecyclerView) rootView.findViewById(R.id.add_business_parent_mail_rv);
        add_business_parent_web_rv = (RecyclerView) rootView.findViewById(R.id.add_business_parent_web_rv);



        /*add_business_cat_iv.setOnClickListener(this);
        add_businesss_address_iv.setOnClickListener(this);
        add_business_phone_iv.setOnClickListener(this);
        add_business_mail_iv.setOnClickListener(this);
        add_business_web_iv.setOnClickListener(this);*/
        business_address_save_btn.setOnClickListener(this);
        //addDynamicLayout();

    }

    /**
     * Adding Dynamic Layout
     */
    private void addDynamicLayout() {
        // Add Business Category
        inflateBusinessCat();

        // Add Business Address
        inflateBusinessAddress();

        // Add Business Phone
        inflateBusinessPhone();

        // Add Business Mail
        inflateBusinessMail();

        // Add Business Wb
        inflateBusinessWeb();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.business_address_save_btn:
                AppUtilities.hideSoftInputMode(getActivity(), add_business_business_name_et);
                if (validate() && validateCategory() && validateAddress() && validatePhNo() && validateEmail() && validateWeb())
                    saveBusinessForm();
                break;
        }
    }


    /**
     * Add Business Web
     */
    private void inflateBusinessWeb() {

        addBusinessWebLayoutModels.add(new AddBusinessWebLayoutModel(true));
        AddBusinessWebLayoutAdapter webLayoutAdapter = new AddBusinessWebLayoutAdapter(getActivity(), addBusinessWebLayoutModels);
        add_business_parent_web_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        add_business_parent_web_rv.setAdapter(webLayoutAdapter);
    }

    /**
     * Add Business Mail
     */
    private void inflateBusinessMail() {
        addBusinessEmailLayoutModels.add(new AddBusinessEmailLayoutModel(true));
        AddBusinessEmailLayoutAdapter emailLayoutAdapter = new AddBusinessEmailLayoutAdapter(getActivity(), addBusinessEmailLayoutModels);
        add_business_parent_mail_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        add_business_parent_mail_rv.setAdapter(emailLayoutAdapter);
    }

    /**
     * Add Business Phone
     */
    ArrayList<ModelLocationDetail> modelLocationDetailsForTelephone = new ArrayList<>();
    AddBusinessPhoneLayoutAdapter addBusinessPhoneLayoutAdapter;

    private void inflateBusinessPhone() {

        AddBusinessPhoneLayoutModel model = new AddBusinessPhoneLayoutModel(true);
//        model.setModelLocationDetails(modelMasterLocation.getDetails());
        model.setModelLocationDetails(modelLocationDetailsForTelephone);
        model.setPhoneTypeModels(modelMasterLocation.getTelephone_type());
        addBusinessPhoneLayoutModels.add(model);

        addBusinessPhoneLayoutAdapter = new AddBusinessPhoneLayoutAdapter(getActivity(), addBusinessPhoneLayoutModels);

        add_business_parent_ph_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        add_business_parent_ph_rv.setAdapter(addBusinessPhoneLayoutAdapter);


        /*
        * no need to add a blank location model at the first position of the array as
        * it is already added in address section */

        //location
//        ModelLocationDetail locationModel = new ModelLocationDetail(getActivity().getString(R.string.select_location));
//        addBusinessPhoneLayoutModels.get(0).getModelLocationDetails().add(0, locationModel);

        //phone type
        ModelCityStateStreetTelZip phoneTypeModel = new ModelCityStateStreetTelZip(getActivity().getString(R.string.select_phone_type));
        addBusinessPhoneLayoutModels.get(0).getPhoneTypeModels().add(0, phoneTypeModel);

    }

    /**
     * Add Business Address
     */
    private void inflateBusinessAddress() {
        AddBusinessAddressLayoutModel model = new AddBusinessAddressLayoutModel(true);
        model.setModelLocationDetails(modelMasterLocation.getDetails());
        addBusinessAddressLayoutModels.add(model);

        AddBusinessAddressLayoutAdapter addBusinessAddressLayoutAdapter = new AddBusinessAddressLayoutAdapter(getActivity(), addBusinessAddressLayoutModels);
        addBusinessAddressLayoutAdapter.onLocationSelection = this;
        add_business_parent_add_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        add_business_parent_add_rv.setAdapter(addBusinessAddressLayoutAdapter);

        //location
        ModelLocationDetail locationModel = new ModelLocationDetail(getActivity().getString(R.string.select_location));
        addBusinessAddressLayoutModels.get(0).getModelLocationDetails().add(0, locationModel);

        //street
        ModelCityStateStreetTelZip streetModel = new ModelCityStateStreetTelZip(getString(R.string.select_street));
        addBusinessAddressLayoutModels.get(0).getModelLocationDetails().get(0).getStreet().add(0, streetModel);

        //city
        ModelCityStateStreetTelZip cityModel = new ModelCityStateStreetTelZip(getString(R.string.select_city));
        addBusinessAddressLayoutModels.get(0).getModelLocationDetails().get(0).getCity().add(0, cityModel);

        //state
        ModelCityStateStreetTelZip stateModel = new ModelCityStateStreetTelZip(getString(R.string.select_state));
        addBusinessAddressLayoutModels.get(0).getModelLocationDetails().get(0).getState().add(0, stateModel);

        //zip
        ModelCityStateStreetTelZip zipModel = new ModelCityStateStreetTelZip(getString(R.string.select_zip));
        addBusinessAddressLayoutModels.get(0).getModelLocationDetails().get(0).getZip().add(0, zipModel);
    }

    /**
     * Add Business Category Layout
     */
    private void inflateBusinessCat() {
        AddBusinessCategoryModel model = new AddBusinessCategoryModel(true);
        //businessCategories.add(0,new BusinessCategories(getActivity().getString(R.string.select_category)));
        model.setCategoriesList(businessCategories);
        addBusinessCategoryLayoutModels.add(model);


        AddBusinessCategoryLayoutAdapter addBusinessCategoryAdapter = new AddBusinessCategoryLayoutAdapter(getActivity(), addBusinessCategoryLayoutModels);
        add_business_parent_cat_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        add_business_parent_cat_rv.setAdapter(addBusinessCategoryAdapter);

    }

    // validation
    private boolean validate() {
        if (add_business_business_name_et.getText().toString().trim().equals("")) {
            Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_name_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        if (add_business_contact_name_et.getText().toString().trim().equals("")) {
            Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.contact_name_empty), Snackbar.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    private boolean validateCategory() {
        if (addBusinessCategoryLayoutModels.size() > 0) {
            for (int i = 0; i < addBusinessCategoryLayoutModels.size(); i++) {
                AddBusinessCategoryModel model = addBusinessCategoryLayoutModels.get(i);
                if (model.getSelectedPos() == 0) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_category_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean validateAddress() {
        if (addBusinessAddressLayoutModels.size() > 0) {
            for (int i = 0; i < addBusinessAddressLayoutModels.size(); i++) {
                AddBusinessAddressLayoutModel model = addBusinessAddressLayoutModels.get(i);
                if (model.getSelectedLocationPos() == 0) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_address_location_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (model.getHouseNo().equals("")) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_address_house_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (!Character.isLetter(model.getHouseNo().charAt(0)) && model.getSelectedStreetPos() == 0) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_address_street_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                }/* else if (model.getAptNo().equals("")) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_address_apt_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                }*/ else if (model.getSelectedCityPos() == 0) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_address_city_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (model.getSelectedStatePos() == 0) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_address_state_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (model.getSelectedZipPos() == 0) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_address_zip_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean validatePhNo() {
        if (addBusinessPhoneLayoutModels.size() > 0) {
            for (int i = 0; i < addBusinessPhoneLayoutModels.size(); i++) {
                AddBusinessPhoneLayoutModel model = addBusinessPhoneLayoutModels.get(i);
                if (model.getSelectedLocationPos() == 0) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_phone_location_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (model.getAreaCode().equals("")) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_phone_area_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (model.getPhoneNo().equals("")) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_phone_no_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (model.getPhoneNo().length() < 7) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_phone_invalid), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (model.getSelectedPhoneTypePos() == 0) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_phone_type_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean validateEmail() {
        if (addBusinessEmailLayoutModels.size() > 0) {
            for (int i = 0; i < addBusinessEmailLayoutModels.size(); i++) {
                AddBusinessEmailLayoutModel model = addBusinessEmailLayoutModels.get(i);
                if (model.getMailId().equals("")) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_email_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (!AppUtilities.isValidEmail(model.getMailId())) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_email_invalid), Snackbar.LENGTH_LONG).show();
                    return false;
                }
            }
        }
        return true;
    }

    private boolean validateWeb() {
        if (addBusinessWebLayoutModels.size() > 0) {
            for (int i = 0; i < addBusinessWebLayoutModels.size(); i++) {
                AddBusinessWebLayoutModel model = addBusinessWebLayoutModels.get(i);
                if (model.getWebUrl().equals("")) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_web_empty), Snackbar.LENGTH_LONG).show();
                    return false;
                } else if (!AppUtilities.isValidURL(model.getWebUrl())) {
                    Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.business_web_invalid), Snackbar.LENGTH_LONG).show();
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Submit Data
     */
    //// TODO: 12-06-2017 Placer the proper key
    private void saveBusinessForm() {

        if (!NetworkUtil.isConnected(getActivity())) {
            Snackbar.make(business_parent_ll, AppUtilities.getStringFromResource(getActivity(), R.string.net_check_error), Snackbar.LENGTH_LONG).show();
            return;
        }

        generateParams();
        Body.Builder builder = new Body.Builder()
                .add(CONST.NAME_VALUE_PAIR.account_type, "2")
                .add(CONST.NAME_VALUE_PAIR.business_name, add_business_business_name_et.getText().toString().trim())
                .add(CONST.NAME_VALUE_PAIR.person_name, nameArr.toString())
                .add(CONST.NAME_VALUE_PAIR.business_category, catArr.toString())
                .add(CONST.NAME_VALUE_PAIR.address, addArr.toString())
                .add(CONST.NAME_VALUE_PAIR.phone, phArr.toString())
                .add(CONST.NAME_VALUE_PAIR.email, emailArr.toString())
                .add(CONST.NAME_VALUE_PAIR.website, webArr.toString())
                .add(CONST.NAME_VALUE_PAIR.comment, business_address_comment_et.getText().toString().trim());

        Body bodyRequest = new Body(builder);
        Header header = new Header(new Header.Builder().add("Content-Type", "application/x-www-form-urlencoded"));

        IceNet.connect()
                .createRequest()
                .post(header, bodyRequest)
                .pathUrl(CONST.REST_API.SAVE_LEAD_ACCOUNT_API)
                .fromString()
                .execute(getActivity(), "SAVE_LEAD_ACCOUNT_API", new RequestCallback() {
                    @Override
                    public void onRequestSuccess(Object o) {
                        DBG.e("AAAAAA", "SAVE_LEAD_ACCOUNT_API response - " + (String) o);
                        JSONObject result = null;
                        try {
                            result = new JSONObject((String) o);
                            if (result.getBoolean("success")) {
                                Snackbar.make(business_parent_ll, result.getString("message"), Snackbar.LENGTH_LONG)
                                        .addCallback(new Snackbar.Callback() {
                                            @Override
                                            public void onDismissed(Snackbar transientBottomBar, int event) {
                                                super.onDismissed(transientBottomBar, event);
                                                getActivity().finish();
                                            }
                                        }).show();

                            } else {
                                Snackbar.make(business_parent_ll, result.getString("message"), Snackbar.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onRequestError(RequestError error) {

                    }
                });

    }


    private void generateParams() {

        // person name
        nameArr = new JSONArray();
        try {
            nameObj = new JSONObject();
            nameObj.put(CONST.NAME_VALUE_PAIR.name, add_business_contact_name_et.getText().toString().trim());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        nameArr.put(nameObj);


        // business category
        catArr = new JSONArray();
        for (int i = 0; i < addBusinessCategoryLayoutModels.size(); i++) {
            catObj = new JSONObject();
            AddBusinessCategoryModel model = addBusinessCategoryLayoutModels.get(i);
            if (model.getSelectedPos() != 0) {
                try {
                    catObj.put(CONST.NAME_VALUE_PAIR.name, model.getCategoriesList().get(model.getSelectedPos()).getBusiness_category_id());
                    catObj.put(CONST.NAME_VALUE_PAIR.nameOrg, model.getCategoriesList().get(model.getSelectedPos()).getBusiness_category_name());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            catArr.put(catObj);

        }


        // address
        addArr = new JSONArray();
        for (int i = 0; i < addBusinessAddressLayoutModels.size(); i++) {
            addObj = new JSONObject();
            AddBusinessAddressLayoutModel model = addBusinessAddressLayoutModels.get(i);
            try {
                addObj.put(CONST.NAME_VALUE_PAIR.location_address, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getLocation_id());
                addObj.put(CONST.NAME_VALUE_PAIR.locationname, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getLocation_name());
                addObj.put(CONST.NAME_VALUE_PAIR.house_no, model.getHouseNo());
                addObj.put(CONST.NAME_VALUE_PAIR.house, model.getHouseNo());
                addObj.put(CONST.NAME_VALUE_PAIR.streetname, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getStreet().get(model.getSelectedStreetPos()).getName());
                addObj.put(CONST.NAME_VALUE_PAIR.street, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getStreet().get(model.getSelectedStreetPos()).getId());
                addObj.put(CONST.NAME_VALUE_PAIR.apt, model.getAptNo());
                addObj.put(CONST.NAME_VALUE_PAIR.apt_no, model.getAptNo());
                addObj.put(CONST.NAME_VALUE_PAIR.city_id, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getCity().get(model.getSelectedCityPos()).getId());
                addObj.put(CONST.NAME_VALUE_PAIR.city, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getCity().get(model.getSelectedCityPos()).getName());
                addObj.put(CONST.NAME_VALUE_PAIR.st, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getState().get(model.getSelectedStatePos()).getName());
                addObj.put(CONST.NAME_VALUE_PAIR.state, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getState().get(model.getSelectedStatePos()).getId());
                addObj.put(CONST.NAME_VALUE_PAIR.zip, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getZip().get(model.getSelectedZipPos()).getName());
                addObj.put(CONST.NAME_VALUE_PAIR.zip_id, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getZip().get(model.getSelectedZipPos()).getId());
                addObj.put(CONST.NAME_VALUE_PAIR.is_publish_address, (model.isDoNotDisplayChecked() ? "1" : "0"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            addArr.put(addObj);
        }


        // phone
        phArr = new JSONArray();
        for (int i = 0; i < addBusinessPhoneLayoutModels.size(); i++) {
            phObj = new JSONObject();
            AddBusinessPhoneLayoutModel model = addBusinessPhoneLayoutModels.get(i);
            try {
                phObj.put(CONST.NAME_VALUE_PAIR.locationname, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getLocation_name());
                phObj.put(CONST.NAME_VALUE_PAIR.location_telephone, model.getModelLocationDetails().get(model.getSelectedLocationPos()).getLocation_name());
                phObj.put(CONST.NAME_VALUE_PAIR.area, model.getAreaCode());
                phObj.put(CONST.NAME_VALUE_PAIR.area_code, model.getAreaCode());
                phObj.put(CONST.NAME_VALUE_PAIR.telephone_no, model.getPhoneNo());
                phObj.put(CONST.NAME_VALUE_PAIR.type, model.getPhoneTypeModels().get(model.getSelectedPhoneTypePos()).getName());
                phObj.put(CONST.NAME_VALUE_PAIR.telephone_type, model.getPhoneTypeModels().get(model.getSelectedPhoneTypePos()).getId());
                phObj.put(CONST.NAME_VALUE_PAIR.is_publish_telephone, (model.isDoNotDisplayPhoneChecked() ? "1" : "0"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            phArr.put(phObj);

        }


        // email
        emailArr = new JSONArray();
        for (int i = 0; i < addBusinessEmailLayoutModels.size(); i++) {
            emailObj = new JSONObject();
            AddBusinessEmailLayoutModel model = addBusinessEmailLayoutModels.get(i);
            try {
                emailObj.put(CONST.NAME_VALUE_PAIR.name, model.getMailId());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            emailArr.put(emailObj);
        }

        // web site
        webArr = new JSONArray();
        for (int i = 0; i < addBusinessWebLayoutModels.size(); i++) {
            webObj = new JSONObject();
            AddBusinessWebLayoutModel model = addBusinessWebLayoutModels.get(i);
            try {
                webObj.put(CONST.NAME_VALUE_PAIR.name, model.getWebUrl());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            webArr.put(webObj);
        }

    }


    @Override
    public void onLocationSelected(ModelLocationDetail modelLocationDetail) {
        modelLocationDetailsForTelephone.clear();
        modelLocationDetailsForTelephone.add(new ModelLocationDetail(getActivity().getString(R.string.select_location)));

        for (int i = 0; i < addBusinessAddressLayoutModels.size(); i++) {
            AddBusinessAddressLayoutModel model = addBusinessAddressLayoutModels.get(i);
            ModelLocationDetail modelLocation = model.getModelLocationDetails().get(model.getSelectedLocationPos());

            if (addBusinessAddressLayoutModels.get(i).getModelLocationDetails()
                    .get(addBusinessAddressLayoutModels.get(i).getSelectedLocationPos()).getLocation_id() != 0) {

                modelLocationDetailsForTelephone.add(modelLocation);
            }
        }

        setAllTelephoneLayoutModelsSelectedLocationPosZero();
        addBusinessPhoneLayoutAdapter.notifyAdapter(modelLocationDetailsForTelephone);

    }

    private void setAllTelephoneLayoutModelsSelectedLocationPosZero() {
        if (addBusinessPhoneLayoutModels.size() > 0) {
            for (int i = 0; i < addBusinessPhoneLayoutModels.size(); i++) {
                addBusinessPhoneLayoutModels.get(i).setSelectedLocationPos(0);
            }
        }
    }
}
