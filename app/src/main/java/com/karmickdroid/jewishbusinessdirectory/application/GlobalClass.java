package com.karmickdroid.jewishbusinessdirectory.application;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.karmickdroid.jewishbusinessdirectory.icenet.IceNet;
import com.karmickdroid.jewishbusinessdirectory.icenet.IceNetConfig;
import io.fabric.sdk.android.Fabric;
import io.paperdb.Paper;

/**
 * Created by administrator on 9/6/17.
 */

public class GlobalClass extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());

        initIceNet();
        Paper.init(getApplicationContext());
        Fresco.initialize(this);
    }

    public void initIceNet() {
        IceNetConfig config = new IceNetConfig.Builder()
                .setBaseUrl("")
                //.setBaseUrl("http://192.168.1.32/upload/")
                .setContext(getApplicationContext())
                .build();
        IceNet.init(config);

    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }
}
