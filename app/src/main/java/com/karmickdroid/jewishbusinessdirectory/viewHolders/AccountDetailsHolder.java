package com.karmickdroid.jewishbusinessdirectory.viewHolders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;

/**
 * Created by administrator on 9/6/17.
 */

public class AccountDetailsHolder extends RecyclerView.ViewHolder {

    public LinearLayout accountDetailsRow_ll_container;
    public ImageView accountDetailsRow_iv_icon;
    public TextView accountDetailsRow_tv_upper, accountDetailsRow_tv_lower_ph,accountDetailsRow_tv_lower_address;

    public AccountDetailsHolder(View itemView) {
        super(itemView);

        accountDetailsRow_ll_container = (LinearLayout) itemView.findViewById(R.id.accountDetailsRow_ll_container);
        accountDetailsRow_iv_icon = (ImageView)itemView.findViewById(R.id.accountDetailsRow_iv_icon);
        accountDetailsRow_tv_upper = (TextView)itemView.findViewById(R.id.accountDetailsRow_tv_upper);
        accountDetailsRow_tv_lower_ph = (TextView)itemView.findViewById(R.id.accountDetailsRow_tv_lower_ph);
        accountDetailsRow_tv_lower_address = (TextView)itemView.findViewById(R.id.accountDetailsRow_tv_lower_address);
    }
}
