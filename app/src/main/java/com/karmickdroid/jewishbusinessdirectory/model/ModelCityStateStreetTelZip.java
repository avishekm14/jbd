package com.karmickdroid.jewishbusinessdirectory.model;

import lombok.Data;

/**
 * Created by Administrator on 09-06-2017.
 */
@Data
public class ModelCityStateStreetTelZip {
    private int id;
    private String name = "";

    public ModelCityStateStreetTelZip(String name) {
        this.name = name;
    }
}