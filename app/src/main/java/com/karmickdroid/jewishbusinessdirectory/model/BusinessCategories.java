package com.karmickdroid.jewishbusinessdirectory.model;

/**
 * Created by avishek on 12/4/17.
 */

public class BusinessCategories {
    String business_category_id = "";
    String business_category_name = "";

    public BusinessCategories(String business_category_name) {
        this.business_category_name = business_category_name;
    }

    public BusinessCategories(String business_category_id, String business_category_name) {
        this.business_category_id = business_category_id;
        this.business_category_name = business_category_name;
    }

    public String getBusiness_category_id() {
        return business_category_id;
    }

    public void setBusiness_category_id(String business_category_id) {
        this.business_category_id = business_category_id;
    }

    public String getBusiness_category_name() {
        return business_category_name;
    }

    public void setBusiness_category_name(String business_category_name) {
        this.business_category_name = business_category_name;
    }
}
