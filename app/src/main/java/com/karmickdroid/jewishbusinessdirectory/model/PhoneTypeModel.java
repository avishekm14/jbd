package com.karmickdroid.jewishbusinessdirectory.model;

import lombok.Data;

/**
 * Created by administrator on 16/6/17.
 */
@Data
public class PhoneTypeModel {
    int id=0;
    String name="";

    public PhoneTypeModel(String name){
        this.name = name;
    }
}
