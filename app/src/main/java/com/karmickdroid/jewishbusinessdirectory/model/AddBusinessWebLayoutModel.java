package com.karmickdroid.jewishbusinessdirectory.model;

import lombok.Data;

/**
 * Created by administrator on 16/6/17.
 */
@Data
public class AddBusinessWebLayoutModel {
    String webUrl ="";
    boolean addOrNot = true;
    boolean doNotDiaplay = false;

    public AddBusinessWebLayoutModel(boolean addOrNot){
        this.addOrNot = addOrNot;
    }
}
