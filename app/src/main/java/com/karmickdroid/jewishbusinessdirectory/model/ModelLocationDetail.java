package com.karmickdroid.jewishbusinessdirectory.model;

import java.util.ArrayList;

import lombok.Data;

/**
 * Created by Administrator on 09-06-2017.
 */
@Data
public class ModelLocationDetail {
    private int location_id=0;
    private String location_name = "";
    private String country_id = "";
    private String country_name = "";
    private ArrayList<ModelCityStateStreetTelZip> street = new ArrayList<>();
    private ArrayList<ModelCityStateStreetTelZip> city = new ArrayList<>();
    private ArrayList<ModelCityStateStreetTelZip> state = new ArrayList<>();
    private ArrayList<ModelCityStateStreetTelZip> zip = new ArrayList<>();
    private ArrayList<ModelAreaCode> areacode = new ArrayList<>();

    public ModelLocationDetail(String location_name){
        this.location_name = location_name;
    }
}
