package com.karmickdroid.jewishbusinessdirectory.model;

/**
 * Created by avishek on 12/4/17.
 */

public class LocationModel {
    String location_id="";
    String location_name="";

    private String selected;

    private boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public LocationModel(){

    }

    public LocationModel(String location_id, String location_name, boolean isSelected){
        this.location_id = location_id;
        this.location_name = location_name;
        this.isSelected = isSelected;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }
}
