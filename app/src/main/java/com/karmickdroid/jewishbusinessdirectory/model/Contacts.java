package com.karmickdroid.jewishbusinessdirectory.model;

/**
 * Created by avishek on 17/4/17.
 */

public class Contacts {
    private String account_type_name = "";
    private String account_type = "";
    private String business_name = "";
    private String account_id = "";
    private String area_code = "";
    private String telephone_no = "";
    private String house_no = "";
    private String apt_no = "";
    private String street_name = "";
    private String city_name = "";
    private String state_name = "";
    private String state_code = "";
    private String country_name = "";
    private String zipcode = "";
    private String first_name = "";
    private String last_name = "";
    private String other_street = "";
    private String is_publish = "";

    private boolean needLoader = false;

    public Contacts() {



    }

    public String getIs_publish() {
        return is_publish;
    }

    public boolean isNeedLoader() {
        return needLoader;
    }

    public void setNeedLoader(boolean needLoader) {
        this.needLoader = needLoader;
    }

    public Contacts(boolean needLoader) {
        this.needLoader = needLoader;
    }

    public String getFirst_name() {
        if (first_name != null)
            return first_name;
        else
            return "";
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        if (last_name != null)
            return last_name;
        else
            return "";
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAccount_type_name() {
        if (account_type_name != null)
            return account_type_name;
        else
            return "";
    }

    public void setAccount_type_name(String account_type_name) {
        this.account_type_name = account_type_name;
    }

    public String getAccount_type() {
        if (account_type != null)
            return account_type;
        else
            return "";
    }

    public void setAccount_type(String account_type) {
        this.account_type = account_type;
    }

    public String getBusiness_name() {
        if (business_name != null)
            return business_name;
        else
            return "";
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getAccount_id() {
        if (account_id != null)
            return account_id;
        else
            return "";
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getArea_code() {
        if (area_code != null)
            return area_code;
        else
            return "";
    }

    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }

    public String getTelephone_no() {
        if (telephone_no != null)
            return telephone_no;
        else
            return "";
    }

    public void setTelephone_no(String telephone_no) {
        this.telephone_no = telephone_no;
    }

    public String getHouse_no() {
        if (house_no != null)
            return house_no;
        else
            return "";
    }

    public void setHouse_no(String house_no) {
        this.house_no = house_no;
    }

    public String getApt_no() {
        if (apt_no != null)
            return apt_no;
        else
            return "";
    }

    public void setApt_no(String apt_no) {
        this.apt_no = apt_no;
    }

    public String getStreet_name() {
        if (street_name != null)
            return street_name;
        else
            return "";
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getCity_name() {
        if (city_name != null)
            return city_name;
        else
            return "";
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getState_name() {
        if (state_name != null)
            return state_name;
        else
            return "";
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getState_code() {
        if (state_code != null)
            return state_code;
        else
            return "";
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getCountry_name() {
        if (country_name != null)
            return country_name;
        else
            return "";
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getZipcode() {
        if (zipcode != null)
            return zipcode;
        else
            return "";
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getOther_street() {
        return other_street;
    }

    public void setOther_street(String other_street) {
        this.other_street = other_street;
    }
}
