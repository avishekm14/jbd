package com.karmickdroid.jewishbusinessdirectory.model;

import lombok.Data;

@Data
public class ModelAreaCode {
    private int areacode;
}