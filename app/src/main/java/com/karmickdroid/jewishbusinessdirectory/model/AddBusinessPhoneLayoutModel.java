package com.karmickdroid.jewishbusinessdirectory.model;

import java.util.ArrayList;

import lombok.Data;

/**
 * Created by administrator on 13/6/17.
 */
@Data
public class AddBusinessPhoneLayoutModel {
    public ArrayList<ModelLocationDetail> modelLocationDetails = new ArrayList<>();
    public ArrayList<ModelCityStateStreetTelZip> phoneTypeModels = new ArrayList<>();
    boolean addOrNot = true;        // used for indicating if need to add or remove
    int selectedLocationPos = 0;
    int selectedPhoneTypePos = 0;

    boolean ifNotifiedByAddRemoveBtn = false;
    boolean doNotDisplayPhoneChecked = false;
    String areaCode = "", phoneNo = "";

    public AddBusinessPhoneLayoutModel(boolean addOrNot) {
        this.addOrNot = addOrNot;
    }
}
