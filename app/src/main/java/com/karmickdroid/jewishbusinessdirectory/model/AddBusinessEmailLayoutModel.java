package com.karmickdroid.jewishbusinessdirectory.model;

import lombok.Data;

/**
 * Created by administrator on 16/6/17.
 */
@Data
public class AddBusinessEmailLayoutModel {
    String mailId="";
    boolean addOrNot = true;
    boolean doNotDiaplay = false;

    public AddBusinessEmailLayoutModel(boolean addOrNot){
        this.addOrNot = addOrNot;
    }
}
