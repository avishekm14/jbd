package com.karmickdroid.jewishbusinessdirectory.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Created by administrator on 13/6/17.
 */
@Data
public class AddBusinessAddressLayoutModel {
    ArrayList<ModelLocationDetail> modelLocationDetails = new ArrayList<>();
    boolean addOrNot = true;        // used for indicating if need to add or remove
    int selectedLocationPos=0;
    int selectedStreetPos=0;
    int selectedCityPos=0;
    int selectedStatePos=0;
    int selectedZipPos=0;
    String houseNo="", aptNo="";
    boolean ifNotifiedByAddRemoveBtn = false;
    boolean doNotDisplayChecked = false;
    ModelLocationDetail selectedPrevAddModel;// = new ModelLocationDetail();

    public AddBusinessAddressLayoutModel(boolean addOrNot){
        this.addOrNot = addOrNot;
    }
}
