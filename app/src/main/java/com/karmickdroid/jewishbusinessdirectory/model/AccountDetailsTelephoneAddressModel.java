package com.karmickdroid.jewishbusinessdirectory.model;

/**
 * Created by administrator on 9/6/17.
 */

public class AccountDetailsTelephoneAddressModel {

    String location_address = "", location_name = "", house_no = "", apt_no = "", street_name = "", city_name = "", state_name = "",
            state_code = "", country_name = "", zipcode = "", other_street = "", other_city = "", other_state = "", other_zip = "";

    String location_telephone = "", area_code = "", telephone_no = "";


    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public String getHouse_no() {
        return house_no;
    }

    public void setHouse_no(String house_no) {
        this.house_no = house_no;
    }

    public String getApt_no() {
        return apt_no;
    }

    public void setApt_no(String apt_no) {
        this.apt_no = apt_no;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getOther_street() {
        return other_street;
    }

    public void setOther_street(String other_street) {
        this.other_street = other_street;
    }

    public String getOther_city() {
        return other_city;
    }

    public void setOther_city(String other_city) {
        this.other_city = other_city;
    }

    public String getOther_state() {
        return other_state;
    }

    public void setOther_state(String other_state) {
        this.other_state = other_state;
    }

    public String getOther_zip() {
        return other_zip;
    }

    public void setOther_zip(String other_zip) {
        this.other_zip = other_zip;
    }

    public String getLocation_telephone() {
        return location_telephone;
    }

    public void setLocation_telephone(String location_telephone) {
        this.location_telephone = location_telephone;
    }

    public String getArea_code() {
        return area_code;
    }

    public void setArea_code(String area_code) {
        this.area_code = area_code;
    }

    public String getTelephone_no() {
        return telephone_no;
    }

    public void setTelephone_no(String telephone_no) {
        this.telephone_no = telephone_no;
    }
}
