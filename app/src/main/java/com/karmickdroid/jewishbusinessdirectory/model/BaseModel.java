package com.karmickdroid.jewishbusinessdirectory.model;

import lombok.Data;

/**
 * Created by Administrator on 09-06-2017.
 */
@Data
public class BaseModel {
    private String message = "";
    private Boolean success = false;
}
