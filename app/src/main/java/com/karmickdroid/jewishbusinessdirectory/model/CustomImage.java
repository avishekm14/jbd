package com.karmickdroid.jewishbusinessdirectory.model;

import android.graphics.drawable.Drawable;

/**
 * Created by administrator on 27/3/18.
 */

public class CustomImage {

    Drawable image;

    public CustomImage(Drawable image) {
        this.image = image;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }
}
