package com.karmickdroid.jewishbusinessdirectory.model;

import java.util.ArrayList;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Created by Administrator on 09-06-2017.
 */
@EqualsAndHashCode(callSuper=true)
@Data
public class ModelMasterLocation extends BaseModel {

    private ArrayList<ModelCityStateStreetTelZip> telephone_type = new ArrayList<>();
    private ArrayList<ModelLocationDetail> details = new ArrayList<>();
}
