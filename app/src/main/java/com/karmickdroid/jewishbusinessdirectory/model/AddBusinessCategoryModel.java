package com.karmickdroid.jewishbusinessdirectory.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Created by administrator on 13/6/17.
 */
@Data
public class AddBusinessCategoryModel {
    List<BusinessCategories> categoriesList = new ArrayList<>();
    boolean addOrNot = true;
    int selectedPos=0;

    public AddBusinessCategoryModel(boolean addOrNot){
        this.addOrNot = addOrNot;
    }
}
