package com.karmickdroid.jewishbusinessdirectory.icenet;

import java.util.HashMap;

/**
 * Created by anton on 10/17/14.
 */
public class Body {
    private final HashMap<String, String> body;

    public Body(Builder builder) {
        this.body = builder.body;
    }

    public HashMap<String, String> getBody() {
        return body;
    }

    public static class Builder {
        private String key;
        private String value;
        private HashMap<String, String> body = new HashMap<String, String>();

        public Builder add(String key, String value) {
            body.put(key, value);
            return this;
        }

        /**
         * Should be used when there is huge post data
         *
         * @param body
         */
        public void setBody(HashMap<String, String> body) {
            this.body = body;
        }

        public Body build() {
            return new Body(this);
        }
    }
}
