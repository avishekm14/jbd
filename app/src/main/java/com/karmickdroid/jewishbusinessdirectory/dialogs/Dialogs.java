package com.karmickdroid.jewishbusinessdirectory.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.karmickdroid.jewishbusinessdirectory.R;
import com.karmickdroid.jewishbusinessdirectory.helper.AppUtilities;


public class Dialogs {


    public static void ReportErrorWithCloseButton(final Context context, final View container, final OnErrorReportMsgBtnClickListener _callback) {
        final Dialog dialog = new Dialog(context, R.style.dialog);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE );
        dialog.setContentView(R.layout.report_error_with_close);
        //dialog.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        dialog.setCanceledOnTouchOutside(false);


        final RelativeLayout reportErrorWithClose_rl_container = (RelativeLayout)dialog.findViewById(R.id.reportErrorWithClose_rl_container);
        ImageView reportErrorWithClose_ivClose = (ImageView) dialog.findViewById(R.id.reportErrorWithClose_ivClose);
        Button reportErrorWithClose_btnOk = (Button) dialog.findViewById(R.id.reportErrorWithClose_btnOk);

//        final EditText reportErrorWithClose_et_userName = (EditText) dialog.findViewById(R.id.reportErrorWithClose_et_userName);
//        final EditText reportErrorWithClose_et_PhoneNo = (EditText) dialog.findViewById(R.id.reportErrorWithClose_et_PhoneNo);
        final EditText reportErrorWithClose_et_reportMsg = (EditText) dialog.findViewById(R.id.reportErrorWithClose_et_reportMsg);


        reportErrorWithClose_ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        reportErrorWithClose_btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String userName = reportErrorWithClose_et_userName.getText().toString().trim();
//                String phNo = reportErrorWithClose_et_PhoneNo.getText().toString().trim();
                String reportMsg = reportErrorWithClose_et_reportMsg.getText().toString().trim();

                if (!reportMsg.equals("")) {
                    dialog.dismiss();
                    if (null != _callback) {
                        _callback.onErrorReportMsgBtnClickListener(reportMsg);
                    }
                }else{
                    Snackbar.make(reportErrorWithClose_rl_container,
                            AppUtilities.getStringFromResource(context
                                    , R.string.user_report_msg_empty), Snackbar.LENGTH_LONG).show();
                }
            }
        });
        dialog.show();
    }




    public static void setErrorInformation(TextInputLayout textInputLayout, EditText editText,
                                           String errorMsg, Context mActivity) {
        getErrorFreeUI(textInputLayout);
        textInputLayout.setError(errorMsg);
        textInputLayout.startAnimation(AnimationUtils.loadAnimation(mActivity, R.anim.shake));
        editText.requestFocus();
    }

    public static void getErrorFreeUI(TextInputLayout textInputLayout) {
        textInputLayout.setError(null);
        textInputLayout.setError(null);
    }

    public interface onDialogOkButtonClickListener {
        void onDialogOkButtonClick();
    }

    public interface YesNoClickListener {
        void onYesClick();

        void onNoClick();

    }

    public interface onEditableDialogButtonClickListener {
        void onEditableDialogButtonClick(String projectName);
    }

    public interface OnErrorReportMsgBtnClickListener {
        void onErrorReportMsgBtnClickListener(String reportMsg);
    }

}
