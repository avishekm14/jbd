package com.karmickdroid.jewishbusinessdirectory.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.karmickdroid.jewishbusinessdirectory.R;

import java.util.logging.Logger;

import static com.karmickdroid.jewishbusinessdirectory.debug.DBG.printStackTrace;

public class AppUtilities {
    private static Context context = null;
    public static int callVersionApi = 301;
    public static int showVersionCheckMessage = 302;

    /**
     * Recently set context will be returned.
     * If not set it from current class it will
     * be null.
     *
     * @return Context
     */
    public static final Context getContext() {
        return AppUtilities.context;
    }

    /**
     * First set context from every activity
     * before use any static method of AppUtils class.
     *
     * @param ctx
     */
    public static final void setContext(Context ctx) {
        AppUtilities.context = ctx;
    }

    /**
     * Get String from resource id
     *
     * @param res
     * @return
     */
    public static final String getStringFromResource(Context context, int res) {
        if (null != context) {
            try {
                return context.getResources().getString(res);
            } catch (Resources.NotFoundException e) {
                printStackTrace(e);
                return "";
            }
        } else {
            return "";
        }
    }

    /**
     * Check for email validation using android default
     * email validator.
     *
     * @param target
     * @return boolean
     */
    public static final boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static boolean isValidURL(String url){
        if(url.equals(null) || url.equals("")){
            return false;
        }else{
            return Patterns.WEB_URL.matcher(url).matches();
        }
    }

    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivity(Context context, Class cls) {
        if (null != context) {
            Intent intent = new Intent(context, cls);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            context.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        }
    }


    /**
     * Redirect current Activity to desire Activity
     *
     * @param cls
     */
    public static final void redirectActivityWithSingleExtra(Context context, Class cls, String stringExtra) {
        if (null != context) {
            Intent intent = new Intent(context, cls);
            intent.putExtra("extra", stringExtra);
            context.startActivity(intent);
        /*    ((Activity) _ctx).finish();*/
        }
    }

    public static int getStatusBarHeight(Activity mActivity) {
        Rect rect = new Rect();
        Window win = mActivity.getWindow();
        win.getDecorView().getWindowVisibleDisplayFrame(rect);

        int statusBarHeight = rect.top;
        int contentViewTop = win.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = contentViewTop - statusBarHeight;
        if (statusBarHeight == 0) {
            statusBarHeight = 40;
        }
        return statusBarHeight;
    }

    /*public static void wantToExitFromApp(Context context) {
        Dialogs.DoubleButtonDialog(context, AppUtilities.getStringFromResource(context, R.string.yes)
                , AppUtilities.getStringFromResource(context, R.string.no),
                AppUtilities.getStringFromResource(context, R.string.want_to_exit),
                new Dialogs.YesNoClickListener() {
                    @Override
                    public void onYesClick() {
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        getContext().startActivity(intent);
                    }

                    @Override
                    public void onNoClick() {

                    }
                });


    }*/


    /**
     * Showing Toast message for short length/time
     * using string.xml file resource id as param.
     *
     * @param rec_id
     */
/*    public static final void showToast(Context context, final int rec_id) {
        if (null != context) {
            try {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);

                View layout = inflater.inflate(R.layout.custom_toast, null);

                TextView text = (TextView) layout.findViewById(R.id.text);
                text.setText(getStringFromResource(context, rec_id));

                Toast toast = new Toast(context.getApplicationContext());
                toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 350);
                toast.setDuration(Toast.LENGTH_SHORT);
                toast.setView(layout);
                toast.show();


            } catch (Resources.NotFoundException e) {
                e.printStackTrace();
            }
        }

    }*/

    /**
     * Showing Toast message for short length/time
     * using string as param.
     *
     * @param str_toastMsg
     */
    /*public static final void showToast(Context context, final String str_toastMsg) {
        if (null != context) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);

            View layout = inflater.inflate(R.layout.custom_toast, null);

            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(str_toastMsg);

            Toast toast = new Toast(context.getApplicationContext());
            toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 350);
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
    }*/

    /*public static final void showToastLong(Context context, final String str_toastMsg) {
        if (null != context) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);

            View layout = inflater.inflate(R.layout.custom_toast, null);

            TextView text = (TextView) layout.findViewById(R.id.text);
            text.setText(str_toastMsg);

            Toast toast = new Toast(context.getApplicationContext());
            toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 350);
            toast.setDuration(Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();


            // Toast.makeText(_ctx, str_toastMsg, Toast.LENGTH_SHORT).show();
        }
    }*/



    /*public static ProgressDialog PD(Context context, boolean showLoadingText, String loaderText) {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(context);
            progressDialog.setCanceledOnTouchOutside(false);

            try {
                progressDialog.show();

                progressDialog.setContentView(R.layout.progress_layout);
                progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                //progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.GREEN));

                TextView loadingText = (TextView) progressDialog.findViewById(R.id.loadingText);
                if (showLoadingText) {
                    loadingText.setVisibility(View.VISIBLE);
                    loadingText.setText(loaderText);
                } else {

                    loadingText.setVisibility(View.GONE);
                }
            } catch (WindowManager.BadTokenException | IllegalStateException e) {
                Logger.setLogTag("AppUtilities");
                Logger.printStackTrace(e);
            }
            // dialog.setMessage(Message);
            return progressDialog;
        } catch (NullPointerException e) {
            Logger.setLogTag("AppUtilities");
            Logger.printStackTrace(e);
            return null;
        }
    }*/

    public static final void hideSoftInputMode(final Context context, final EditText et) {
        if (null != context) {
            InputMethodManager im = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(et.getWindowToken(), 0);
        }
    }

    public static void showSoftInputMode(Context context){
        InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
    }
}
