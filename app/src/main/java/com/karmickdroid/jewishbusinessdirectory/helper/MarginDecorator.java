package com.karmickdroid.jewishbusinessdirectory.helper;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;


public class MarginDecorator extends RecyclerView.ItemDecoration {
    private static final int[] ATTRS = new int[]{android.R.attr.listDivider};

    private Drawable mDivider;
    private Context context;

    /**
     * Default divider will be used
     */
    public MarginDecorator(Context context) {
        final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
        mDivider = styledAttributes.getDrawable(0);
        //mDivider = context.getResources().getDrawable(R.drawable.divider,null);
        styledAttributes.recycle();
        this.context = context;
    }

    /**
     * Custom divider will be used
     */
    public MarginDecorator(Context context, int resId) {
        mDivider = ContextCompat.getDrawable(context, resId);
        this.context = context;
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (i < childCount-1) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                //c.drawColor(context.getResources().getColor(R.color.colorAccent,null));


                /*Paint paint = new Paint();
                paint.setColor(context.getResources().getColor(R.color.colorDivider));
                paint.setStyle(Paint.Style.STROKE);
                c.drawLine(left, bottom, right, bottom, paint);*/


                mDivider.draw(c);

            }
        }
    }
}

