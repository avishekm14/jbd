package com.karmickdroid.jewishbusinessdirectory.helper;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.karmickdroid.jewishbusinessdirectory.R;

public class MyDialog {
    ProgressDialog progressDialog = null;

    public void showProgressLoader(Context ctx, String msg) {
        progressDialog = new ProgressDialog(ctx, R.style.MyProgressDialogTheme);
        //progressDoalog.setMessage(msg);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        Drawable drawable = new ProgressBar(ctx).getIndeterminateDrawable().mutate();
        drawable.setColorFilter(ContextCompat.getColor(ctx, R.color.textColorBlack),
                PorterDuff.Mode.SRC_IN);
        progressDialog.setIndeterminateDrawable(drawable);
        //progressDialog .setIndeterminateDrawable(ctx.resources.getDrawable(R.drawable.ic_add_wish))
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        progressDialog.show();
    }

    public void dismissProgressLoader() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

}