package com.karmickdroid.jewishbusinessdirectory.helper;

import com.karmickdroid.jewishbusinessdirectory.helper.CONST;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by arnab on 24/06/18.
 */

public interface RestApi {
    @POST(CONST.REST_API.GET_BUSINESS_CONTACTS)
    @FormUrlEncoded
    Call<ResponseBody> getSearchResult(
            @Field("business_category") String category,
            @Field("location") String location,
            @Field("name") String name,
            @Field("account_type") String account_type,
            @Field("offset") String offset,
            @Field("length") String length
    );
}
