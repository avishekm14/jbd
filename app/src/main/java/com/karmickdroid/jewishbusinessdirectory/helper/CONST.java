package com.karmickdroid.jewishbusinessdirectory.helper;

/**
 * Created by avishek on 12/4/17.
 */

public class CONST {
    public interface REST_API {
        //              String BASE_API = "http://www.karmickdev.com/jbd/";
        String BASE_API = "http://www.thejbd.com/app/";
        String GET_LOCATION = BASE_API + "get-location-api";
        String GET_BUSINESS_CATEGORIES = BASE_API + "get-business-category-api";
        String GET_BUSINESS_CONTACTS = BASE_API + "get-account-api";
        String GET_ACCOUNT_DETAILS_API = BASE_API + "get-account-details-api";
        String MASTER_LOCATION = BASE_API + "get-location-street-city-state-zip-api";

        String SAVE_LEAD_ACCOUNT_API = BASE_API + "save-lead-account-api";

        String MAIL_REPORT_MSG_API = BASE_API + "mail-report-msg-api";
        String GET_DIRECTORY_ADS_IMAGE_API = BASE_API + "get-account-image-api";
    }

    public interface NAME_VALUE_PAIR {
        String LOCATION_ID = "location_id";
        String CATEGORIES_ID = "categories_id";
        String SEARCH_TEXT = "search_text";
        String LOCATIONS = "locations";
        String CATEGORY = "categories";

        //details
        String account_id = "account_id";
        String comment_report = "comment";

        // add residential
        String account_type = "account_type";
        String first_name = "first_name";
        String last_name = "last_name";
        String father_name = "father_name";
        String maidan_name = "maidan_name";
        String comment = "comment";
        String address = "address";
        String phone = "phone";

        String location_address = "location_address";
        String house_no = "house_no";
        String street = "street";
        String apt_no = "apt_no";
        String city_id = "city_id";
        String state = "state";
        String zip_id = "zip_id";
        String is_publish_address = "is_publish_address";

        String area_code = "area_code";
        String telephone_no = "telephone_no";
        String is_publish_telephone = "is_publish_telephone";
        String location_telephone = "location_telephone";
        String telephone_type = "telephone_type";


        // add business
        //String account_type = "account_type";
        String business_name = "business_name";

        String person_name = "person_name";
        String name = "name";

        String business_category = "business_category";
        //String name = "name";
        String nameOrg = "nameOrg";

        //String address = "address";
        //String state = "state";
        //String location_address = "location_address";
        //String house_no = "house_no";
        String streetname = "streetname";
        String apt = "apt";
        String locationname = "locationname";
        //String apt_no = "apt_no";
        String st = "st";
        String zip = "zip";
        //String street = "street";
        //String is_publish_address = "is_publish_address";
        //String zip_id = "zip_id";
        //String city_id = "city_id";
        String city = "city";
        String house = "house";

        //String phone = "phone";
        //String is_publish_telephone = "is_publish_telephone";
        String area = "area";
        //String area_code = "area_code";
        //String telephone_type = "telephone_type";
        //String locationname = "locationname";
        //String telephone_no = "telephone_no";
        String type = "type";
        //String location_telephone = "location_telephone";

        String email = "email";
        //String name = "name";

        String website = "website";
        //String name = "name";

        //String comment = "comment";


    }
}
